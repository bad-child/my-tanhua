package com.tanhua.dubbo.server.api;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.enums.StatusEnum;
import com.tanhua.dubbo.server.pojo.*;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuanZiApiImpl implements QuanZiApi {
    //评论数据存储在Redis中key的前缀
    private static final String COMMENT_REDIS_KEY_PREFIX = "QUANZI_COMMENT_";

    //用户是否点赞的前缀
    private static final String COMMENT_USER_LIKE_REDIS_KEY_PREFIX = "USER_LIKE_";

    //用户是否喜欢的前缀
    private static final String COMMENT_USER_LOVE_REDIS_KEY_PREFIX = "USER_LOVE_";

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 查询用户时间线表
     *
     * @param id 登陆者id
     * @return
     */
    @Override
    public List<TimeLine> queryTimeLine(Long id) {
        return mongoTemplate.findAll(TimeLine.class, "quanzi_time_line_" + id);
    }

    /**
     * 查询动态列表
     *
     * @param publishIdList 动态id集合
     * @param page
     * @param pagesize
     * @return
     */
    @Override
    public List<Publish> queryPublishList(List<Object> publishIdList, Integer page, Integer pagesize) {
        List<Publish> newList = new ArrayList<>();
        PageRequest pageRequest = PageRequest.of(page - 1, pagesize, Sort.by(Sort.Order.desc("created")));
        Query query = Query.query(Criteria.where("id").in(publishIdList).and("status").is(StatusEnum.YTG.getValue())).with(pageRequest);
        List<Publish> publishList = mongoTemplate.find(query, Publish.class);
        for (Publish publish : publishList) {
            if (publish.getTop() == 2) {
                newList.add(publish);
                break;
            }
        }

        for (Publish publish : publishList) {
            if (publish.getTop() == 2) {
                continue;
            }
            newList.add(publish);
        }
        return newList;
    }

    /**
     * 查询系统推荐动态列表
     *
     * @param pids     系统推荐的动态自增长id集合
     * @param page
     * @param pagesize
     * @return
     */
    @Override
    public List<Publish> queryRecommendList(List<Long> pids, Integer page, Integer pagesize) {
        PageRequest pageRequest = PageRequest.of(page - 1, pagesize, Sort.by(Sort.Order.desc("created")));
        Query query = Query.query(Criteria.where("pid").in(pids).and("status").is(StatusEnum.YTG.getValue())).with(pageRequest);
        return mongoTemplate.find(query, Publish.class);
    }

    /**
     * 写入动态
     *
     * @param publish 动态
     */
    @Override
    public void savePublish(Publish publish) {
        mongoTemplate.insert(publish);
    }

    /**
     * 写入相册表
     *
     * @param album 相册表对象
     * @param id    登录者id
     */
    @Override
    public void saveAlbum(Album album, Long id) {
        mongoTemplate.insert(album, "quanzi_album_" + id);
    }

    /**
     * 查询用户好友集合
     *
     * @param id 登录者id
     * @return
     */
    @Override
    public List<Users> queryFriendIds(Long id) {
        Query query = Query.query(Criteria.where("userId").is(id));
        return mongoTemplate.find(query, Users.class);
    }

    /**
     * 写入好友时间线表
     *
     * @param friendId 好友id
     * @param timeLine
     */
    @Override
    public void saveTimeLine(Object friendId, TimeLine timeLine) {
        mongoTemplate.insert(timeLine, "quanzi_time_line_" + friendId);
    }

    /**
     * 查询当前用户是否已点赞/喜欢
     *
     * @param id          当前登录用户id
     * @param publishId   动态id/评论id/视频id
     * @param commentType 操作类型
     * @return
     */
    @Override
    public Boolean queryUserIsLikeOrLove(Long id, String publishId, CommentType commentType) {
        //查询redis
        String redisKey = getCommentRedisKeyPrefix(publishId);
        String userHashKey = getCommentUserRedisKeyPrefix(id, commentType);
        Object data = redisTemplate.opsForHash().get(redisKey, userHashKey);
        //判断是否有数据
        if (ObjectUtil.isNotEmpty(data)) {
            //判断是否为  "1"        将data转换为字符串
            return StrUtil.equals(Convert.toStr(data), "1");
        }
        //查询MongoDB
        Query query = Query.query(Criteria.where("userId").is(id)
                .and("publishId").is(new ObjectId(publishId))
                .and("commentType").is(commentType));
        long count = mongoTemplate.count(query, Comment.class);
        if (count == 0) {
            return false;
        }
        //已点赞则写入redis中
        redisTemplate.opsForHash().put(redisKey, userHashKey, "1");
        return true;
    }

    private String getCommentUserRedisKeyPrefix(Long id, CommentType commentType) {
        return commentType == CommentType.LIKE ?
                COMMENT_USER_LIKE_REDIS_KEY_PREFIX + id :
                COMMENT_USER_LOVE_REDIS_KEY_PREFIX + id;
    }

    private String getCommentRedisKeyPrefix(String publishId) {
        return COMMENT_REDIS_KEY_PREFIX + publishId;
    }

    /**
     * 向comment表写入数据
     *
     * @param id          登录者id
     * @param publishId   动态id/评论id/视频id
     * @param commentType 操作类型
     * @param content     评论正文(非评论为null)
     * @return
     */
    private Boolean saveComment(Long id, String publishId, CommentType commentType, String content) {
        try {
            Comment comment = new Comment();
            comment.setPublishId(new ObjectId(publishId));
            comment.setCommentType(commentType.getType());
            comment.setContent(content);
            comment.setUserId(id);
            //根据动态id查询发布动态/动态评论/视频的用户id
            Publish publish = mongoTemplate.findById(new ObjectId(publishId), Publish.class);
            if (ObjectUtil.isNotEmpty(publish)) {
                comment.setPublishUserId(publish.getUserId());
            } else {
                Comment parentComment = mongoTemplate.findById(new ObjectId(publishId), Comment.class);
                if (ObjectUtil.isNotEmpty(parentComment)) {
                    comment.setPublishUserId(parentComment.getPublishUserId());
                } else {
                    Video video = mongoTemplate.findById(publishId, Video.class);
                    comment.setPublishUserId(video.getUserId());
                }
            }
            comment.setCreated(System.currentTimeMillis());
            mongoTemplate.save(comment);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 点赞/喜欢/评论 动态/评论/视频
     *
     * @param id          登录者id
     * @param publishId   动态id/评论id/视频id
     * @param commentType 操作类型
     * @param content     评论正文(非评论为null)
     * @return
     */
    @Override
    public Boolean insertComment(Long id, String publishId, CommentType commentType, String content) {
        if (commentType.getType() != CommentType.COMMENT.getType()) {
            //判断该用户是否已点赞/喜欢(查询comment表中是否有记录),已点赞/喜欢则直接返回
            if (queryUserIsLikeOrLove(id, publishId, commentType)) {
                return false;
            }
        }
        //向comment表写入数据
        Boolean result = saveComment(id, publishId, commentType, content);
        if (!result) {
            return false;
        }
        //修改redis中的数据
        String redisKey = getCommentRedisKeyPrefix(publishId);
        String hashKey = commentType.toString();
        String userHashKey = getCommentUserRedisKeyPrefix(id, commentType);
        //修改点赞/喜欢/评论数
        redisTemplate.opsForHash().increment(redisKey, hashKey, 1);
        if (commentType.getType() != CommentType.COMMENT.getType()) {
            //修改该用户是否点赞/喜欢
            redisTemplate.opsForHash().put(redisKey, userHashKey, "1");
        }
        return true;
    }

    /**
     * 取消点赞/喜欢 动态/评论/视频
     *
     * @param id          登录者id
     * @param publishId   动态id/评论id/视频id
     * @param commentType 操作类型
     * @return
     */
    @Override
    public Boolean delComment(Long id, String publishId, CommentType commentType) {
        //判断当前用户是否已点赞
        if (!queryUserIsLikeOrLove(id, publishId, commentType)) {
            return false;
        }
        //从comment表中删除数据
        Boolean result = removeComment(id, publishId, commentType);
        if (!result) {
            return false;
        }
        //修改缓存中的数据
        String redisKey = getCommentRedisKeyPrefix(publishId);
        String hashKey = commentType.toString();
        String userHashKey = getCommentUserRedisKeyPrefix(id, commentType);
        //修改该用户是否点赞/喜欢
        redisTemplate.opsForHash().delete(redisKey, userHashKey);
        //修改点赞/喜欢数
        redisTemplate.opsForHash().increment(redisKey, hashKey, -1);
        return true;
    }

    /**
     * 查询动态/视频评论
     *
     * @param publishId 动态id/视频id
     * @return
     */
    @Override
    public List<Comment> queryCommentList(String publishId) {
        Query query = Query.query(Criteria.where("publishId").is(new ObjectId(publishId)).and("commentType").is(CommentType.COMMENT.getType()));
        return mongoTemplate.find(query, Comment.class);
    }

    /**
     * 查询点赞/评论/喜欢
     *
     * @param publishUserId 登陆者用户id
     * @param page
     * @param pageSize
     * @param commentType   操作类型
     * @return
     */
    @Override
    public List<Comment> queryCommentListByUser(Long publishUserId, Integer page, Integer pageSize, CommentType commentType) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Order.desc("created")));
        Query query = Query.query(Criteria.where("publishUserId").is(publishUserId)
                        .and("commentType").is(commentType.getType()))
                .with(pageRequest);
        return mongoTemplate.find(query, Comment.class);
    }

    /**
     * 查询个人动态
     *
     * @param userId   要查询的用户id
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public List<Publish> queryAlbumList(Long userId, Integer page, Integer pageSize) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Order.desc("created")));
        Query query = new Query().with(pageRequest);
        List<Album> albumList = mongoTemplate.find(query, Album.class, "quanzi_album_" + userId);
        if (CollUtil.isEmpty(albumList)) {
            return null;
        }
        List<Object> publishIds = CollUtil.getFieldValues(albumList, "publishId");
        return queryPublishList(publishIds, page, pageSize);
    }

    /**
     * 从comment表中删除记录
     *
     * @param id          登录者id
     * @param publishId   动态id/评论id/视频id
     * @param commentType 操作类型
     * @return
     */
    private Boolean removeComment(Long id, String publishId, CommentType commentType) {
        Query query = Query.query(Criteria.where("userId").is(id)
                .and("publishId").is(new ObjectId(publishId))
                .and("commentType").is(commentType.getType()));
        return mongoTemplate.remove(query, Comment.class).getDeletedCount() > 0;
    }

    /**
     * 查询点赞/评论/喜欢数
     *
     * @param publishId   动态id/评论id/视频id
     * @param commentType 操作类型
     * @return
     */
    @Override
    public Long queryCommentCount(String publishId, CommentType commentType) {
        //从缓存中查找
        String redisKey = getCommentRedisKeyPrefix(publishId);
        String hashKey = commentType.toString();
        Object data = redisTemplate.opsForHash().get(redisKey, hashKey);
        //判断数据是否为空
       /* if (ObjectUtil.isNotEmpty(data)) {
            if (Convert.toInt(data) != 0) {
                return Convert.toLong(data);
            }
        }*/
        //缓存为命中则从评论表中查询记录数
        Query query = Query.query(Criteria.where("publishId").is(new ObjectId(publishId))
                .and("commentType").is(commentType.getType()));
        long count = mongoTemplate.count(query, Comment.class);
        //写入缓存
        redisTemplate.opsForHash().put(redisKey, hashKey, String.valueOf(count));
        return count;
    }

    /**
     * 查询个人动态
     *
     * @param userId   要查询的用户id
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public List<Publish> queryPublishList(Long userId, Integer page, Integer pageSize) {
        List<Publish> newList = new ArrayList<>();
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Order.desc("created")));
        Query query = new Query(Criteria.where("userId").is(userId).and("status").is(StatusEnum.YTG.getValue())).with(pageRequest);
        List<Publish> publishList = mongoTemplate.find(query, Publish.class);

        for (Publish publish : publishList) {
            if (publish.getTop() == 2) {
                newList.add(publish);
                break;
            }
        }

        for (Publish publish : publishList) {
            if (publish.getTop() == 2) {
                continue;
            }
            newList.add(publish);
        }

        return newList;
    }

}
