package com.tanhua.dubbo.server.api;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.UserLike;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class UserLikeApiImpl implements UserLikeApi {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    public static final String LIKE_REDIS_KEY_PREFIX = "USER_LIKE_";
    public static final String NOT_LIKE_REDIS_KEY_PREFIX = "USER_NOT_LIKE_";

    /**
     * 获取喜欢数据的redis key
     *
     * @param userId
     * @return
     */
    private String getLikeRedisKey(Long userId) {
        return LIKE_REDIS_KEY_PREFIX + userId;
    }

    /**
     * 获取不喜欢数据的redis key
     *
     * @param userId
     * @return
     */
    private String getNotLikeRedisKey(Long userId) {
        return NOT_LIKE_REDIS_KEY_PREFIX + userId;
    }

    /**
     * 是否喜欢
     *
     * @param userId     操作者id
     * @param likeUserId 被喜欢者id
     * @return
     */
    public Boolean isLike(Long userId, Long likeUserId) {
        Query query = Query.query(Criteria.where("userId").is(userId).and("likeUserId").is(likeUserId));
        long count = mongoTemplate.count(query, UserLike.class);
        return count > 0;
        /*String redisKey = getLikeRedisKey(userId);
        String hashKey = String.valueOf(likeUserId);
        return redisTemplate.opsForHash().hasKey(redisKey, hashKey);*/
    }

    /**
     * 是否不喜欢
     *
     * @param userId     操作者id
     * @param likeUserId 被不喜欢者id
     * @return
     */
    private Boolean isNotLike(Long userId, Long likeUserId) {
        String redisKey = getNotLikeRedisKey(userId);
        String hashKey = String.valueOf(likeUserId);
        return redisTemplate.opsForHash().hasKey(redisKey, hashKey);
    }

    /**
     * 喜欢
     *
     * @param userId     操作用户id
     * @param likeUserId 被喜欢用户id
     * @return
     */
    @Override
    public Boolean likeUser(Long userId, Long likeUserId) {
        //判断是否已喜欢
        if (isLike(userId, likeUserId)) {
            return false;

        }
        if (ObjectUtil.isEmpty(likeUserId)) {
            return true;
        }
        //存入MongoDB数据库
        UserLike userLike = new UserLike();
        userLike.setId(ObjectId.get());
        userLike.setUserId(userId);
        userLike.setLikeUserId(likeUserId);
        userLike.setCreated(System.currentTimeMillis());
        mongoTemplate.save(userLike);
        //存入缓存(redis)
        String redisKey = getLikeRedisKey(userId);
        String hashKey = String.valueOf(likeUserId);
        redisTemplate.opsForHash().put(redisKey, hashKey, "1");
        //判断被喜欢用户是否在不喜欢列表中
        if (isNotLike(userId, likeUserId)) {
            //存在则删除数据
            redisKey = getNotLikeRedisKey(userId);
            redisTemplate.opsForHash().delete(redisKey, hashKey);
        }
        return true;
    }

    /**
     * 不喜欢
     *
     * @param userId     操作用户id
     * @param likeUserId 被不喜欢用户id
     * @return
     */
    @Override
    public Boolean notLikeUser(Long userId, Long likeUserId) {
        //判断是否已不喜欢
        if (isNotLike(userId, likeUserId)) {
            return false;
        }
        //存入缓存(redis)
        String redisKey = getNotLikeRedisKey(userId);
        String hashKey = String.valueOf(likeUserId);
        redisTemplate.opsForHash().put(redisKey, hashKey, "1");
        //判断被喜欢用户是否在喜欢列表中
        //在则移除喜欢列表
        if (isLike(userId, likeUserId)) {
            cancelLike(userId, likeUserId);
        }
        return true;
    }


    /**
     * 取消喜欢
     *
     * @param userId     操作用户id
     * @param likeUserId 被取消喜欢用户id
     * @return
     */
    @Override
    public Boolean cancelLike(Long userId, Long likeUserId) {
        try {
            String redisKey = getLikeRedisKey(userId);
            String hashKey = String.valueOf(likeUserId);
            //删除数据(MongoDB与redis)
            Query query = Query.query(Criteria.where("userId").is(userId).and("likeUserId").is(likeUserId));
            long count = mongoTemplate.remove(query, UserLike.class).getDeletedCount();
            if (count == 0) {
                return false;
            }
            redisTemplate.opsForHash().delete(redisKey, hashKey);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 是否相互喜欢
     *
     * @param userId     操作用户id
     * @param likeUserId 被喜欢用户id
     * @return
     */
    @Override
    public Boolean isMutualLike(Long userId, Long likeUserId) {
        return isLike(userId, likeUserId) && isLike(likeUserId, userId);
    }

    /**
     * 查询喜欢列表
     *
     * @param userId 操作用户id
     * @return
     */
    @Override
    public List<Long> queryLikeList(Long userId) {


        String redisKey = getLikeRedisKey(userId);
        Set<Object> keys = redisTemplate.opsForHash().keys(redisKey);
        if (CollUtil.isEmpty(keys)) {
            return ListUtil.empty();
        }

        /*for (Object key : keys) {
            redisTemplate.opsForHash().delete(redisKey, key);
        }*/

        List<Long> likeUserIds = new ArrayList<>();
        keys.forEach(id -> likeUserIds.add(Convert.toLong(id)));


        return likeUserIds;
    }

    /**
     * 查询被喜欢列表
     *
     * @param userId 操作用户 id
     * @return
     */
    public List<UserLike> queryBeLikeList(Long userId) {
        Query query = Query.query(Criteria.where("likeUserId").is(userId));
        List<UserLike> userLikes = mongoTemplate.find(query, UserLike.class);
        return userLikes;
    }

    /**
     * 查询不喜欢列表
     *
     * @param userId 操作用户id
     * @return
     */
    @Override
    public List<Long> queryNotLikeList(Long userId) {
        String redisKey = getNotLikeRedisKey(userId);
        Set<Object> keys = redisTemplate.opsForHash().keys(redisKey);
        if (CollUtil.isEmpty(keys)) {
            return ListUtil.empty();
        }
        List<Long> notLikeUserIds = new ArrayList<>();
        keys.forEach(id -> notLikeUserIds.add(Convert.toLong(id)));
        return notLikeUserIds;
    }

    /**
     * 获取 喜欢数
     *
     * @param userId
     * @return
     */
    @Override
    public Integer queryLikedCount(Long userId) {
        return queryLikeList(userId).size();
    }

    /**
     * 获取被喜欢数
     *
     * @param userId
     * @return
     */
    @Override
    public Integer queryBeLikedCount(Long userId) {
        return queryBeLikeList(userId).size();
    }

    /**
     * 获取互相喜欢数
     *
     * @param userId
     * @return
     */
    @Override
    public Integer queryMatchingCount(Long userId) {
        List<Long> list = queryLikeList(userId);
        List<UserLike> userLikes = queryBeLikeList(userId);
        int count = 0;
        for (UserLike userLike : userLikes) {
            if (CollUtil.contains(list, userLike.getUserId())) {
                count++;
            }
        }


        return count;
    }

    //获取粉丝列表
    @Override
    public List<Long> queryFansList(Long id) {
        //获取粉丝列表
        List<UserLike> userLikes = queryBeLikeList(id);
        //定义粉丝id列表
        List<Long> beLikeList = new ArrayList<>();
        //遍历粉丝列表封装到粉丝id列表
        for (UserLike userLike : userLikes) {
            Long likeUserId = userLike.getUserId();
            beLikeList.add(likeUserId);
            if (likeUserId.equals(id)) {
                beLikeList.remove(likeUserId);
            }
        }
        return beLikeList;
    }

    //查询喜欢列表
    @Override
    public List<UserLike> queryLikeList2(Long id) {
        //根据查询条件查询mongoDB中喜欢列表
        Query query = Query.query(Criteria.where("userId").is(id));
        List<UserLike> list = mongoTemplate.find(query, UserLike.class);
        return list;
    }

    //获取互相喜欢列表
    @Override
    public List<Long> querylikeEachOtherIdsList(Long id) {
        //查询用户喜欢的列表
        List<UserLike> userList = queryLikeList2(id);
        //遍历取出用户喜欢的列表的friendId
        List<Long> userIdList = new ArrayList<>();
        for (UserLike users : userList) {
            userIdList.add(users.getLikeUserId());
        }
        //查询用户被喜欢列表
        List<UserLike> beLikeList = queryBeLikeList(id);
        List<Long> backIdList = new ArrayList<>();
        //遍历beLikeList对比喜欢列表中是否存在
        for (UserLike beLike : beLikeList) {
            boolean contains = CollUtil.contains(userIdList, beLike.getUserId());
            if (contains) {
                backIdList.add(beLike.getUserId());
                if (beLike.getUserId().equals(id)) {
                    backIdList.remove(beLike.getUserId());
                }
            }
        }
        return backIdList;
    }

    //重写判断用户是否喜欢ta
    @Override
    public Boolean likeUser2(Long userId, Long likeUserId) {
        //在monogoDB中判断用户喜欢关系
        Query query = new Query(Criteria.where("userId").is(userId)
                .and("likeUserId").is(likeUserId));
        List<UserLike> userLikes = mongoTemplate.find(query, UserLike.class);
        if (ObjectUtil.isNotEmpty(userLikes)) {
            return true;
        }
        return false;
    }

    //查询喜欢的数据
    @Override
    public Integer queryLikeCount2(Long id) {
        //在redis中查找是否有数据
        String likeRedisKey = "USER_LIKE_2_" + id;
        String data = redisTemplate.opsForValue().get(likeRedisKey);

        if (StrUtil.isNotEmpty(data)) {
            //return Integer.parseInt(data);
        }

        //查询mongoDB
        List<UserLike> list = queryLikeList2(id);

        //保存数据到redis中
        //redisTemplate.opsForValue().set(likeRedisKey, String.valueOf(list.size()));

        return list.size() - 1;
    }

    //查询互相喜欢数据
    @Override
    public Integer queryLikeEachOtherCount(Long id) {
        //查询用户喜欢的列表
        List<UserLike> userList = queryLikeList2(id);
        List<Long> userIdList = new ArrayList<>();
        //遍历取出用户喜欢的列表的friendId
        for (UserLike users : userList) {
            userIdList.add(users.getLikeUserId());
        }
        //查询用户被喜欢列表
        List<UserLike> beLikeList = queryBeLikeList(id);
        int count = 0;
        //遍历beLikeList对比喜欢列表中是否存在
        for (UserLike beLike : beLikeList) {
            if (CollUtil.contains(userIdList, beLike.getUserId())) {
                count++;
            }
        }
        //减去自己
        return count;
    }
}
