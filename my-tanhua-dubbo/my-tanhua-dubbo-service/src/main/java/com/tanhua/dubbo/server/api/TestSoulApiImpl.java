package com.tanhua.dubbo.server.api;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.*;
import com.tanhua.dubbo.server.vo.QuestionnaireVo;
import com.tanhua.dubbo.server.vo.SoulQuestionOptionVo;
import com.tanhua.dubbo.server.vo.SoulQuestionVo;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.ArrayList;
import java.util.List;
@Service
public class TestSoulApiImpl implements TestSoulApi {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<QuestionnaireVo> queryQuestinnaireVoList(Long id) {
        Query query1=Query.query(Criteria.where("userId").is(id.toString()));
        UserCanDoQs userCanDoQs = mongoTemplate.findOne(query1, UserCanDoQs.class);
        if (ObjectUtil.isEmpty(userCanDoQs)){
            userCanDoQs=new UserCanDoQs();
            userCanDoQs.setUserId(Convert.toStr(id));
            userCanDoQs.setId(new ObjectId());
            userCanDoQs.setLevel(1);
            userCanDoQs.setIslock(0);
            mongoTemplate.save(userCanDoQs);
        }
        List<Questionnaire> questionnaireList = mongoTemplate.findAll(Questionnaire.class);
        List<QuestionnaireVo> questionnaireVoList = new ArrayList<>();


        for (Questionnaire questionnaire : questionnaireList) {
            QuestionnaireVo questionnaireVo = new QuestionnaireVo();
            questionnaireVo.setId(questionnaire.getId().toHexString());
            String level = questionnaire.getLevel().toString();
            switch (level) {
                case "1":
                    questionnaireVo.setName("初级灵魂测试题");
                    questionnaireVo.setStar(2);
                    questionnaireVo.setIsLock(0);
                    //userCanDoQs.setLevel(2);
                    questionnaireVo.setLevel(Convert.toStr(2));
                    /*questionnaireVo.setLevel(Convert.toStr(2));*/
                    break;
                case "2":
                    questionnaireVo.setName("中级灵魂测试题");
                    questionnaireVo.setStar(3);
                    //userCanDoQs.setLevel(3);
                    questionnaireVo.setIsLock(Convert.toInt(questionnaire.getLevel()) > Convert.toInt(userCanDoQs.getLevel()) ? 1 : 0);
                    //questionnaireVo.setLevel(Convert.toStr(3));

                    break;
                case "3":
                    questionnaireVo.setName("高级灵魂测试题");
                    questionnaireVo.setStar(5);
                    questionnaireVo.setIsLock(Convert.toInt(questionnaire.getLevel()) >Convert.toInt(userCanDoQs.getLevel())? 1 : 0);
                    break;
            }
            questionnaireVo.setCover(questionnaire.getCover());
            questionnaireVo.setLevel(questionnaire.getLevel().toString());
            List<SoulQuestionVo> soulQuestionVoList = new ArrayList<>();
            Query qyery = Query.query(Criteria.where("questionnaireId").is(questionnaire.getLevel()));
            List<Questions> questionsList = mongoTemplate.find(qyery, Questions.class);
            for (Questions questions : questionsList) {
                SoulQuestionVo soulQuestionVo = new SoulQuestionVo();
                soulQuestionVo.setId(questions.getId().toHexString());
                soulQuestionVo.setQuestion(questions.getQuestionText());
                if (questionnaire.getLevel()<=2){
                    questionnaire.setLevel(questionnaire.getLevel()+1);

                }
                System.out.println(questions.getId());
                Query query = Query.query(Criteria.where("questionId").is(questions.getPId()));
                List<Options> optionsList = mongoTemplate.find(query,Options.class);
                List<SoulQuestionOptionVo> soulQuestionOptionVoList = new ArrayList<>();
                for (Options options : optionsList) {
                    SoulQuestionOptionVo soulQuestionOptionVo = new SoulQuestionOptionVo();
                    soulQuestionOptionVo.setId(options.getId().toHexString());
                    soulQuestionOptionVo.setOption(options.getOptionText());
                    soulQuestionOptionVoList.add(soulQuestionOptionVo);
                }
                soulQuestionVo.setOptions(soulQuestionOptionVoList);
                soulQuestionVoList.add(soulQuestionVo);
            }
            questionnaireVo.setQuestions(soulQuestionVoList);
            questionnaireVoList.add(questionnaireVo);
        }
        return questionnaireVoList;
    }

    @Override
    public Integer queryScore( String optionId) {
        Query query = Query.query(Criteria.where("id").is(optionId));
        Options one = mongoTemplate.findOne(query, Options.class);
        return one.getScore();
    }

    @Override
    public List<String> queryScope() {
        List<QuestionnaireResult> questionnaireResults = mongoTemplate.findAll(QuestionnaireResult.class);
        System.out.println(questionnaireResults.size());
        List<String> scopeList=new ArrayList<>();
        List<Long> questionnaireIdList=new ArrayList<>();
        for (QuestionnaireResult questionnaireResult : questionnaireResults) {
            Long questionnaireId = questionnaireResult.getQuestionnaireId();
            String scope = questionnaireResult.getScope();
            questionnaireIdList.add(questionnaireId);
            scopeList.add(scope);
        }
        return scopeList;
    }



    @Override
    public QuestionnaireResult queryResult(String scope,String questionnaireId) {

        Query query=Query.query(Criteria.where("scope").is(scope).and("questionnaireId").is(Convert.toLong(questionnaireId)));
        QuestionnaireResult result = mongoTemplate.findOne(query, QuestionnaireResult.class);
        return result;
    }

    @Override
    public String queryQuestinnaireId(String questionId) {
        Query query=Query.query(Criteria.where("id").is(questionId));
        Questions one = mongoTemplate.findOne(query, Questions.class);
       return one.getQuestionnaireId();
    }

    @Override
    public void saveResult(QuestionnaireReport questionnaireReport) {
        QuestionnaireReport save = mongoTemplate.save(questionnaireReport);
        String userId = questionnaireReport.getUserId();
        Query query2 =Query.query(Criteria.where("userId").is(userId));
        UserCanDoQs one = mongoTemplate.findOne(query2, UserCanDoQs.class);
        Update update=new Update();

        if (one.getLevel()<=2){
        update.set("level",one.getLevel()+1);
        Query query3=Query.query(Criteria.where("userId").is(userId).and("_id").is(one.getId()));
        mongoTemplate.updateFirst(query3,update,UserCanDoQs.class);}
        System.out.println(save);
    }
    @Override
    public void saveNewReport(String questionnaireId, ObjectId id) {
        Query query=Query.query(Criteria.where("level").is(Convert.toInt(questionnaireId)));
        Update update=new Update();
        update.set("reportId",id.toHexString());
        mongoTemplate.updateFirst(query,update,Questionnaire.class);
    }

    @Override
    public QuestionnaireReport queryReport(String id) {
        Query query=Query.query(Criteria.where("id").is(new ObjectId(id)));
        QuestionnaireReport questionnaireReport = mongoTemplate.findOne(query, QuestionnaireReport.class);
        return questionnaireReport;
    }

    @Override
    public QuestionnaireResult queryQuestionsResult(String resultId) {
        Query query=Query.query(Criteria.where("id").is(resultId));
        return mongoTemplate.findOne(query, QuestionnaireResult.class);
    }

    @Override
    public List<QuestionnaireReport> findAllReport() {
        List<QuestionnaireReport> all = mongoTemplate.findAll(QuestionnaireReport.class);
        return all;
    }
}
