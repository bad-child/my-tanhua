package com.tanhua.dubbo.server.api;


import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.Sound;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;


@Service
public class SoundApiImpl implements SoundApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    /*保存语音*/
    @Override
    public void saveSound(Sound sound) {
        this.mongoTemplate.save(sound);
    }

    /**
     * 根据userId查询语音
     *
     * @param userId
     * @return
     */
    public List<Sound> querySoundById(Long userId) {
        Query query = new Query(Criteria.where("userId").is(userId));
        return this.mongoTemplate.find(query, Sound.class);
    }

    /**
     * 获取所有可用的语音
     *
     * @return
     */
    public List<Sound> querySounds(Long userId, String gender) {

        Query query = Query.query(Criteria.where("status").is(1).and("gender").is(StrUtil.equals(gender,"MAN")?"WOMAN":"MAN"));
        List<Sound> sounds = mongoTemplate.find(query, Sound.class);


        return sounds;
    }

    /**
     * @param id 声音 id
     */
    @Override
    public void removeSound(ObjectId id) {
        Query query = Query.query(Criteria.where("id").is(id));
        mongoTemplate.updateFirst(query, Update.update("status", 0), Sound.class);
    }


}
