package com.tanhua.dubbo.server.api;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.RecommendUser;
import com.tanhua.dubbo.server.pojo.Visitors;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class VisitorsApiImpl implements VisitorsApi {
    @Autowired
    private MongoTemplate mongoTemplate;
    private static final String VISITOR_REDIS_KEY = "VISITOR_USER";
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 保存访客数据
     *
     * @param userId        被访问者id
     * @param visitorUserId 访客id
     * @param from          来源
     * @return
     */
    @Override
    public String saveVisitor(Long userId, Long visitorUserId, String from) {
        if (!ObjectUtil.isAllNotEmpty(userId, visitorUserId, from)) {
            return null;
        }
        //如果访客为自身,则不记录
        if (ObjectUtil.equals(userId, visitorUserId)) {
            return null;
        }
        //查询访客用户今天是否已记录过,如果已经记录过,不再记录
        String today = DateUtil.today();
        long minDate = DateUtil.parseDateTime(today + " 00:00:00").getTime();
        long maxDate = DateUtil.parseDateTime(today + " 23:59:59").getTime();
        Query query = Query.query(Criteria.where("userId").is(userId)
                .and("visitorUserId").is(visitorUserId)
                .andOperator(Criteria.where("date").gte(minDate),
                        Criteria.where("date").lte(maxDate)
                )
        );
        long count = mongoTemplate.count(query, Visitors.class);
        if (count > 0) {
            return null;
        }
        //未记录过则记录该用户
        Visitors visitors = new Visitors();
        visitors.setId(ObjectId.get());
        visitors.setUserId(userId);
        visitors.setVisitorUserId(visitorUserId);
        visitors.setFrom(from);
        visitors.setDate(System.currentTimeMillis());
        mongoTemplate.save(visitors);
        return visitors.getId().toHexString();
    }

    /**
     * 查询登录者的访客数据
     * 查询前5个访客数据,按照访问时间倒序排序
     * 如果用户已查询过该列表,记录查询时间,后续查询按照这个时间往后查询
     *
     * @param userId 登录者id
     * @return
     */
    @Override
    public List<Visitors> queryMyVisitor(Long userId) {
        //查询上一次查询列表时间
        Long date = Convert.toLong(redisTemplate.opsForHash().get(VISITOR_REDIS_KEY, String.valueOf(userId)));


        Query query = Query.query(Criteria.where("userId").is(userId)).with(Sort.by(Sort.Order.desc("date")));

        // 判断上一次查询时间,若已查询,则查询大于查询时间的数据
        if (ObjectUtil.isNotEmpty(date)) {
            query.addCriteria(Criteria.where("date").gte(date));
        }

        List<Visitors> visitorsList = mongoTemplate.find(query, Visitors.class);

        List<Visitors> visitorsListNew = new ArrayList<>();


        //获取合适的用户
        for (Visitors visitors : visitorsList) {
            if (visitorsListNew.size() == 5) {
                break;
            }

            if (!CollUtil.contains(visitorsListNew, visitors.getVisitorUserId())) {
                visitorsListNew.add(visitors);
            }
        }

        // 封装分数
        for (Visitors visitors : visitorsListNew) {
            Double score = queryScore(visitors.getVisitorUserId(), visitors.getUserId());
            visitors.setScore(score);
        }


        return visitorsListNew;
    }


    public Double queryScore(Long userId, Long toUserId) {
        Query query = Query.query(Criteria.where("userId").is(userId).and("toUserId").is(toUserId));
        RecommendUser recommendUser = mongoTemplate.findOne(query, RecommendUser.class);
        if (null != recommendUser) {
            return recommendUser.getScore();
        }
        return 70D;
    }
}
