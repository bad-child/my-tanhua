package com.tanhua.dubbo.server.api;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.Users;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsersApiImpl implements UsersApi {
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 保存好友关系
     *
     * @param userId   用户id
     * @param friendId 好友id
     * @return 好友关系id
     */
    @Override
    public String saveUsers(Long userId, Long friendId) {
        //判断用户id与好友id是否都不为空
        if (!ObjectUtil.isAllNotEmpty(userId, friendId)) {
            return null;
        }
        //判断好友关系是否存在
        Query query1 = Query.query(Criteria.where("userId").is(userId).and("friendId").is(friendId));
        long count1 = mongoTemplate.count(query1, Users.class);
        Query query2 = Query.query(Criteria.where("userId").is(friendId).and("friendId").is(userId));
        long count2 = mongoTemplate.count(query2, Users.class);
        //已互为好友则直接结束
        if (count1 > 0 && count2 > 0) {
            return ObjectId.get().toHexString();
        }
        //好友关系不是双向则数据有误
        if (count1 > 0 || count2 > 0) {
            return null;
        }
        //注册用户与好友的关系
        Users users = new Users();
        users.setId(new ObjectId());
        users.setUserId(userId);
        users.setFriendId(friendId);
        users.setDate(System.currentTimeMillis());
        mongoTemplate.save(users);
        //注册好友与用户的关系
        users.setId(new ObjectId());
        users.setUserId(friendId);
        users.setFriendId(userId);
        mongoTemplate.save(users);
        return users.getId().toHexString();
    }

    /**
     * 删除好友关系
     *
     * @param userId   用户id
     * @param friendId 好友id
     * @return
     */
    @Override
    public Boolean removeUsers(Long userId, Long friendId) {
        //删除用户与好友的关系数据
        Query query1 = Query.query(Criteria.where("userId").is(userId).and("friendId").is(friendId));
        long count1 = mongoTemplate.remove(query1, Users.class).getDeletedCount();
        //删除好友与用户的关系数据
        Query query2 = Query.query(Criteria.where("userId").is(friendId).and("friendId").is(userId));
        long count2 = mongoTemplate.remove(query2, Users.class).getDeletedCount();
        return count1 > 0 && count2 > 0;
    }

    /**
     * 根据用户id查询全部users列表
     *
     * @param userId 登录用户id
     * @return
     */
    @Override
    public List<Users> queryAllUsersList(Long userId) {
        Query query = Query.query(Criteria.where("userId").is(userId));
        return mongoTemplate.find(query, Users.class);
    }

    /**
     * 根据用户id查询users列表(分页)
     *
     * @param userId   登录用户id
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public List<Users> queryUsersList(Long userId, Integer page, Integer pageSize) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Order.desc("created")));
        Query query = Query.query(Criteria.where("userId").is(userId)).with(pageRequest);
        return mongoTemplate.find(query, Users.class);
    }

    /**
     * 根据用户id查询所有好友的id集合
     *
     * @param userId 查询者id
     * @return 好友id集合
     */
    @Override
    public List<Long> queryUsersIdListById(Long userId) {
        List<Users> usersList = queryAllUsersList(userId);
        List<Long> friendIds = new ArrayList<>();
        for (Users users : usersList) {
            friendIds.add(users.getFriendId());
        }
        return friendIds;
    }
}
