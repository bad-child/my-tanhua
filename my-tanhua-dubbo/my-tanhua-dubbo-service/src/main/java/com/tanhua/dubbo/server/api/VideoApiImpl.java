package com.tanhua.dubbo.server.api;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Opt;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.PageUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.*;
import com.tanhua.dubbo.server.vo.QuestionnaireVo;
import com.tanhua.dubbo.server.vo.SoulQuestionOptionVo;
import com.tanhua.dubbo.server.vo.SoulQuestionVo;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class VideoApiImpl implements VideoApi {
    private static final String VIDEO_FOLLOW_USER_KEY_PREFIX = "VIDEO_FOLLOW_USER_";
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 根据id查询推荐视频集合
     *
     * @param id       当前登录者id
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public List<Video> queryVideoList(Long id, Integer page, Integer pageSize) {
        //在缓存中查询推荐列表
        String redisKey = "QUANZI_VIDEO_RECOMMEND_" + id;
        String data = redisTemplate.opsForValue().get(redisKey);
        List<Long> vids = new ArrayList<>();
        int recommendCount = 0;
        if (StrUtil.isNotEmpty(data)) {
            //获取推荐视频的id
            List<String> vidList = StrUtil.split(data, ",");
            //计算分页
            int[] startEnd = PageUtil.transToStartEnd(page - 1, pageSize);
            //开始索引
            int startIndex = startEnd[0];
            //结束索引(取计算出的结束索引和推荐列表的长度里较小的值,防止空指针)
            int endIndex = Math.min(startEnd[1], vidList.size());
            for (int i = startIndex; i < endIndex; i++) {
                vids.add(Convert.toLong(vidList.get(i)));
            }
            //记录推荐列表的长度
            recommendCount = vidList.size();
        }
        if (CollUtil.isEmpty(vids)) {
            //计算推荐视频占用页数
            int totalPage = PageUtil.totalPage(recommendCount, pageSize);
            PageRequest pageRequest = PageRequest.of(page - totalPage - 1, pageSize);
            Query query = new Query().with(Sort.by(Sort.Order.desc("created"))).with(pageRequest);
            return mongoTemplate.find(query, Video.class);
        }
        Query query = Query.query(Criteria.where("vid").in(vids)).with(Sort.by(Sort.Order.desc("created")));
        return mongoTemplate.find(query, Video.class);
    }

    /**
     * 写入视频信息
     *
     * @param video
     */
    @Override
    public void saveVideo(Video video) {
        mongoTemplate.save(video);
    }

    /**
     * 关注视频发布用户
     *
     * @param userId       当前登录者id
     * @param followUserId 被关注者id
     */
    @Override
    public void followUser(Long userId, Long followUserId) {
        //已关注则不对数据库进行操作
        if (isFollowUser(userId, followUserId)) {
            return;
        }
        //封装对象
        FollowUser followUser = new FollowUser();
        followUser.setId(new ObjectId());
        followUser.setUserId(userId);
        //设置被关注用户的id
        followUser.setFollowUserId(followUserId);
        //设置关注时间
        followUser.setCreated(System.currentTimeMillis());
        //写入MongoDB
        mongoTemplate.save(followUser);
        //写入缓存
        String redisKey = getVideoFollowUserKey(userId);
        String hashKey = String.valueOf(followUserId);
        redisTemplate.opsForHash().put(redisKey, hashKey, "1");
    }

    /**
     * 取消关注视频发布用户
     *
     * @param userId       当前登录者id
     * @param followUserId 被关注者id
     */
    @Override
    public void disFollowUser(Long userId, Long followUserId) {
        //删除MongoDB中的数据
        Query query = Query.query(Criteria.where("userId").is(userId).and("followUserId").is(followUserId));
        mongoTemplate.remove(query, FollowUser.class);
        //更新缓存
        String redisKey = getVideoFollowUserKey(userId);
        String hashKey = String.valueOf(followUserId);
        redisTemplate.opsForHash().delete(redisKey, hashKey);
    }

    /**
     * 查询是否关注用户
     *
     * @param userId       当前登录者id
     * @param followUserId 被关注用户id
     * @return
     */
    @Override
    public Boolean isFollowUser(Long userId, Long followUserId) {
        //查询缓存数据
        String redisKey = getVideoFollowUserKey(userId);
        String hashKey = String.valueOf(followUserId);
        Object data = redisTemplate.opsForHash().get(redisKey, hashKey);
        /*if (ObjectUtil.isNotEmpty(data)) {
            return StrUtil.equals(String.valueOf(data), "1");
        }*/
        Query query = Query.query(Criteria.where("userId").is(userId).and("followUserId").is(followUserId));
        FollowUser followUser = mongoTemplate.findOne(query, FollowUser.class);
        return followUser != null;
    }

    private String getVideoFollowUserKey(Long userId) {
        return VIDEO_FOLLOW_USER_KEY_PREFIX + userId;
    }
}
