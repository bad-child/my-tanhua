package com.tanhua.dubbo.server.api;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.RecommendUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecommendApiImpl implements RecommendApi {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private UsersApi usersApi;
    @Autowired
    private UserLikeApi userLikeApi;

    /**
     * 根据id查询推荐用户
     *
     * @param toUserId 登录者id
     * @return
     */
    @Override
    public RecommendUser findTodayBest(Long toUserId) {
        Query query = Query.query(Criteria.where("toUserId").is(toUserId)).with(Sort.by(Sort.Order.desc("score"))).limit(1);
        return mongoTemplate.findOne(query, RecommendUser.class);
    }

    /**
     * 根据id查询推荐用户集合
     *
     * @param toUserId 登录者id
     * @return
     */
    @Override
    public List<RecommendUser> findRecommendation(Long toUserId) {
        //设置总记录数的查询条件
        Query query = Query.query(Criteria.where("toUserId").is(toUserId));
        return mongoTemplate.find(query, RecommendUser.class);
    }

    /**
     * 查询好友的缘分值
     *
     * @param userId   好友id
     * @param toUserId 登录者id
     * @return
     */
    @Override
    public Double queryScore(Long userId, Long toUserId) {
        Query query = Query.query(Criteria.where("userId").is(userId).and("toUserId").is(toUserId));
        RecommendUser recommendUser = mongoTemplate.findOne(query, RecommendUser.class);
        if (null != recommendUser) {
            return recommendUser.getScore();
        }
        return 70D;
    }

    /**
     * 探花-查询推荐列表
     *
     * @param id 登录者id
     * @return
     */
    @Override
    public List<RecommendUser> queryCardsList(Long id) {
        //设置分页及排序规则
        PageRequest pageRequest = PageRequest.of(0, 50, Sort.by(Sort.Order.desc("score")));
        List<Long> userIds = new ArrayList<>();
        //排除以喜欢用户
        userIds.addAll(userLikeApi.queryLikeList(id));
        //排除不喜欢用户
        userIds.addAll(userLikeApi.queryNotLikeList(id));
        //排除好友
        userIds.addAll(usersApi.queryUsersIdListById(id));
        //执行查询
        Criteria criteria = Criteria.where("toUserId").is(id);
        if (CollUtil.isNotEmpty(userIds)) {
            criteria.andOperator(Criteria.where("userId").nin(userIds));
        }
        Query query = Query.query(criteria).with(pageRequest);
        return mongoTemplate.find(query, RecommendUser.class);
    }
}
