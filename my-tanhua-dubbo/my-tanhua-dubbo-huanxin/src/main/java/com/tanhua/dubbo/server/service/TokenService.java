package com.tanhua.dubbo.server.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.tanhua.dubbo.server.config.HuanXinConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class TokenService {
    @Autowired
    private HuanXinConfig huanXinConfig;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    private static final String REDIS_KEY = "HX_TOKEN";

    /**
     * 获取token
     * 先从redis中获取
     * 没有则去环信接口获取
     *
     * @return token
     */
    public String getToken() {
        //从redis中获取token
        String token = redisTemplate.opsForValue().get(REDIS_KEY);
        if (StrUtil.isNotEmpty(token)) {
            return token;
        }
        //访问环信接口获取token
        return refreshToken();
    }

    /**
     * 刷新token
     * 访问环信接口,将token存储到redis中
     *
     * @return token
     */
    public String refreshToken() {
        //环信接口URL
        String targetUrl = huanXinConfig.getUrl() +
                huanXinConfig.getOrgName() + "/" +
                huanXinConfig.getAppName() + "/token";
        //环信接口规定参数
        Map<String, Object> param = new HashMap<>();
        param.put("grant_type", "client_credentials");
        param.put("client_id", huanXinConfig.getClientId());
        param.put("client_secret", huanXinConfig.getClientSecret());
        HttpResponse response = HttpRequest.post(targetUrl)//设置请求方式和url
                .body(JSONUtil.toJsonStr(param))//设置请求体
                .timeout(20000)//设置请求超时时间
                .execute();//执行请求
        //请求失败返回null
        if (!response.isOk()) {
            return null;
        }

        //请求成功则解析响应体获取token及有效时间
        String jsonBody = response.body();
        JSONObject jsonObject = JSONUtil.parseObj(jsonBody);
        String token = jsonObject.getStr("access_token");
        //token不为空则存入redis
        if (StrUtil.isNotEmpty(token)) {
            long timeout = jsonObject.getLong("expires_in") - 3600;
            redisTemplate.opsForValue().set(REDIS_KEY, token, timeout, TimeUnit.SECONDS);
            return token;
        }
        return null;
    }
}
