package com.tanhua.dubbo.server.api;

import cn.hutool.core.util.IdUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.Method;
import cn.hutool.json.JSONException;
import cn.hutool.json.JSONUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.dubbo.server.config.HuanXinConfig;
import com.tanhua.dubbo.server.enums.HuanXinMessageType;
import com.tanhua.dubbo.server.mapper.HuanXinUserMapper;
import com.tanhua.dubbo.server.pojo.HuanXinUser;
import com.tanhua.dubbo.server.service.RequestService;
import com.tanhua.dubbo.server.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

@Service
public class HuanXinApiImpl implements HuanXinApi {
    @Autowired
    private TokenService tokenService;
    @Autowired
    private HuanXinConfig huanXinConfig;
    @Autowired
    private RequestService requestService;
    @Autowired
    private HuanXinUserMapper huanXinUserMapper;

    /**
     * 获取环信token(管理员权限)
     *
     * @return token
     */
    @Override
    public String getToken() {
        return tokenService.getToken();
    }

    /**
     * 注册环信用户
     *
     * @param userId 用户id
     * @return
     */
    @Override
    public Boolean register(Long userId) {
        String targetUrl = huanXinConfig.getUrl()
                + huanXinConfig.getOrgName() + "/"
                + huanXinConfig.getAppName() + "/users";
        HuanXinUser huanXinUser = new HuanXinUser();
        //设置用户名
        huanXinUser.setUsername("HX_" + userId);
        //设置密码(随机生成)
        huanXinUser.setPassword(IdUtil.simpleUUID());
        //执行请求
        HttpResponse response = requestService.execute(targetUrl, JSONUtil.toJsonStr(huanXinUser), Method.POST);
        //请求成功则将环信的账号信息
        if (response.isOk()) {
            huanXinUser.setUserId(userId);
            huanXinUser.setCreated(new Date());
            huanXinUser.setUpdated(huanXinUser.getCreated());
            huanXinUserMapper.insert(huanXinUser);
            return true;
        }
        return false;
    }

    /**
     * 根据用户id查询环信账户信息
     *
     * @param userId 用户id
     * @return
     */
    @Override
    public HuanXinUser queryHuanXinUser(Long userId) {
        LambdaQueryWrapper<HuanXinUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(HuanXinUser::getUserId, userId);
        return huanXinUserMapper.selectOne(wrapper);
    }

    /**
     * 根据环信用户名查询用户信息
     *
     * @param userName 用户名(HX_1)
     * @return
     */
    @Override
    public HuanXinUser queryUserByUserName(String userName) {
        LambdaQueryWrapper<HuanXinUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(HuanXinUser::getUsername, userName);
        return huanXinUserMapper.selectOne(wrapper);
    }

    /**
     * 添加好友关系(双向)
     *
     * @param userId   用户id
     * @param friendId 好友id
     * @return
     */
    @Override
    public Boolean addUserFriend(Long userId, Long friendId) {
        String targetUrl = huanXinConfig.getUrl()
                + huanXinConfig.getOrgName() + "/"
                + huanXinConfig.getAppName() + "/users/HX_"
                + userId + "/contacts/users/HX_" + friendId;
        //请求环信接口
        return requestService.execute(targetUrl, null, Method.POST).isOk();
    }

    /**
     * 删除好友关系(双向)
     *
     * @param userId   用户id
     * @param friendId 好友id
     * @return
     */
    @Override
    public Boolean removeUserFriend(Long userId, Long friendId) {
        String targetUrl = huanXinConfig.getUrl()
                + huanXinConfig.getOrgName() + "/"
                + huanXinConfig.getAppName() + "/users/HX_"
                + userId + "/contacts/users/HX_" + friendId;
        //请求环信接口
        return requestService.execute(targetUrl, null, Method.DELETE).isOk();
    }

    /**
     * 以管理员身份发送消息
     *
     * @param targetUserName     发送目标的用户名
     * @param huanXinMessageType 消息类型
     * @param msg
     * @return
     */
    @Override
    public Boolean sendMsgFromAdmin(String targetUserName, HuanXinMessageType huanXinMessageType, String msg) {
        String targetUrl = huanXinConfig.getUrl()
                + huanXinConfig.getOrgName() + "/"
                + huanXinConfig.getAppName() + "/messages";
        try {
            String body = JSONUtil.createObj().set("target_type", "users")
                    .set("target", JSONUtil.createArray().set(targetUserName))
                    .set("msg", JSONUtil.createObj()
                            .set("type", huanXinMessageType.getType())
                            .set("msg", msg))
                    .toString();
            return requestService.execute(targetUrl, body, Method.POST).isOk();
        } catch (JSONException e) {
            return false;
        }
    }

    /**
     * 添加用户黑名单
     *
     * @param userId  操作者id
     * @param blackId 被拉黑者id
     * @return
     */
    @Override
    public Boolean addUserBlacklist(Long userId, Long blackId) {
        String targetUrl = huanXinConfig.getUrl()
                + huanXinConfig.getOrgName() + "/"
                + huanXinConfig.getAppName() + "/users/HX_"
                + userId + "/blocks/users";
        try {
            String body = JSONUtil.createObj().set("usernames", JSONUtil.createArray().set("HX_" + blackId)).toString();
            return requestService.execute(targetUrl, body, Method.POST).isOk();
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 移除黑名单
     *
     * @param userId  操作者id
     * @param blackId 被拉黑者id
     * @return
     */
    @Override
    public Boolean removeBlacklist(Long userId, Long blackId) {
        String targetUrl = huanXinConfig.getUrl()
                + huanXinConfig.getOrgName() + "/"
                + huanXinConfig.getAppName() + "/users/HX_"
                + userId + "/blocks/users/HX_" + blackId;
        return requestService.execute(targetUrl, null, Method.DELETE).isOk();
    }
}
