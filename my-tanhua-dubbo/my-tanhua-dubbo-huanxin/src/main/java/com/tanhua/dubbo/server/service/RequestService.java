package com.tanhua.dubbo.server.service;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.Method;
import com.tanhua.dubbo.server.exception.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

@Service
public class RequestService {
    @Autowired
    private TokenService tokenService;

    /**
     * 通用的发送请求方法
     *
     * @param url    请求地址
     * @param body   请求参数
     * @param method 请求方法
     * @return
     */
    @Retryable(value = UnauthorizedException.class, maxAttempts = 5,
            backoff = @Backoff(delay = 2000L, multiplier = 2))
    public HttpResponse execute(String url, String body, Method method) {
        //获取环信token
        String token = tokenService.getToken();
        HttpRequest httpRequest;
        switch (method) {
            case POST:
                httpRequest = HttpRequest.post(url);
                break;
            case DELETE:
                httpRequest = HttpRequest.delete(url);
                break;
            case PUT:
                httpRequest = HttpRequest.put(url);
                break;
            case GET:
                httpRequest = HttpRequest.get(url);
                break;
            default:
                return null;
        }
        HttpResponse response = httpRequest
                .header("Content-Type", "application/json")//设置请求内容类型
                .header("Authorization", "Bearer " + token)//设置token
                .body(body)//设置请求数据
                .timeout(20000)//超时时间
                .execute();//执行请求
        if (response.getStatus() == 401) {
            //token失效,重新刷新token
            tokenService.refreshToken();
            //抛出异常
            throw new UnauthorizedException(url, body, method);
        }
        return response;
    }

    @Recover//全部重试失败后执行
    public HttpResponse recover(UnauthorizedException e) {
        System.out.println("获取token失败！url = " + e.getUrl() +
                ", body = " + e.getBody() +
                ", method = " + e.getMethod().toString());
        return null;
    }

}
