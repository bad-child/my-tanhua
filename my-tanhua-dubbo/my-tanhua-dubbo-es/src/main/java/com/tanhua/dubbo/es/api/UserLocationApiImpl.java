package com.tanhua.dubbo.es.api;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.api.UserLocationApi;
import com.tanhua.dubbo.server.pojo.UserLocation;
import com.tanhua.dubbo.server.vo.UserLocationVo;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.GeoDistanceQueryBuilder;
import org.elasticsearch.search.sort.GeoDistanceSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.*;
import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserLocationApiImpl implements UserLocationApi {
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * 初始化索引库
     */
    @PostConstruct//只在初始化servlet时自动执行一次
    public void initIndex() {
        //判断索引库是否存在,如果不存在,需要创建
        if (!elasticsearchTemplate.indexExists("tanhua")) {
            elasticsearchTemplate.createIndex(UserLocation.class);
        }
        //判断表是否存在,如果不存在,需要创建
        if (!elasticsearchTemplate.typeExists("tanhua", "user_location")) {
            elasticsearchTemplate.putMapping(UserLocation.class);
        }
    }

    /**
     * 更新用户地理位置
     *
     * @param userId    用户id
     * @param longitude 经度
     * @param latitude  纬度
     * @param address   地址名称
     * @return
     */
    @Override
    public Boolean updateUserLocation(Long userId, Double longitude, Double latitude, String address) {
        try {
            //查询个人的地理位置
            GetQuery query = new GetQuery();
            query.setId(String.valueOf(userId));
            UserLocation userLocation = elasticsearchTemplate.queryForObject(query, UserLocation.class);
            if (ObjectUtil.isEmpty(userLocation)) {
                //如果不存在,需要新增
                userLocation = new UserLocation();
                userLocation.setUserId(userId);
                userLocation.setAddress(address);
                userLocation.setCreated(System.currentTimeMillis());
                userLocation.setUpdated(userLocation.getCreated());
                //                       new GeoPoint(  纬度   ,   经度  )
                userLocation.setLocation(new GeoPoint(latitude, longitude));
                IndexQuery indexQuery = new IndexQueryBuilder().withObject(userLocation).build();
                //保存数据到ES中
                elasticsearchTemplate.index(indexQuery);
            } else {
                //如果存在数据,更新数据
                Map<String, Object> map = new HashMap<>();
                map.put("location", new GeoPoint(latitude, longitude));
                map.put("updated", System.currentTimeMillis());
                map.put("lastupdated", userLocation.getUpdated());
                map.put("address", address);
                UpdateRequest updateRequest = new UpdateRequest();
                updateRequest.doc(map);
                UpdateQuery updateQuery = new UpdateQueryBuilder()
                        .withId(String.valueOf(userId))
                        .withClass(UserLocation.class)
                        .withUpdateRequest(updateRequest).build();
                //更新数据
                elasticsearchTemplate.update(updateQuery);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 查询用户地理位置
     *
     * @param userId 被查询者用户id
     * @return
     */
    @Override
    public UserLocationVo queryByUserId(Long userId) {
        GetQuery query = new GetQuery();
        query.setId(String.valueOf(userId));
        UserLocation userLocation = elasticsearchTemplate.queryForObject(query, UserLocation.class);
        if (ObjectUtil.isNotEmpty(userLocation)) {
            return UserLocationVo.format(userLocation);
        }
        return null;
    }

    /**
     * 根据位置搜索附近
     *
     * @param longitude 经度
     * @param latitude  纬度
     * @param distance  距离
     * @return
     */
    @Override
    public List<UserLocationVo> queryUserFromLocation(Double longitude, Double latitude, Double distance) {
        String fieldName = "location";
        NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        //以点为中心,指定范围查询
        GeoDistanceQueryBuilder geoDistanceQueryBuilder = new GeoDistanceQueryBuilder(fieldName);
        //中心点
        geoDistanceQueryBuilder.point(new GeoPoint(latitude, longitude));
        //距离(半径)                                                    (单位 : 公里)
        geoDistanceQueryBuilder.distance(distance / 1000, DistanceUnit.KILOMETERS);
        boolQueryBuilder.must(geoDistanceQueryBuilder);
        searchQueryBuilder.withQuery(boolQueryBuilder);
        //排序
        GeoDistanceSortBuilder geoDistanceSortBuilder = new GeoDistanceSortBuilder(fieldName, latitude, longitude);
        geoDistanceSortBuilder.order(SortOrder.ASC);//由近到远
        geoDistanceSortBuilder.unit(DistanceUnit.KILOMETERS);//设置单位
        searchQueryBuilder.withSort(geoDistanceSortBuilder);
        //执行查询
        List<UserLocation> userLocationList = elasticsearchTemplate.queryForList(searchQueryBuilder.build(), UserLocation.class);
        //将UserLocation集合转换为UserLocationVo集合
        return UserLocationVo.formatToList(userLocationList);
    }
}
