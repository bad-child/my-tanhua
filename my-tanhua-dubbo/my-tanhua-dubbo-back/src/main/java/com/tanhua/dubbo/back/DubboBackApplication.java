package com.tanhua.dubbo.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

@SpringBootApplication //排除mongo的自动配置
public class DubboBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboBackApplication.class, args);
    }
}