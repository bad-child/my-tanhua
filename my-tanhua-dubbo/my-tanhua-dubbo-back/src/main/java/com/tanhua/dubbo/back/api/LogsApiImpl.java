package com.tanhua.dubbo.back.api;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.api.LogsApi;
import com.tanhua.dubbo.server.pojo.UserActiveRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class LogsApiImpl implements LogsApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 日志翻页
     *
     * @param page
     * @param pageSize
     * @param sortProp
     * @param sortOrder
     * @param type
     * @param userId
     * @return
     */
    @Override
    public List<UserActiveRecord> queryLogsList(Integer page, Integer pageSize, String sortProp, String sortOrder, String type, String userId) {

        // 分页
        Pageable pageAble = PageRequest.of(page - 1, pageSize);

        // 指定用户id
        Query query = new Query(Criteria
                .where("userId").is(Convert.toLong(userId))
                .and("isLogin").is(1L))
                .with(pageAble);

        // 指定排序字段和规则
        if (StrUtil.equals("ascending", sortOrder)) {
            query.with(Sort.by(Sort.Order.asc("activeTime")));
        } else {
            query.with(Sort.by(Sort.Order.desc("activeTime")));
        }
        List<UserActiveRecord> userActiveRecordList = mongoTemplate.find(query, UserActiveRecord.class);
        return userActiveRecordList;
    }

    @Override
    public List<UserActiveRecord> queryAllLogs(String userId) {
        Query query = new Query(Criteria
                .where("userId").is(Convert.toLong(userId))
                .and("isLogin").is(1L));
        List<UserActiveRecord> userActiveRecordList = mongoTemplate.find(query, UserActiveRecord.class);
        return userActiveRecordList;

    }
}
