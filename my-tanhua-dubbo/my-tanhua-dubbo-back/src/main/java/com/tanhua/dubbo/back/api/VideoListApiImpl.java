package com.tanhua.dubbo.back.api;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.api.VideoListApi;
import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.pojo.Video;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class VideoListApiImpl implements VideoListApi {


    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 视频记录翻页
     *
     * @param page
     * @param pageSize
     * @param userId
     * @return
     */
    @Override
    public List<Video> queryVideoList(Integer page, Integer pageSize, String userId) {

        // 默认给10个,按时间降序
        Query query = new Query()
                .with(PageRequest.of(page - 1,
                        pageSize, Sort.by(Sort.Order.desc("created"))));

        // 指定排序字段和排序规则
/*        if (StrUtil.equals("ascending ", sortOrder)) {
            query.with(Sort.by(Sort.Order.asc("created")));
        } else {
            query.with(Sort.by(Sort.Order.desc("created")));
        }*/

        if (StringUtils.isNotBlank(userId)) {
            query.addCriteria(Criteria.where("userId").is(Convert.toLong(userId)));
        }

        List<Video> videoList = mongoTemplate.find(query, Video.class);
        return videoList;

    }

    /**
     * 不分页查询用户的video
     *
     * @param userId
     * @return
     */
    @Override
    public List<Video> queryAllVideo(String userId) {
        // 指定用户的视频列表
        Query query = new Query(Criteria
                .where("userId").is(Convert.toLong(userId)));

        List<Video> videoList = mongoTemplate.find(query, Video.class);
        return videoList;

    }

    @Override
    public Long queryCommentCount(String publishId, CommentType commentType) {

        Query query = Query.query(Criteria.where("publishId").is(new ObjectId(publishId))
                .and("commentType").is(commentType.getType()));
        long count = mongoTemplate.count(query, Comment.class);

        return count;
    }
}
