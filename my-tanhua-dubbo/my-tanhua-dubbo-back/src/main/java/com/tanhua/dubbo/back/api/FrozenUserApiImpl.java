package com.tanhua.dubbo.back.api;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.api.FrozenUserApi;
import com.tanhua.dubbo.server.pojo.FrozenUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.core.RedisTemplate;

import java.time.Duration;

@Service
public class FrozenUserApiImpl implements FrozenUserApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 冻结用户
     *
     * @param frozenUser
     * @return
     */
    @Override
    public Boolean frozenOneUser(FrozenUser frozenUser) {

        try {
            String frozenKey = getFrozenKey(frozenUser.getUserId());
            Integer frozenRange = frozenUser.getFreezingRange();
            Integer frozenTime = frozenUser.getFreezingTime();

            // 将用户id与用户范围存入 redis,并设置失效时间(冻结时间)

            // 10s
            if (ObjectUtil.equals(frozenTime,1)){
                redisTemplate.opsForValue().set(frozenKey, String.valueOf(frozenRange), Duration.ofSeconds(Convert.toLong(10)));
            }
            // 30s
            if (ObjectUtil.equals(frozenTime,2)){
                redisTemplate.opsForValue().set(frozenKey, String.valueOf(frozenRange), Duration.ofSeconds(Convert.toLong(30)));
            }
            if (ObjectUtil.equals(frozenTime,3)){
                redisTemplate.opsForValue().set(frozenKey, String.valueOf(frozenRange));
            }

            frozenUser.setCreateTime(System.currentTimeMillis());
            // 记录到 mongo 中
            mongoTemplate.save(frozenUser);

            return true;

        } catch (Exception e) {
            return false;
        }

    }


    /**
     * 获取用户信息
     */
    public FrozenUser getFrozenUser(Long userId) {

        Query query=Query.query(Criteria.where("userId").is(userId)).with(Sort.by(Sort.Order.desc("createTime"))).limit(1);
        FrozenUser frozenUser = mongoTemplate.findOne(query, FrozenUser.class);


        return frozenUser;
    }


    /**
     * 获取用户状态
     *
     * @param userId
     * @return 返回用户冻结范围
     */
    public String getStatus(Long userId) {
        String frozenKey = getFrozenKey(userId);
        String value = redisTemplate.opsForValue().get(frozenKey);

        return value;
    }

    /**
     * 解冻用户
     *
     * @param userId 用户 id
     * @param reason 原因
     * @return
     */
    @Override
    public Boolean unFrozenOneUser(Integer userId, String reason) {

        String frozenKey = getFrozenKey(userId);

        // 删除 redis 中数据
        Boolean result = redisTemplate.delete(frozenKey);

        //TODO 若为 false  直接返回 false

        // 获取 mongo 中的该条记录
        Query query = Query.query(Criteria.where("userId").is(userId)).with(Sort.by(Sort.Order.desc("createTime"))).limit(1);
        FrozenUser frozenUser = mongoTemplate.findOne(query, FrozenUser.class);

        // 无该数据,直接返回
        if (ObjectUtil.isEmpty(frozenUser)) {
            return false;
        }

        // 修改解冻原因
        frozenUser.setReasonsForUnFreezing(reason);
        // 更新记录
        Query newQuery = Query.query(Criteria.where("id").is(frozenUser.getId()));

        mongoTemplate.updateFirst(newQuery, Update.update("reasonsForUnFreezing", reason), FrozenUser.class);


        return result;

    }

    /**
     * 获取 redis key
     *
     * @param userId
     * @return
     */
    public String getFrozenKey(Object userId) {
        return "FROZEN_" + userId;
    }

}
