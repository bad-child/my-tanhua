package com.tanhua.dubbo.back.api;

import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.api.CommentApi;
import lombok.val;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class CommentApiImpl implements CommentApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 评论列表翻页
     *
     * @param sortOrder 排序规则  ascending 升序 descending 降序
     * @param publishId
     * @return
     */
    @Override
    public List<Comment> queryCommentListByPage(Integer page, Integer pageSize, String sortProp, String sortOrder, String publishId) {

        // 指定动态id
        Query query = new Query(Criteria
                .where("publishId").is(new ObjectId(publishId))
                .and("commentType").is(2))
                .with(PageRequest.of(page - 1, pageSize));

        // 指定排序字段和规则
        if (StrUtil.equals("ascending ", sortOrder)) {
            query.with(Sort.by(Sort.Order.asc("created")));
        } else {
            query.with(Sort.by(Sort.Order.desc("created")));
        }

        List<Comment> commentList = mongoTemplate.find(query, Comment.class);
        return commentList;
    }

    @Override
    public List<Comment> queryAllCommentList(String publishId) {
        Query query = new Query(Criteria
                .where("commentType").is(2)
                .and("publishId").is(new ObjectId(publishId)));

        List<Comment> commentList = mongoTemplate.find(query, Comment.class);
        return commentList;
    }
}
