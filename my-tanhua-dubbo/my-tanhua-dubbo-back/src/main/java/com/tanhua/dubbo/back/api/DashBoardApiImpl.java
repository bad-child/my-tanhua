package com.tanhua.dubbo.back.api;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.api.DashBoardApi;
import com.tanhua.dubbo.server.pojo.UserActiveRecord;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class DashBoardApiImpl implements DashBoardApi {
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 查询时间内的活跃用户数
     *
     * @param startOfMonth 开始时间
     * @param endOfMonth   结束时间
     * @param isLogin      是否为登陆操作 1为登录操作 0为非登录操作
     * @return 活跃人数
     */
    @Override
    public Integer countTimeActiveUsers(Long startOfMonth, Long endOfMonth, Long isLogin) {
        //查询时间范围内的登录记录
        Query query = Query.query(Criteria.where("isLogin").is(isLogin)
                .andOperator(Criteria.where("activeTime").gte(startOfMonth), Criteria.where("activeTime").lte(endOfMonth)));
        List<UserActiveRecord> userActiveRecordList = mongoTemplate.find(query, UserActiveRecord.class);
        //去除重复
        Set<Long> activeUserSet = new HashSet<>();
        for (UserActiveRecord userActiveRecord : userActiveRecordList) {
            activeUserSet.add(userActiveRecord.getUserId());
        }
        return activeUserSet.size();
    }

    /**
     * 查询次日活跃人数
     *
     * @param userActiveRecordList 注册用户集合
     * @return 次日活跃人数
     */
    @Override
    public Integer countNextDayUsers(List<UserActiveRecord> userActiveRecordList) {
        //记录次日活跃人数
        Integer nextDayCount = 0;
        //遍历注册用户集合
        for (UserActiveRecord userActiveRecord : userActiveRecordList) {
            //注册时间
            Date date = new Date(userActiveRecord.getActiveTime());
            //注册时间次日开始时间
            long nextDayStartTime = DateUtil.beginOfDay(DateUtil.offsetDay(date, 1)).getTime();
            //注册时间次日结束时间
            long nextDayEndTime = DateUtil.endOfDay(DateUtil.offsetDay(date, 1)).getTime();
            //查询用户是否在次日活跃过
            Query query = Query.query(Criteria.where("userId").is(userActiveRecord.getUserId())
                    .andOperator(Criteria.where("activeTime").gte(nextDayStartTime),
                            Criteria.where("activeTime").lte(nextDayEndTime)));
            long count = mongoTemplate.count(query, UserActiveRecord.class);
            //活跃过则计数加一
            if (count > 0) {
                nextDayCount++;
            }
        }
        return nextDayCount;
    }

    /**
     * 用户进行操作时记录用户活跃
     *
     * @param userId  登录用户id
     * @param isLogin 是否为登陆操作 1为登录,0为非登录
     */
    @Override
    public void saveUserActive(Long userId, Long isLogin) {
        //获取当前时间
        Date date = new Date();
        //获取当日开始时间
        DateTime beginOfDay = DateUtil.beginOfDay(date);
        //查询当日是否已记录活跃
        PageRequest pageRequest = PageRequest.of(0, 1, Sort.by(Sort.Order.desc("activeTime")));
        Query query = Query.query(Criteria.where("userId").is(userId).and("activeTime").gte(beginOfDay.getTime())).with(pageRequest);
        List<UserActiveRecord> userActiveRecordList = mongoTemplate.find(query, UserActiveRecord.class);
        //当日未记录则进行记录
        UserActiveRecord userActiveRecord = new UserActiveRecord();
        userActiveRecord.setId(ObjectId.get());
        userActiveRecord.setUserId(userId);
        userActiveRecord.setActiveTime(System.currentTimeMillis());
        userActiveRecord.setIsLogin(isLogin);
        //记录数不为0则当日已记录过
        if (CollUtil.isNotEmpty(userActiveRecordList)) {
            //超过5分钟则视为用户不在使用(超过5分钟则记录5分钟)
            userActiveRecord.setUseTime(Math.min(userActiveRecord.getActiveTime() - userActiveRecordList.get(0).getActiveTime(), 5 * 60 * 1000L));
        } else {
            //为空则使用时间为0
            userActiveRecord.setUseTime(0L);
        }
        mongoTemplate.save(userActiveRecord);
    }

    /**
     * 查询过去天数的活跃用户数
     *
     * @param day     天数
     * @param isLogin 是否为登陆操作 1为登录操作 0为非登录操作
     * @return
     */
    @Override
    public Integer countActivePass(Integer day, Long isLogin) {
        //计算开始时间(天数前日期的开始时间)
        long startTime = DateUtil.beginOfDay(DateUtil.offsetDay(DateUtil.date(), -day)).getTime();
        //计算结束时间(当前时间)
        long endTime = DateUtil.date().getTime();
        //查询时间范围内的活跃人数
        return countTimeActiveUsers(startTime, endTime, isLogin);
    }

    /**
     * 查询今日的活跃用户数
     *
     * @param isLogin 是否为登陆操作 1为登录操作 0为非登录操作
     * @return
     */
    @Override
    public Integer countActiveToday(Long isLogin) {
        //计算开始时间
        long startTime = DateUtil.beginOfDay(DateUtil.date()).getTime();
        //计算结束时间
        long endTime = DateUtil.endOfDay(DateUtil.date()).getTime();
        //查询时间范围内的活跃人数
        return countTimeActiveUsers(startTime, endTime, isLogin);
    }

    /**
     * 查询过去天数使用时长
     *
     * @param day 天数
     * @return 总使用时长
     */
    @Override
    public Long queryUseTimePass(Integer day) {
        Long useTimeTotal = 0L;
        //计算开始时间(天数前日期的开始时间)
        long startTime = DateUtil.beginOfDay(DateUtil.offsetDay(DateUtil.date(), -day)).getTime();
        //计算结束时间(当前时间)
        long endTime = DateUtil.date().getTime();
        //获取过去天数活跃用户id的集合
        Query query = Query.query(Criteria.where("activeTime").gte(startTime)
                .andOperator(Criteria.where("activeTime").lte(endTime)));
        List<UserActiveRecord> userActiveRecordList = mongoTemplate.find(query, UserActiveRecord.class);
        for (UserActiveRecord userActiveRecord : userActiveRecordList) {
            //将每个记录的使用时长相加
            useTimeTotal += userActiveRecord.getUseTime();
        }
        return useTimeTotal;
    }

    /**
     * 查询时间内的登录次数
     *
     * @param startOfMonth 开始时间
     * @param endOfMonth   结束时间
     * @return 登录次数
     */
    @Override
    public Integer countTimeLoginUsers(Long startOfMonth, Long endOfMonth) {
        //查询时间范围内的登录记录数
        Query query = Query.query(Criteria.where("isLogin").is(1L)
                .andOperator(Criteria.where("activeTime").gte(startOfMonth), Criteria.where("activeTime").lte(endOfMonth)));
        return Convert.toInt(mongoTemplate.count(query, UserActiveRecord.class));
    }

    /**
     * 查询登录用户的记录集合
     * @param userId
     * @return
     */
    @Override
    public List<UserActiveRecord> queryLoginUser(Long userId) {

        Query query = Query.query(Criteria
                .where("userId").is(userId)
                .and("isLogin").is(1L))
                .with(Sort.by(Sort.Order.desc("activeTime")));
        List<UserActiveRecord> userActiveRecordList = mongoTemplate.find(query, UserActiveRecord.class);
        return userActiveRecordList;
    }

    @Override
    public Long getLastActiveTime(Long userId) {
        Query query=Query.query(Criteria.where("userId").is(userId)).with(Sort.by(Sort.Order.desc("activeTime"))).limit(1);;
        UserActiveRecord userActiveRecord = mongoTemplate.findOne(query, UserActiveRecord.class);

        if (ObjectUtil.isEmpty(userActiveRecord)){
            return null;
        }

        return userActiveRecord.getActiveTime();
    }

}
