package com.tanhua.dubbo.back.api;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.mongodb.client.result.DeleteResult;
import com.tanhua.dubbo.server.api.MessagesApi;
import com.tanhua.dubbo.server.enums.StatusEnum;
import com.tanhua.dubbo.server.pojo.Publish;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

@Service
public class MessagesApiImpl implements MessagesApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    //redis自定义字段key
    private static final String PUBLISH_REDIS_KEY_PREFIX = "PUBLISH_STATUE_KEY";

    //redis自定义status字段value
    private static final String PUBLISH_STATUE_KEY_PREFIX = "PUBLISH_STATUE";

    //redis自定义top字段value
    private static final String PUBLISH_TOP_KEY_PREFIX = "PUBLISH_TOP";

    //获取redis自定义字段key
    private String getPublishRedisKeyPrefix(String publishId) {
        return PUBLISH_REDIS_KEY_PREFIX + publishId;
    }

    //获取redis自定义status字段value
    private String getPublishStatueKeyPrefix(String publishId) {
        return PUBLISH_STATUE_KEY_PREFIX + publishId;
    }

    //获取redis自定义top字段value
    private String getPublishTopKeyPrefix(String publishId) {
        return PUBLISH_TOP_KEY_PREFIX + publishId;
    }

    /**
     * 从publish表中,删除对应的数据
     *
     * @param idList
     * @return
     */
    @Override
    public Boolean updatePublish(List<String> idList) {
        if (CollUtil.isEmpty(idList)){
            return false;
        }
        //匹配条件
        for (String id : idList) {
            Publish publish = new Publish();
            Query query=Query.query(Criteria.where("id").is(id));
            publish.setStatus(StatusEnum.DSH.getValue());
            Update update=Update.update("status",publish.getStatus());
            long matchedCount = mongoTemplate.updateFirst(query, update, Publish.class).getMatchedCount();
            if (matchedCount==0){
                return false;
            }
        }
        return true;
    }

    //消息(动态审核)是否通过
    public Boolean isMessagesPass(Publish publish) {
        //查询redis中的字段
        String redisKey = getPublishRedisKeyPrefix(StrUtil.toString(publish.getId()));
        String passStatus = getPublishStatueKeyPrefix(StrUtil.toString(publish.getId()));
        Object date = redisTemplate.opsForHash().get(redisKey, passStatus);
        //判断redis中是否有数据
        if (ObjectUtil.isNotEmpty(date)) {
            return StrUtil.equals(Convert.toStr(date), "5");
        }

        //查询mongoDB中动态状态
        if (publish.getStatus() != 5) {
            return false;
        }

        //保存到redis中
        redisTemplate.opsForHash().put(redisKey, passStatus, "5");
        return true;
    }

    //消息是否置顶
    public Boolean isMessagesTop(Publish publish) {
        //查询redis中的字段
        String redisKey = getPublishRedisKeyPrefix(StrUtil.toString(publish.getId()));
        String topStatus = getPublishTopKeyPrefix(StrUtil.toString(publish.getId()));
        Object date = redisTemplate.opsForHash().get(redisKey, topStatus);
        //判断redis中是否有数据
        if (ObjectUtil.isNotEmpty(date)) {
            //return StrUtil.equals(Convert.toStr(date), "2");
        }

        //查询mongoDB中动态是否置顶
        if (publish.getTop() != 2) {
            return false;
        }

        //保存到redis中
        redisTemplate.opsForHash().put(redisKey, topStatus, "2");
        return true;
    }

    //消息通过(动态审核通过)
    @Override
    public Boolean messagesPass(List<String> ids) {

        //构造查询条件
        Query query = Query.query(Criteria.where("id").in(ids));
        //根据动态id查询动态
        List<Publish> publishList = mongoTemplate.find(query, Publish.class);

        if (CollUtil.isEmpty(publishList)) {
            return false;
        }

        //遍历动态列表获取动态的状态
        for (Publish publish : publishList) {

            Query newQuery = Query.query(Criteria.where("pid").is(publish.getPid()));
            // 修改 mongo
            mongoTemplate.updateFirst(newQuery, Update.update("status", 5), Publish.class);


            // 修改 redis
            String redisKey = getPublishRedisKeyPrefix(StrUtil.toString(publish.getId()));
            String passStatus = getPublishStatueKeyPrefix(StrUtil.toString(publish.getId()));
            redisTemplate.opsForHash().put(redisKey, passStatus, "5");

        }

        return true;

    }

    //消息拒绝
    @Override
    public Boolean messagesReject(List<String> ids) {

        //构造查询条件
        Query query = Query.query(Criteria.where("id").in(ids));
        //根据动态id查询动态
        List<Publish> publishList = mongoTemplate.find(query, Publish.class);

        if (CollUtil.isEmpty(publishList)) {
            return false;
        }

        //遍历动态列表获取动态的状态
        for (Publish publish : publishList) {

            Query newQuery = Query.query(Criteria.where("pid").is(publish.getPid()));
            // 修改 mongo
            mongoTemplate.updateFirst(newQuery, Update.update("status", 4), Publish.class);


            // 修改 redis
            String redisKey = getPublishRedisKeyPrefix(StrUtil.toString(publish.getId()));
            String passStatus = getPublishStatueKeyPrefix(StrUtil.toString(publish.getId()));
            redisTemplate.opsForHash().put(redisKey, passStatus, "4");

        }

        //消息拒绝成功
        return true;

    }

    //消息置顶
    @Override
    public Boolean messagesTop(String id) {
        try {
            //构造查询条件
            Query query = Query.query(Criteria.where("id").is(new ObjectId(id)));
            //根据动态id查询动态
            List<Publish> publish = mongoTemplate.find(query, Publish.class);
            //只能有一个动态置顶
            //根据用户id查询动态
            Query query2 = Query.query(Criteria.where("userId").is(publish.get(0).getUserId()));
            List<Publish> publishList = mongoTemplate.find(query2, Publish.class);
            //遍历publishList找到是否有动态已经置顶
            for (Publish newpublish : publishList) {
                if (newpublish.getTop() == 2) {
                    return false;
                }
            }

            Boolean result = isMessagesTop(publish.get(0));
            if (!result) {
                //将动态设置为置顶
                publish.get(0).setTop(2);
                //保存到mongoDB
                Query newQuery = Query.query(Criteria.where("pid").is(publish.get(0).getPid()));
                mongoTemplate.updateFirst(newQuery, Update.update("top", 2), Publish.class);
            }
            //消息置顶成功
            return true;
        } catch (Exception e) {
            //消息置顶失败
            return false;
        }
    }

    //取消置顶
    @Override
    public Boolean messagesUntop(String id) {
        try {
            //构造查询条件
            Query query = Query.query(Criteria.where("id").is(new ObjectId(id)));
            //根据动态id查询动态
            List<Publish> publish = mongoTemplate.find(query, Publish.class);
            Boolean result = isMessagesTop(publish.get(0));
            if (result) {
                //将动态设置为取消置顶
                publish.get(0).setTop(1);
                //保存到mongoDB
                Query newQuery = Query.query(Criteria.where("pid").is(publish.get(0).getPid()));
                mongoTemplate.updateFirst(newQuery, Update.update("top", 1), Publish.class);
            }
            //消息取消置顶成功
            return true;
        } catch (Exception e) {
            //消息取消置顶失败
            return false;
        }
    }

    /**
     * 根据动态id查询 动态信息
     *
     * @param publishId
     * @return
     */
    @Override
    public Publish queryPublish(String publishId) {

        //查询mongodb数据库
        return mongoTemplate.findById(publishId, Publish.class);

    }

    /**
     * 根据状态查询数量
     *
     * @param status
     * @return
     */
    @Override
    public Integer queryStatusCount(StatusEnum status, Long uid, String ed, String id, String sd) {
        Query query = new Query();
        long count = 0;
        //动态id  使用用户id代替
        if (StringUtils.isNotBlank(id)) {
            query.addCriteria(Criteria.where("userId").is(Convert.toLong(id)));
        }
        //uid
        if (ObjectUtil.isNotEmpty(uid)) {
            query.addCriteria(Criteria.where("userId").is(uid));
        }

        //开始 结束 时间
        if (!(StrUtil.equals("-1", sd) && StrUtil.equals("-1", ed))) {
            if (StrUtil.isNotEmpty(sd) && StrUtil.isNotEmpty(ed)) {
                long startTime = Convert.toLong(sd);
                long endTime = Convert.toLong(ed);
                query.addCriteria(Criteria.where("created").gte(startTime).lte(endTime));
            }
        }


        if (status.getValue() == 0) {
            count = mongoTemplate.count(query, Publish.class);
        } else {
            query.addCriteria(Criteria.where("status").is(status.getValue()));
            count = mongoTemplate.count(query, Publish.class);
        }

        return Convert.toInt(count);
    }


    /**
     * 根据查询条件,查询动态表
     *
     * @param page
     * @param pageSize
     * @param ed        结束时间
     * @param id        消息id
     * @param sd        开始时间
     * @param sortOrder 升序降序
     * @param state     审核状态
     * @param uid       用户ID
     * @param sortProp  排序字段
     * @return
     */
    @Override
    public List<Publish> queryPublishList(Integer page, Integer pageSize,
                                          String ed, String id, String sd, String sortOrder,
                                          Integer state, Long uid, String sortProp) {
        //查询条件
        Query query = new Query().with(PageRequest.of(page - 1, pageSize, Sort.by(Sort.Order.desc("created"))));

        //动态id  使用用户id代替
        if (StringUtils.isNotBlank(id)) {
            query.addCriteria(Criteria.where("userId").is(Convert.toLong(id)));
        }
        //uid
        if (ObjectUtil.isNotEmpty(uid)) {
            query.addCriteria(Criteria.where("userId").is(uid));
        }

        //开始 结束 时间
        if (!(StrUtil.equals("-1", sd) && StrUtil.equals("-1", ed))) {
            //当开始时间和结束时间都不为空时
            if (StrUtil.isNotEmpty(sd) && StrUtil.isNotEmpty(ed)) {
                //转为long
                long startTime = Convert.toLong(sd);
                long endTime = Convert.toLong(ed);
                query.addCriteria(Criteria.where("created").gte(startTime).lte(endTime));
            }
        }

        //state
        if (ObjectUtil.isNotEmpty(state)) {
            if (state != 0) {
                query.addCriteria(Criteria.where("status").is(state));
            }
        }

        //执行查询
        List<Publish> publishList = mongoTemplate.find(query, Publish.class);

        return publishList;
    }
}
