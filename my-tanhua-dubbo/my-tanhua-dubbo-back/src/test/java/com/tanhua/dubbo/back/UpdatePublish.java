package com.tanhua.dubbo.back;

import cn.hutool.core.collection.CollUtil;
import com.tanhua.dubbo.server.pojo.Publish;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UpdatePublish {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void test1() {
        List<Publish> publishList = mongoTemplate.findAll(Publish.class);
        for (Publish publish : publishList) {
            Publish p = new Publish();
            p.setId(publish.getId());
            p.setUserId(publish.getUserId());
            p.setCreated(publish.getCreated());
            p.setSeeType(publish.getSeeType());
            p.setLongitude(publish.getLongitude());
            p.setLatitude(publish.getLatitude());
            p.setText(publish.getText());
            p.setLocationName(publish.getLocationName());
            p.setMedias(publish.getMedias());
            p.setPid(publish.getPid());
            p.setNotSeeList(publish.getNotSeeList());
            p.setSeeList(publish.getSeeList());
            p.setTop(2);
            p.setStatus(3);
            mongoTemplate.save(p);
        }
    }
}
