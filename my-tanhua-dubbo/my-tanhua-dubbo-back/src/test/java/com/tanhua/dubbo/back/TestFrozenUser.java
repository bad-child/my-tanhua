package com.tanhua.dubbo.back;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.math.MathUtil;
import cn.hutool.core.util.StrUtil;
import com.tanhua.dubbo.server.api.FrozenUserApi;
import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.pojo.FrozenUser;
import com.tanhua.dubbo.server.pojo.Publish;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@SpringBootTest
@RunWith(SpringRunner.class)
public class TestFrozenUser {

    @Autowired
    private FrozenUserApi frozenUserApi;

    @Autowired
    private MongoTemplate mongoTemplate;


    @Test
    public void testFrozenUser(){
        FrozenUser frozenUser =new FrozenUser();
        frozenUser.setUserId(1);
        frozenUser.setFreezingRange(1);
        frozenUser.setFreezingTime(1);
        frozenUser.setReasonsForFreezing("测试原因");
        frozenUser.setFrozenRemarks("测试备注");
        frozenUser.setCreateTime(System.currentTimeMillis());
        frozenUserApi.frozenOneUser(frozenUser);
    }


    @Test
    public void testUnFrozenUser(){

        frozenUserApi.unFrozenOneUser(1,"测试解冻");
    }


    @Test
    public void testDeletePublish(){
        Long dateTime = DateUtil.parse(DateUtil.today() + " 00:00:00").getTime();
        Query query=new Query(Criteria.where("created").gt(dateTime));
        long count = mongoTemplate.remove(query, Comment.class).getDeletedCount();
        System.out.println(count);
    }

}
