package com.tanhua.dubbo.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 分布
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Distribution {
    private String title;
    private Integer amount;//数量
}
