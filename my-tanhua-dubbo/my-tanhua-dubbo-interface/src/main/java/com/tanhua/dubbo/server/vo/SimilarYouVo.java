package com.tanhua.dubbo.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SimilarYouVo implements Serializable {
    private Integer id;//用户id
    private String avatar;//与你相似的用户头像
}
