package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.RecommendUser;

import java.util.List;

public interface RecommendApi {
    RecommendUser findTodayBest(Long toUserId);

    List<RecommendUser> findRecommendation(Long toUserId);

    Double queryScore(Long userId, Long toUserId);

    List<RecommendUser> queryCardsList(Long id);
}
