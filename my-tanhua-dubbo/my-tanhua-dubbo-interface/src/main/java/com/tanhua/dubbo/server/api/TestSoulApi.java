package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.QuestionnaireReport;
import com.tanhua.dubbo.server.pojo.QuestionnaireResult;
import com.tanhua.dubbo.server.vo.QuestionnaireVo;
import org.bson.types.ObjectId;

import java.util.List;

public interface TestSoulApi {


    List<QuestionnaireVo> queryQuestinnaireVoList(Long id);

    Integer queryScore( String optionId);

    List<String> queryScope();



    QuestionnaireResult queryResult(String scope,String questionnaireId);

    String queryQuestinnaireId(String questionId);

    void saveResult(QuestionnaireReport questionnaireReport);

    void saveNewReport(String questionnaireId, ObjectId id);

    QuestionnaireReport queryReport(String id);

    QuestionnaireResult queryQuestionsResult(String resultId);

    List<QuestionnaireReport> findAllReport();
}
