package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.pojo.Video;

import java.util.List;

public interface VideoListApi {

    /**
     * 视频记录翻页
     * @param page
     * @param pageSize
     * @param userId
     * @return
     */
    List<Video> queryVideoList(Integer page, Integer pageSize, String userId);

    List<Video> queryAllVideo(String userId);

    /**
     * MongoDB数据库查询评论
     * @param publishId
     * @param commentType
     * @return
     */
    Long queryCommentCount(String publishId, CommentType commentType);
}
