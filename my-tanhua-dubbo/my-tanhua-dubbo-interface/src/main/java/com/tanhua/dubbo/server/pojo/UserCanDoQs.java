package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tb_question_user_lock")
public class UserCanDoQs implements Serializable {
    private ObjectId id;
    private String userId;//用户id
    private Integer level;
    private Integer islock;

}
