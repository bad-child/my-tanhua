package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.Sound;
import org.bson.types.ObjectId;

import java.util.List;

public interface SoundApi {

    /**
     * 发送语音
     *
     * @param sound
     * @return
     */
    void saveSound(Sound sound);

    /**
     * 接收语音
     *
     * @param userId
     * @return
     */
    List<Sound> querySoundById(Long userId);

    List<Sound> querySounds(Long userId,String sex);

    /**
     * 根据 sound id 更改状态
     * @param id
     */
    void removeSound(ObjectId id);

}
