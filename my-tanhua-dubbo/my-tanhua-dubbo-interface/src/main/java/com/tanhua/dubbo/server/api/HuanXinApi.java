package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.enums.HuanXinMessageType;
import com.tanhua.dubbo.server.pojo.HuanXinUser;

public interface HuanXinApi {
    String getToken();

    Boolean register(Long userId);

    HuanXinUser queryHuanXinUser(Long userId);

    HuanXinUser queryUserByUserName(String userName);

    Boolean addUserFriend(Long userId, Long friendId);

    Boolean removeUserFriend(Long userId, Long friendId);

    Boolean sendMsgFromAdmin(String targetUserName, HuanXinMessageType huanXinMessageType, String msg);

    Boolean addUserBlacklist(Long userId, Long blackId);

    Boolean removeBlacklist(Long userId, Long blackId);
}
