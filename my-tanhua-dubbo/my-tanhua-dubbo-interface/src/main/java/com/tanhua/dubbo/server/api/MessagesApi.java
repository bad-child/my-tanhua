package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.enums.StatusEnum;
import com.tanhua.dubbo.server.pojo.Publish;

import java.util.List;

public interface MessagesApi {
    /**
     *  从publish表中,修改状态
     * @param idList
     * @return
     */
    Boolean updatePublish(List<String> idList);

    /**
     *  根据查询条件,查询动态表
     * @param page
     * @param pageSize
     * @param ed    结束时间
     * @param id    消息id
     * @param sd    开始时间
     * @param sortOrder     排序字段
     * @param state 审核状态
     * @param uid   用户ID
     * @param sortProp  排序字段
     * @return
     */
    List<Publish> queryPublishList(Integer page, Integer pageSize, String ed,
                                   String id, String sd, String sortOrder, Integer state,
                                   Long uid, String sortProp);

    //消息通过
    Boolean messagesPass(List<String> ids);

    //消息拒绝
    Boolean messagesReject(List<String> ids);

    //消息置顶
    Boolean messagesTop(String id);

    //消息取消置顶
    Boolean messagesUntop(String id);

    /**
     * 根据动态id查询 动态信息
     * @param publishId
     * @return
     */
    Publish queryPublish(String publishId);

    /**
     * 根据状态查询数量
     * @param status
     * @return
     */
    Integer queryStatusCount(StatusEnum status, Long uid,String ed, String id, String sd);
}
