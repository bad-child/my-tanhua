package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.pojo.*;

import java.util.List;

public interface QuanZiApi {
    List<TimeLine> queryTimeLine(Long id);

    List<Publish> queryPublishList(List<Object> publishIdList, Integer page, Integer pagesize);

    List<Publish> queryRecommendList(List<Long> pids, Integer page, Integer pagesize);

    void savePublish(Publish publish);

    void saveAlbum(Album album, Long id);

    List<Users> queryFriendIds(Long id);

    void saveTimeLine(Object friendId, TimeLine timeLine);

    Boolean insertComment(Long id, String publishId, CommentType commentType, String content);

    Long queryCommentCount(String publishId, CommentType commentType);

    Boolean queryUserIsLikeOrLove(Long id, String publishId, CommentType commentType);

    Boolean delComment(Long id, String publishId, CommentType commentType);

    List<Comment> queryCommentList(String publishId);

    List<Comment> queryCommentListByUser(Long publishUserId, Integer page, Integer pageSize, CommentType commentType);

    List<Publish> queryAlbumList(Long userId, Integer page, Integer pageSize);

    List<Publish> queryPublishList(Long userId, Integer page, Integer pageSize);
}
