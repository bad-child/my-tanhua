package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.FrozenUser;

public interface FrozenUserApi {

    Boolean frozenOneUser(FrozenUser frozenUser);

    String getStatus(Long userId);

    Boolean unFrozenOneUser(Integer userId, String reason);

    FrozenUser getFrozenUser(Long userId);

}
