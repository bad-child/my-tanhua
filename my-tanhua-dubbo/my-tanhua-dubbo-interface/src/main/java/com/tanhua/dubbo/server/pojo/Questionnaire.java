package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tb_questionnaire")
public class Questionnaire  implements Serializable{
    private ObjectId id;
    private Integer level;
    private String star;
    private String reportId;
    private String cover;//封面
}
