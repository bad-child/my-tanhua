package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tb_questionnaire_result")
public class QuestionnaireResult implements Serializable {

    private ObjectId id;
    private Long questionnaireId;

    private String scope;//得分
    private String cover;//封面
    private String content;//结果
    /*private String extroversion;//外向型
    private String judgement;//判断力
    private String abstraction;//抽象
    private String rationality;//理性值*/

}