package com.tanhua.dubbo.server.enums;

import cn.hutool.core.util.StrUtil;

public enum IndustryEnum {
    WULIU("物流"),
    WENYU("文娱"),
    YILIAO("医疗"),
    JINRONG("金融"),
    CANYIN("餐饮"),
    JIAOYU("教育"),
    ZHUSU("住宿"),
    DICHAN("地产"),
    FUWU("服务"),
    ZHIZAO("制造"),
    JISUANJI("计算机"),
    QITA("其他");
    private String name;

    IndustryEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static IndustryEnum getIndustryEnum(String industry) {
        for (IndustryEnum industryEnum : values()) {
            if (StrUtil.contains(industry, industryEnum.getName())) {
                return industryEnum;
            }
        }
        //默认行业
        return QITA;
    }
}
