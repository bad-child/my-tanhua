package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.vo.UserLocationVo;

import java.util.List;

public interface UserLocationApi {
    Boolean updateUserLocation(Long userId, Double longitude, Double latitude, String address);

    UserLocationVo queryByUserId(Long userId);

    List<UserLocationVo> queryUserFromLocation(Double longitude, Double latitude, Double distance);
}
