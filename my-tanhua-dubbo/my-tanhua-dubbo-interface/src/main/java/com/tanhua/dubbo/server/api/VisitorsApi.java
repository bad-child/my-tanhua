package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.Visitors;

import java.util.List;

public interface VisitorsApi {
    String saveVisitor(Long userId, Long visitorUserId, String from);

    List<Visitors> queryMyVisitor(Long userId);
}
