package com.tanhua.dubbo.server.enums;


public enum StatusEnum {

    ALL(0,"全部"),
    DRGSH(1, "待人工审核"),
    ZDSHTG(2, "自动审核通过"),
    DSH(3, "待审核"),
    YJJ(4, "已拒绝"),
    YTG(5, "已通过"),
    ZDSHJJ(6, "自动审核拒绝");

    private int value;
    private String desc;

    StatusEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public int getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 根据value获取desc
     * @param value
     * @return
     */
    public static String getDescByValue(int value) {

        for (StatusEnum statusEnum : values()) {
            if (statusEnum.value == value) {
                return statusEnum.desc;
            }
        }
        return null;
    }

    /**
     * 根据value获取desc
     * @param value
     * @return
     */
    public static StatusEnum getStatus(int value) {

        for (StatusEnum statusEnum : values()) {
            if (statusEnum.value == value) {
                return statusEnum;
            }
        }
        return null;
    }
}
