package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.Video;

import java.util.List;

public interface                                                          VideoApi {
    List<Video> queryVideoList(Long id, Integer page, Integer pageSize);

    void saveVideo(Video video);

    void followUser(Long userId, Long followUserId);

    void disFollowUser(Long userId, Long followUserId);

    Boolean isFollowUser(Long userId, Long followUserId);
}
