package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tb_questionnaire_report")
public class QuestionnaireReport  implements Serializable{

    private ObjectId id;
    private String userId;//答题人id

    private String resultId;//答题结果
    private String cover;
    private String extroversion;//外向型
    private String judgement;//判断力
    private String abstraction;//抽象
    private String rationality;//理性值
}