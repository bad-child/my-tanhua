package com.tanhua.dubbo.server.api;


import com.tanhua.dubbo.server.pojo.Comment;

import java.util.List;

public interface CommentApi {

    /**
     * 评论列表翻页
     * @param page
     * @param pageSize
     * @param sortProp
     * @param sortOrder
     * @param messageID
     * @return
     */
    List<Comment> queryCommentListByPage(Integer page, Integer pageSize, String sortProp, String sortOrder, String messageID);

    /**
     * 不分页
     * @param messageID
     * @return
     */
    List<Comment> queryAllCommentList(String messageID);
}
