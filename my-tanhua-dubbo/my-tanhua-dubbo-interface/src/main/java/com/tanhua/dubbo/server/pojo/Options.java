package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tb_soul_question_option")

public class Options implements Serializable{
    private ObjectId id;
    private Integer questionId;
    private String optionText;
    private Integer score;
}
