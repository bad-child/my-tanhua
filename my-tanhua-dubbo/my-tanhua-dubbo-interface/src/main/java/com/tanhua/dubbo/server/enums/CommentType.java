package com.tanhua.dubbo.server.enums;

public enum CommentType {
    LIKE(1), COMMENT(2), LOVE(3),REPORT(4),  //举报
    FORWARD(5); //转发
    int type;

    CommentType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
