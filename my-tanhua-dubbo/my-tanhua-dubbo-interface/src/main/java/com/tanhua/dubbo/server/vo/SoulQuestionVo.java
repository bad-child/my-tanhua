package com.tanhua.dubbo.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoulQuestionVo implements Serializable {
    private String id;
    private String question;
    List<SoulQuestionOptionVo> options;
}