package com.tanhua.dubbo.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultVo implements Serializable {
    private String conclusion;//鉴定结果
    private String cover;//封面
    private List<DimensionsVo> dimensions;//维度
    private Set<SimilarYouVo> similarYou;//与你相似
}
