package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "frozen_user")
public class FrozenUser implements Serializable {

    private ObjectId id;
    private Integer userId;// 用户id
    private Integer freezingTime;// 冻结时间(1:3天  2:7天  3:永久冻结)
    private Integer freezingRange;// 冻结范围(1为冻结登录，2为冻结发言，3为冻结发布动态)
    private String reasonsForFreezing;// 冻结原因
    private String reasonsForUnFreezing = "到期自动解冻";// 解冻原因
    private String frozenRemarks;// 冻结备注
    private Long createTime;// 创建时间

}
