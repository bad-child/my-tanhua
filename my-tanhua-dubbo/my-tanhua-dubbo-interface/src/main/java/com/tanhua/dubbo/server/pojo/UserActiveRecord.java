package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document("system_useractiverecord")
public class UserActiveRecord implements Serializable {
    private static final long serialVersionUID = -4296017160071130562L;

    @Id
    private ObjectId id;
    private Long userId;//操作用户id
    private Long activeTime;//操作时间
    private Long isLogin;//是否为登录操作 1为登录操作 0为其他操作
    private Long useTime;//距离上次操作经过时间
}
