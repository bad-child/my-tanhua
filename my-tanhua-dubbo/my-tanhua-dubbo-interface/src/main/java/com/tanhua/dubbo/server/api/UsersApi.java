package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.Users;

import java.util.List;

public interface UsersApi {
    String saveUsers(Long userId, Long friendId);

    Boolean removeUsers(Long userId, Long friendId);
    
    List<Users> queryAllUsersList(Long userId);

    List<Users> queryUsersList(Long userId, Integer page, Integer pageSize);

    List<Long> queryUsersIdListById(Long userId);
}
