package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.UserLike;
import com.tanhua.dubbo.server.pojo.Users;

import java.util.List;

public interface UserLikeApi {
    Boolean likeUser(Long userId, Long likeUserId);

    Boolean notLikeUser(Long userId, Long likeUserId);

    Boolean isLike(Long userId, Long likeUserId);

    Boolean isMutualLike(Long userId, Long likeUserId);

    List<Long> queryLikeList(Long userId);

    List<Long> queryNotLikeList(Long userId);


    /**
     * 获取 喜欢数
     *
     * @param userId
     * @return
     */
    Integer queryLikedCount(Long userId);

    /**
     * 获取被喜欢数
     *
     * @param userId
     * @return
     */
    Integer queryBeLikedCount(Long userId);

    /**
     * 获取互相喜欢数
     *
     * @param userId
     * @return
     */
    Integer queryMatchingCount(Long userId);

    /**
     * 取消喜欢
     * @param userId
     * @param likeUserId
     * @return
     */
    Boolean cancelLike(Long userId, Long likeUserId);

    //获取粉丝列表
    List<Long> queryFansList(Long id);

    //查询喜欢列表
    List<UserLike> queryLikeList2(Long id);

    //查询喜欢的数
    Integer queryLikeCount2(Long id);

    //查询互相喜欢数
    Integer queryLikeEachOtherCount(Long id);

    //查询互相喜欢列表
    List<Long> querylikeEachOtherIdsList(Long id);

    //重写判断用户是否喜欢ta
    Boolean likeUser2(Long id, Long userId);
}
