package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.UserActiveRecord;

import java.util.List;

public interface DashBoardApi {
    Integer countTimeActiveUsers(Long startOfMonth, Long endOfMonth, Long isLogin);

    Integer countNextDayUsers(List<UserActiveRecord> userActiveRecordList);

    void saveUserActive(Long userId, Long isLogin);

    Integer countActivePass(Integer day, Long isLogin);

    Integer countActiveToday(Long isLogin);

    Long queryUseTimePass(Integer day);

    Long getLastActiveTime(Long userId);

    Integer countTimeLoginUsers(Long startOfMonth, Long endOfMonth);

    List<UserActiveRecord> queryLoginUser(Long userId);

}
