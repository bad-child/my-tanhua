package com.tanhua.dubbo.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionnaireVo implements Serializable {
    private String id;//问卷编号
    private String name;//问卷名称：初级灵魂题  中级灵魂题   高级灵魂题
    private String cover;//封面
    private String level;//问卷级别:初 中 高
    private Integer star;//星级
    private List<SoulQuestionVo> questions;//试题
    private Integer isLock;//是否锁住
    private String reportId;//最新报告编号
}