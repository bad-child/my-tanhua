package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tanhua_sound")//userId：具体的登录者id
public class Sound implements java.io.Serializable {
    private ObjectId id;
    private Long userId;
    private String gender;
    private String soundUrl;
    private Integer status = 1;
}
