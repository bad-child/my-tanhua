package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor//"tb_question"
@Document(collection = "tb_question")
public class Questions  implements Serializable{
    @Id
    private ObjectId id;
    private Integer pId;
    private String questionText;
    private String questionnaireId;
}
