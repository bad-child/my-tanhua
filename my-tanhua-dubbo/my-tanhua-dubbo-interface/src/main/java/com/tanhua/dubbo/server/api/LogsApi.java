package com.tanhua.dubbo.server.api;


import com.tanhua.dubbo.server.pojo.UserActiveRecord;

import java.util.List;

public interface LogsApi {

    /**
     *  日志翻页
     * @param page
     * @param pageSize
     * @param sortProp
     * @param sortOrder
     * @param type
     * @param userId
     * @return
     */
    List<UserActiveRecord> queryLogsList(Integer page, Integer pageSize, String sortProp, String sortOrder, String type, String userId);


    /**
     * 不分页
     * @param userId
     * @return
     */
    List<UserActiveRecord> queryAllLogs(String userId);
}
