/*
 Navicat Premium Data Transfer

 Source Server         : tanhua
 Source Server Type    : MySQL
 Source Server Version : 50650
 Source Host           : 192.168.31.81:3306
 Source Schema         : mytanhua

 Target Server Type    : MySQL
 Target Server Version : 50650
 File Encoding         : 65001

 Date: 15/01/2022 17:29:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_announcement
-- ----------------------------
DROP TABLE IF EXISTS `tb_announcement`;
CREATE TABLE `tb_announcement`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `created` datetime(0) NULL DEFAULT NULL,
  `updated` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `created`(`created`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公告表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_announcement
-- ----------------------------
INSERT INTO `tb_announcement` VALUES (1, '探花新版本上线发布啦～,盛夏high趴开始了，赶紧来报名吧！', '探花App2019年7月23日起在苹果商店…,浓情夏日，清爽一聚，探花将吧大家聚…', '2019-10-14 11:06:34', '2019-10-14 11:06:37');
INSERT INTO `tb_announcement` VALUES (2, '探花交友的圈子功能正式上线啦~~', '探花交友的圈子功能正式上线啦，欢迎使用~', '2019-10-14 11:09:31', '2019-10-14 11:09:33');
INSERT INTO `tb_announcement` VALUES (3, '国庆放假期间，探花交友正常使用~', '国庆放假期间，探花交友正常使用~', '2019-10-14 11:10:01', '2019-10-14 11:10:04');

-- ----------------------------
-- Table structure for tb_black_list
-- ----------------------------
DROP TABLE IF EXISTS `tb_black_list`;
CREATE TABLE `tb_black_list`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `black_user_id` bigint(20) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `updated` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '黑名单' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_black_list
-- ----------------------------

-- ----------------------------
-- Table structure for tb_huanxin_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_huanxin_user`;
CREATE TABLE `tb_huanxin_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '环信用户名',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '环信密码',
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `created` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updated` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_huanxin_user
-- ----------------------------
INSERT INTO `tb_huanxin_user` VALUES (1, 1, 'HX_1', 'd7c44e0cf2424f8ab7f53e9b4c025fa6', NULL, '2022-01-05 21:19:26', '2022-01-05 21:19:26');
INSERT INTO `tb_huanxin_user` VALUES (2, 2, 'HX_2', '4c335ee106c142988eb751af68d30269', NULL, '2022-01-06 10:58:12', '2022-01-06 10:58:12');
INSERT INTO `tb_huanxin_user` VALUES (3, 3, 'HX_3', '7dcee41dfbb74790aaf449c5dfc90adc', NULL, '2022-01-06 10:58:12', '2022-01-06 10:58:12');
INSERT INTO `tb_huanxin_user` VALUES (4, 4, 'HX_4', 'a49372e367a745ce9a25388e533deddf', NULL, '2022-01-06 10:58:12', '2022-01-06 10:58:12');
INSERT INTO `tb_huanxin_user` VALUES (5, 5, 'HX_5', '5cb21462f43f4ef68c757b867893e3dc', NULL, '2022-01-06 10:58:12', '2022-01-06 10:58:12');
INSERT INTO `tb_huanxin_user` VALUES (6, 6, 'HX_6', '6af4621ce15b47988812be47f2d45b53', NULL, '2022-01-06 10:58:12', '2022-01-06 10:58:12');
INSERT INTO `tb_huanxin_user` VALUES (7, 7, 'HX_7', 'a4edcdd161eb439d9bfb279d5854f2d9', NULL, '2022-01-06 10:58:12', '2022-01-06 10:58:12');
INSERT INTO `tb_huanxin_user` VALUES (8, 8, 'HX_8', 'dcccb7d518784effbed505629be3c6ed', NULL, '2022-01-06 10:58:13', '2022-01-06 10:58:13');
INSERT INTO `tb_huanxin_user` VALUES (9, 9, 'HX_9', '2ffc566b1477440e885168ca8a22fa7c', NULL, '2022-01-06 10:58:13', '2022-01-06 10:58:13');
INSERT INTO `tb_huanxin_user` VALUES (10, 10, 'HX_10', '26d838abe98b4cdba4fa6363ba30266d', NULL, '2022-01-06 10:58:13', '2022-01-06 10:58:13');
INSERT INTO `tb_huanxin_user` VALUES (11, 11, 'HX_11', '490f0c6febcb4b29a0697370007296bd', NULL, '2022-01-06 10:58:13', '2022-01-06 10:58:13');
INSERT INTO `tb_huanxin_user` VALUES (12, 12, 'HX_12', 'fad03909467e4fbc8d143c9bf366bd2d', NULL, '2022-01-06 10:58:13', '2022-01-06 10:58:13');
INSERT INTO `tb_huanxin_user` VALUES (13, 13, 'HX_13', '7b68870507f849f9a01d9a16c1c71478', NULL, '2022-01-06 10:58:13', '2022-01-06 10:58:13');
INSERT INTO `tb_huanxin_user` VALUES (14, 14, 'HX_14', 'b37c60e4942a4c8a87e8270137381020', NULL, '2022-01-06 10:58:13', '2022-01-06 10:58:13');
INSERT INTO `tb_huanxin_user` VALUES (15, 15, 'HX_15', 'fce68b3d73834ad8adbe383020d1214d', NULL, '2022-01-06 10:58:13', '2022-01-06 10:58:13');
INSERT INTO `tb_huanxin_user` VALUES (16, 16, 'HX_16', '0b105af84c974d1bb41002ad737ea571', NULL, '2022-01-06 10:58:13', '2022-01-06 10:58:13');
INSERT INTO `tb_huanxin_user` VALUES (17, 17, 'HX_17', '0d2789902aa84dc38a288edb820e0f31', NULL, '2022-01-06 10:58:13', '2022-01-06 10:58:13');
INSERT INTO `tb_huanxin_user` VALUES (18, 18, 'HX_18', '2d7f5e0b8037478a9faacdfb86ee9179', NULL, '2022-01-06 10:58:14', '2022-01-06 10:58:14');
INSERT INTO `tb_huanxin_user` VALUES (19, 19, 'HX_19', '2ae7f6441d304dc1bb4b661070d30338', NULL, '2022-01-06 10:58:14', '2022-01-06 10:58:14');
INSERT INTO `tb_huanxin_user` VALUES (20, 20, 'HX_20', '04373e3bb1634136aae0fe9405859a37', NULL, '2022-01-06 10:58:14', '2022-01-06 10:58:14');
INSERT INTO `tb_huanxin_user` VALUES (21, 21, 'HX_21', 'cde8cf6f304c4fa18c5a546fb0e798eb', NULL, '2022-01-06 10:58:14', '2022-01-06 10:58:14');
INSERT INTO `tb_huanxin_user` VALUES (22, 22, 'HX_22', 'e6bed519bfac488abaf8a9348fe2a9f4', NULL, '2022-01-06 10:58:14', '2022-01-06 10:58:14');
INSERT INTO `tb_huanxin_user` VALUES (23, 23, 'HX_23', '5ca6c202f295484e8f2bbcb52db14cc6', NULL, '2022-01-06 10:58:14', '2022-01-06 10:58:14');
INSERT INTO `tb_huanxin_user` VALUES (24, 24, 'HX_24', '8cb5d53811f347cda4a6042ff3e5da30', NULL, '2022-01-06 10:58:14', '2022-01-06 10:58:14');
INSERT INTO `tb_huanxin_user` VALUES (25, 25, 'HX_25', '463eba840ad1471f8cb62c8b07e98c75', NULL, '2022-01-06 10:58:14', '2022-01-06 10:58:14');
INSERT INTO `tb_huanxin_user` VALUES (26, 26, 'HX_26', '2b0ba5fe16964e83a1efdf5d545dc86e', NULL, '2022-01-06 10:58:14', '2022-01-06 10:58:14');
INSERT INTO `tb_huanxin_user` VALUES (27, 27, 'HX_27', '9395ebfd53c442a5aedb6c987655bae8', NULL, '2022-01-06 10:58:15', '2022-01-06 10:58:15');
INSERT INTO `tb_huanxin_user` VALUES (28, 28, 'HX_28', '61e7172794124ea495e4ff309ba9cfbe', NULL, '2022-01-06 10:58:15', '2022-01-06 10:58:15');
INSERT INTO `tb_huanxin_user` VALUES (29, 29, 'HX_29', 'ff0439830db04dd89b2a1e59d6e959fa', NULL, '2022-01-06 10:58:15', '2022-01-06 10:58:15');
INSERT INTO `tb_huanxin_user` VALUES (30, 30, 'HX_30', 'e3d99db16f12483996c9db0a18cc8639', NULL, '2022-01-06 10:58:15', '2022-01-06 10:58:15');
INSERT INTO `tb_huanxin_user` VALUES (31, 31, 'HX_31', '494f126075d84f93848be0f6bfadf26e', NULL, '2022-01-06 10:58:15', '2022-01-06 10:58:15');
INSERT INTO `tb_huanxin_user` VALUES (32, 32, 'HX_32', 'ee343a7b48a049e4aaeb3c66d6fcaaf6', NULL, '2022-01-06 10:58:15', '2022-01-06 10:58:15');
INSERT INTO `tb_huanxin_user` VALUES (33, 33, 'HX_33', '4ff31d0fd1af47679804a15c3c4fc6cd', NULL, '2022-01-06 10:58:15', '2022-01-06 10:58:15');
INSERT INTO `tb_huanxin_user` VALUES (34, 34, 'HX_34', '12bcacf2e1734fac94fd4da088a6fd26', NULL, '2022-01-06 10:58:15', '2022-01-06 10:58:15');
INSERT INTO `tb_huanxin_user` VALUES (35, 35, 'HX_35', '962e07aca24a47508d81357927bf8bad', NULL, '2022-01-06 10:58:15', '2022-01-06 10:58:15');
INSERT INTO `tb_huanxin_user` VALUES (36, 36, 'HX_36', '68c31c37a1af44dd902094ddd4fd0c03', NULL, '2022-01-06 10:58:15', '2022-01-06 10:58:15');
INSERT INTO `tb_huanxin_user` VALUES (37, 37, 'HX_37', '87f2dabb4db746098bd0cc97c56361eb', NULL, '2022-01-06 10:58:16', '2022-01-06 10:58:16');
INSERT INTO `tb_huanxin_user` VALUES (38, 38, 'HX_38', '69ddd0d546c7444bbf6ba8b8e9dd2257', NULL, '2022-01-06 10:58:16', '2022-01-06 10:58:16');
INSERT INTO `tb_huanxin_user` VALUES (39, 39, 'HX_39', '775d3ec06efe420697bbf9640e3b81fb', NULL, '2022-01-06 10:58:16', '2022-01-06 10:58:16');
INSERT INTO `tb_huanxin_user` VALUES (40, 40, 'HX_40', '355f6fbed3054e10bdb6215b0edcd680', NULL, '2022-01-06 10:58:16', '2022-01-06 10:58:16');
INSERT INTO `tb_huanxin_user` VALUES (41, 41, 'HX_41', 'a1e0c807371148a999a9a21e5079bfec', NULL, '2022-01-06 10:58:16', '2022-01-06 10:58:16');
INSERT INTO `tb_huanxin_user` VALUES (42, 42, 'HX_42', '93456a7d32de449dbebe661a2087f423', NULL, '2022-01-06 10:58:16', '2022-01-06 10:58:16');
INSERT INTO `tb_huanxin_user` VALUES (43, 43, 'HX_43', '9db5cfd3b9ac44edb643e9ecc239bd10', NULL, '2022-01-06 10:58:16', '2022-01-06 10:58:16');
INSERT INTO `tb_huanxin_user` VALUES (44, 44, 'HX_44', 'b49aeef96f194db8bd49b8333a60a9eb', NULL, '2022-01-06 10:58:16', '2022-01-06 10:58:16');
INSERT INTO `tb_huanxin_user` VALUES (45, 45, 'HX_45', '650932dbc9444a639b034d2b1479a6e7', NULL, '2022-01-06 10:58:16', '2022-01-06 10:58:16');
INSERT INTO `tb_huanxin_user` VALUES (46, 46, 'HX_46', '766cfa9967104b2984b33b2833325b67', NULL, '2022-01-06 10:58:17', '2022-01-06 10:58:17');
INSERT INTO `tb_huanxin_user` VALUES (47, 47, 'HX_47', '2e50e10a285845c282f7861555598649', NULL, '2022-01-06 10:58:18', '2022-01-06 10:58:18');
INSERT INTO `tb_huanxin_user` VALUES (48, 48, 'HX_48', '98b190d7885148f697cc352714b8887e', NULL, '2022-01-06 10:58:18', '2022-01-06 10:58:18');
INSERT INTO `tb_huanxin_user` VALUES (49, 49, 'HX_49', '735b889cf4c74fa794b49cc86fd6eda7', NULL, '2022-01-06 10:58:18', '2022-01-06 10:58:18');
INSERT INTO `tb_huanxin_user` VALUES (50, 50, 'HX_50', '6683ef9b4eb1473e8d48fac10cae3d50', NULL, '2022-01-06 10:58:18', '2022-01-06 10:58:18');
INSERT INTO `tb_huanxin_user` VALUES (51, 51, 'HX_51', '9798623a4cd54fb486488cb21767c8fe', NULL, '2022-01-06 10:58:18', '2022-01-06 10:58:18');
INSERT INTO `tb_huanxin_user` VALUES (52, 52, 'HX_52', '37d5e43105b542e7b955f62108ce5410', NULL, '2022-01-06 10:58:18', '2022-01-06 10:58:18');
INSERT INTO `tb_huanxin_user` VALUES (53, 53, 'HX_53', '157352d7b1c846ce938cfb0369aa0502', NULL, '2022-01-06 10:58:18', '2022-01-06 10:58:18');
INSERT INTO `tb_huanxin_user` VALUES (54, 54, 'HX_54', 'f6d00bfb536c43cd909096a409f76f20', NULL, '2022-01-06 10:58:18', '2022-01-06 10:58:18');
INSERT INTO `tb_huanxin_user` VALUES (55, 55, 'HX_55', '0f2e250166b240f4b9c4a2e0c654710f', NULL, '2022-01-06 10:58:19', '2022-01-06 10:58:19');
INSERT INTO `tb_huanxin_user` VALUES (56, 56, 'HX_56', 'e9bcf3d07ac54024aac7f9949a6689cf', NULL, '2022-01-06 10:58:19', '2022-01-06 10:58:19');
INSERT INTO `tb_huanxin_user` VALUES (57, 57, 'HX_57', 'ab7f89de3ced48809acb8880f6fee883', NULL, '2022-01-06 10:58:19', '2022-01-06 10:58:19');
INSERT INTO `tb_huanxin_user` VALUES (58, 58, 'HX_58', 'f4d88bba6d6e49798b40eab89a54735f', NULL, '2022-01-06 10:58:19', '2022-01-06 10:58:19');
INSERT INTO `tb_huanxin_user` VALUES (59, 59, 'HX_59', '7f2bbfebf07b4d67be2acf9617aa9ea9', NULL, '2022-01-06 10:58:19', '2022-01-06 10:58:19');
INSERT INTO `tb_huanxin_user` VALUES (60, 60, 'HX_60', '374ca4859de84fc4b1d59f664516f9d4', NULL, '2022-01-06 10:58:19', '2022-01-06 10:58:19');
INSERT INTO `tb_huanxin_user` VALUES (61, 61, 'HX_61', '97dec0542d9d4cabb815c222b5ebf3b1', NULL, '2022-01-06 10:58:20', '2022-01-06 10:58:20');
INSERT INTO `tb_huanxin_user` VALUES (62, 62, 'HX_62', 'b192808a90d746b0958e31cb76cc740d', NULL, '2022-01-06 10:58:20', '2022-01-06 10:58:20');
INSERT INTO `tb_huanxin_user` VALUES (63, 63, 'HX_63', 'a4ae73d0aa954d56a7a7b950765711ac', NULL, '2022-01-06 10:58:20', '2022-01-06 10:58:20');
INSERT INTO `tb_huanxin_user` VALUES (64, 64, 'HX_64', '781662325eba4026a26365b9c4fe8317', NULL, '2022-01-06 10:58:20', '2022-01-06 10:58:20');
INSERT INTO `tb_huanxin_user` VALUES (65, 65, 'HX_65', 'c822917509a14812a5f1be7ec9dd4f0a', NULL, '2022-01-06 10:58:20', '2022-01-06 10:58:20');
INSERT INTO `tb_huanxin_user` VALUES (66, 66, 'HX_66', '63caeefe6c794ab5abb5ca0cf5322fcf', NULL, '2022-01-06 10:58:20', '2022-01-06 10:58:20');
INSERT INTO `tb_huanxin_user` VALUES (67, 67, 'HX_67', '017e552d60044086a5e110709287905f', NULL, '2022-01-06 10:58:20', '2022-01-06 10:58:20');
INSERT INTO `tb_huanxin_user` VALUES (68, 68, 'HX_68', '7f45494400014f1aa98b1a7de92bd9cb', NULL, '2022-01-06 10:58:20', '2022-01-06 10:58:20');
INSERT INTO `tb_huanxin_user` VALUES (69, 69, 'HX_69', '4c1a2004a9ac497d817c578e11b5468f', NULL, '2022-01-06 10:58:20', '2022-01-06 10:58:20');
INSERT INTO `tb_huanxin_user` VALUES (70, 70, 'HX_70', '1781a76bf2f64cceb7ac74fd53ceb055', NULL, '2022-01-06 10:58:20', '2022-01-06 10:58:20');
INSERT INTO `tb_huanxin_user` VALUES (71, 71, 'HX_71', '1b033dd3f17d4076ab50af3f441e46e9', NULL, '2022-01-06 10:58:21', '2022-01-06 10:58:21');
INSERT INTO `tb_huanxin_user` VALUES (72, 72, 'HX_72', 'a8b63d2b408d4552bfb71951ffc8d370', NULL, '2022-01-06 10:58:21', '2022-01-06 10:58:21');
INSERT INTO `tb_huanxin_user` VALUES (73, 73, 'HX_73', '6b77737eee4c4e73a6ad30d14cda2e79', NULL, '2022-01-06 10:58:21', '2022-01-06 10:58:21');
INSERT INTO `tb_huanxin_user` VALUES (74, 74, 'HX_74', '0ee3eda6229a4f3895d1019ff36cbeba', NULL, '2022-01-06 10:58:21', '2022-01-06 10:58:21');
INSERT INTO `tb_huanxin_user` VALUES (75, 75, 'HX_75', 'b81cc0e32fda44c58952a09fea54af4a', NULL, '2022-01-06 10:58:21', '2022-01-06 10:58:21');
INSERT INTO `tb_huanxin_user` VALUES (76, 76, 'HX_76', '443bc039a42547f892f8031ee5b890c7', NULL, '2022-01-06 10:58:21', '2022-01-06 10:58:21');
INSERT INTO `tb_huanxin_user` VALUES (77, 77, 'HX_77', '5e94b394ea9b45c5961fd36606627ffd', NULL, '2022-01-06 10:58:21', '2022-01-06 10:58:21');
INSERT INTO `tb_huanxin_user` VALUES (78, 78, 'HX_78', '4e399e6ccc1642ff820643de20735dc4', NULL, '2022-01-06 10:58:21', '2022-01-06 10:58:21');
INSERT INTO `tb_huanxin_user` VALUES (79, 79, 'HX_79', '139cebfdd1704943ab95fcdbbdc2fe8f', NULL, '2022-01-06 10:58:21', '2022-01-06 10:58:21');
INSERT INTO `tb_huanxin_user` VALUES (80, 80, 'HX_80', '0d56614fc20c48da95ab7bac702ae250', NULL, '2022-01-06 10:58:22', '2022-01-06 10:58:22');
INSERT INTO `tb_huanxin_user` VALUES (81, 81, 'HX_81', '5eef977300894dc9ba9f8fe23b9bd901', NULL, '2022-01-06 10:58:22', '2022-01-06 10:58:22');
INSERT INTO `tb_huanxin_user` VALUES (82, 82, 'HX_82', '871c31d3b1a945f7bb7758a2ff1ccb4a', NULL, '2022-01-06 10:58:22', '2022-01-06 10:58:22');
INSERT INTO `tb_huanxin_user` VALUES (83, 83, 'HX_83', 'adaec076ecd045d8926c4b0e0ff2412b', NULL, '2022-01-06 10:58:22', '2022-01-06 10:58:22');
INSERT INTO `tb_huanxin_user` VALUES (84, 84, 'HX_84', 'abb03b9165024ce5bda3008d00e6e0d0', NULL, '2022-01-06 10:58:22', '2022-01-06 10:58:22');
INSERT INTO `tb_huanxin_user` VALUES (85, 85, 'HX_85', '5f1f050943364cd5915e13074db5a246', NULL, '2022-01-06 10:58:22', '2022-01-06 10:58:22');
INSERT INTO `tb_huanxin_user` VALUES (86, 86, 'HX_86', '0473bc17a9ec45f19e2eea489e08db35', NULL, '2022-01-06 10:58:22', '2022-01-06 10:58:22');
INSERT INTO `tb_huanxin_user` VALUES (87, 87, 'HX_87', '38ae5b7fa55e47ac92adec1e328bbda5', NULL, '2022-01-06 10:58:22', '2022-01-06 10:58:22');
INSERT INTO `tb_huanxin_user` VALUES (88, 88, 'HX_88', 'e9835a23fd1d4f6aa91908a8b112a0c2', NULL, '2022-01-06 10:58:22', '2022-01-06 10:58:22');
INSERT INTO `tb_huanxin_user` VALUES (89, 89, 'HX_89', 'e2d644f00d694024a72ea15c8aad2397', NULL, '2022-01-06 10:58:22', '2022-01-06 10:58:22');
INSERT INTO `tb_huanxin_user` VALUES (90, 90, 'HX_90', '60bab365be9444e3890af13d4b0c8dd0', NULL, '2022-01-06 10:58:23', '2022-01-06 10:58:23');
INSERT INTO `tb_huanxin_user` VALUES (91, 91, 'HX_91', '8351104a2696451f8cd24f98b4671094', NULL, '2022-01-06 10:58:23', '2022-01-06 10:58:23');
INSERT INTO `tb_huanxin_user` VALUES (92, 92, 'HX_92', 'ee4dc84007db4b73a05913ffeb91f4fe', NULL, '2022-01-06 10:58:23', '2022-01-06 10:58:23');
INSERT INTO `tb_huanxin_user` VALUES (93, 93, 'HX_93', 'bab678e209b14c5fa089300710ef5537', NULL, '2022-01-06 10:58:23', '2022-01-06 10:58:23');
INSERT INTO `tb_huanxin_user` VALUES (94, 94, 'HX_94', 'cdb81f2213474c8c8db68254418bb40c', NULL, '2022-01-06 10:58:23', '2022-01-06 10:58:23');
INSERT INTO `tb_huanxin_user` VALUES (95, 95, 'HX_95', '05d17f38f80841bb830118e1abf0339d', NULL, '2022-01-06 10:58:23', '2022-01-06 10:58:23');
INSERT INTO `tb_huanxin_user` VALUES (96, 96, 'HX_96', 'ba43aaf3ec854c80a14d69b4cdd1b98f', NULL, '2022-01-06 10:58:23', '2022-01-06 10:58:23');
INSERT INTO `tb_huanxin_user` VALUES (97, 97, 'HX_97', '9267c3d9f14247028da38b6e6865d1dd', NULL, '2022-01-06 10:58:23', '2022-01-06 10:58:23');
INSERT INTO `tb_huanxin_user` VALUES (98, 98, 'HX_98', 'd31e29d4cb884ccc8ec60ae07128c761', NULL, '2022-01-06 10:58:23', '2022-01-06 10:58:23');
INSERT INTO `tb_huanxin_user` VALUES (99, 99, 'HX_99', '2d0d54bbb79f427eb59128f9eb0437fe', NULL, '2022-01-06 10:58:24', '2022-01-06 10:58:24');
INSERT INTO `tb_huanxin_user` VALUES (100, 98, 'HX_98', 'f8c3dc9acc324e11a4aac0c70790e480', NULL, '2022-01-12 11:22:23', '2022-01-12 11:22:23');
INSERT INTO `tb_huanxin_user` VALUES (101, 99, 'HX_99', '62569af56b824e9882d95d2813c5ec9f', NULL, '2022-01-12 11:22:23', '2022-01-12 11:22:23');

-- ----------------------------
-- Table structure for tb_manager
-- ----------------------------
DROP TABLE IF EXISTS `tb_manager`;
CREATE TABLE `tb_manager`  (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `logo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `updated` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_manager
-- ----------------------------
INSERT INTO `tb_manager` VALUES (1, 'admin', '123456', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/', NULL, NULL);
INSERT INTO `tb_manager` VALUES (2, 'admin2', '123456', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/', NULL, NULL);
INSERT INTO `tb_manager` VALUES (3, '艾家庆', '123456', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/', NULL, NULL);

-- ----------------------------
-- Table structure for tb_question
-- ----------------------------
DROP TABLE IF EXISTS `tb_question`;
CREATE TABLE `tb_question`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `txt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问题内容',
  `created` datetime(0) NULL DEFAULT NULL,
  `updated` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_question
-- ----------------------------
INSERT INTO `tb_question` VALUES (1, 1, '你确定真喜欢我吗?', '2019-10-20 17:25:58', '2022-01-15 11:34:41');
INSERT INTO `tb_question` VALUES (2, 57, '你喜欢什么颜色？', '2019-10-20 23:17:39', '2019-10-20 23:17:41');

-- ----------------------------
-- Table structure for tb_settings
-- ----------------------------
DROP TABLE IF EXISTS `tb_settings`;
CREATE TABLE `tb_settings`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `like_notification` tinyint(4) NULL DEFAULT 1 COMMENT '推送喜欢通知',
  `pinglun_notification` tinyint(4) NULL DEFAULT 1 COMMENT '推送评论通知',
  `gonggao_notification` tinyint(4) NULL DEFAULT 1 COMMENT '推送公告通知',
  `created` datetime(0) NULL DEFAULT NULL,
  `updated` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '设置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_settings
-- ----------------------------
INSERT INTO `tb_settings` VALUES (1, 1, 0, 1, 1, '2019-11-03 11:58:36', '2022-01-14 09:22:17');
INSERT INTO `tb_settings` VALUES (2, 17, 0, 0, 0, NULL, NULL);
INSERT INTO `tb_settings` VALUES (3, 86, 0, 0, 0, NULL, NULL);
INSERT INTO `tb_settings` VALUES (4, 79, 0, 0, 0, NULL, NULL);
INSERT INTO `tb_settings` VALUES (5, 28, 0, 0, 0, '2022-01-12 20:04:00', '2022-01-12 20:04:00');
INSERT INTO `tb_settings` VALUES (6, 37, 0, 0, 0, '2022-01-12 20:19:27', '2022-01-12 20:19:27');
INSERT INTO `tb_settings` VALUES (7, 2, 0, 0, 0, '2022-01-12 21:00:57', '2022-01-12 21:00:57');
INSERT INTO `tb_settings` VALUES (8, 52, 0, 0, 0, '2022-01-13 08:43:17', '2022-01-13 08:43:17');
INSERT INTO `tb_settings` VALUES (9, 42, 0, 0, 0, '2022-01-13 08:45:46', '2022-01-13 08:45:46');
INSERT INTO `tb_settings` VALUES (10, 61, 0, 0, 0, '2022-01-13 14:08:16', '2022-01-13 14:08:16');
INSERT INTO `tb_settings` VALUES (11, 73, 0, 0, 0, '2022-01-13 14:26:28', '2022-01-13 14:26:39');
INSERT INTO `tb_settings` VALUES (12, 29, 0, 0, 0, '2022-01-13 17:05:50', '2022-01-13 17:05:50');
INSERT INTO `tb_settings` VALUES (13, 84, 0, 0, 0, '2022-01-13 18:12:29', '2022-01-13 18:12:29');
INSERT INTO `tb_settings` VALUES (14, 76, 0, 0, 0, '2022-01-13 18:21:57', '2022-01-13 18:21:57');
INSERT INTO `tb_settings` VALUES (15, 24, 1, 1, 0, '2022-01-14 11:08:31', '2022-01-15 10:10:14');
INSERT INTO `tb_settings` VALUES (16, 19, 0, 0, 0, '2022-01-14 11:10:54', '2022-01-14 11:10:54');
INSERT INTO `tb_settings` VALUES (17, 30, 1, 1, 1, '2022-01-14 18:59:14', '2022-01-14 18:59:55');
INSERT INTO `tb_settings` VALUES (18, 83, 0, 0, 0, '2022-01-15 10:28:25', '2022-01-15 10:28:25');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码，需要加密',
  `created` datetime(0) NULL DEFAULT NULL,
  `updated` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `mobile`(`mobile`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, '17602026868', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-09 16:43:46', '2022-01-15 10:34:04');
INSERT INTO `tb_user` VALUES (2, '15800807988', 'e10adc3949ba59abbe56e057f20f883e', '2021-11-07 16:44:23', '2022-01-14 21:18:00');
INSERT INTO `tb_user` VALUES (3, '13172725874', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user` VALUES (4, '13877469758', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user` VALUES (5, '13037577648', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user` VALUES (6, '13079029863', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user` VALUES (7, '13024673388', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user` VALUES (8, '13235171211', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user` VALUES (9, '13554575271', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (10, '13006615355', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2022-01-13 17:10:23');
INSERT INTO `tb_user` VALUES (11, '13496418811', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (12, '13501605527', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (13, '13814273863', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (14, '13980560088', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (15, '13287655532', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (16, '13392532132', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (17, '13305577548', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2022-01-12 11:38:35');
INSERT INTO `tb_user` VALUES (18, '13056740178', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (19, '13968734556', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2022-01-14 11:08:54');
INSERT INTO `tb_user` VALUES (20, '13790585392', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (21, '13783990035', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (22, '13955001055', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (23, '13503888939', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (24, '13584175268', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2022-01-15 10:04:19');
INSERT INTO `tb_user` VALUES (25, '13381313372', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user` VALUES (26, '13255389620', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user` VALUES (27, '13690910957', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user` VALUES (28, '13376147486', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-12 19:33:35');
INSERT INTO `tb_user` VALUES (29, '13467015997', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-13 16:52:09');
INSERT INTO `tb_user` VALUES (30, '13248053315', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-14 11:13:24');
INSERT INTO `tb_user` VALUES (31, '13618464100', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user` VALUES (32, '13443447446', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user` VALUES (33, '13125719539', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user` VALUES (34, '13072800811', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user` VALUES (35, '13145571979', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user` VALUES (36, '13720672325', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user` VALUES (37, '13055381341', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-12 20:04:18');
INSERT INTO `tb_user` VALUES (38, '13349885884', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user` VALUES (39, '13347146816', 'e10adc3949ba59abbe56e057f20f883e', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (40, '13179664691', 'e10adc3949ba59abbe56e057f20f883e', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (41, '13766900927', 'e10adc3949ba59abbe56e057f20f883e', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (42, '13151553585', 'e10adc3949ba59abbe56e057f20f883e', '2021-12-30 16:44:23', '2022-01-13 08:45:07');
INSERT INTO `tb_user` VALUES (43, '13391205369', 'e10adc3949ba59abbe56e057f20f883e', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (44, '13133094367', 'e10adc3949ba59abbe56e057f20f883e', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (45, '13281763998', 'e10adc3949ba59abbe56e057f20f883e', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (46, '13936660584', 'e10adc3949ba59abbe56e057f20f883e', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (47, '13052316704', 'e10adc3949ba59abbe56e057f20f883e', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (48, '13324491235', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (49, '13314858500', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (50, '13304162069', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (51, '13228119966', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (52, '13303326951', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-13 08:44:43');
INSERT INTO `tb_user` VALUES (53, '13949043386', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (54, '13117484024', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-13 09:28:22');
INSERT INTO `tb_user` VALUES (55, '13468570608', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (56, '13829424512', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (57, '13667416111', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (58, '13107800128', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (59, '13456540976', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (60, '13986961800', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (61, '13471803074', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-13 10:36:46');
INSERT INTO `tb_user` VALUES (62, '13291235699', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (63, '13165654879', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (64, '13160861931', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (65, '13387208557', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (66, '13021534882', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (67, '13025422987', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (68, '13536312434', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (69, '13935547569', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (70, '13936197370', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (71, '13376796578', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (72, '13773395735', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (73, '13312354710', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-13 14:08:34');
INSERT INTO `tb_user` VALUES (74, '13206863662', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (75, '13041697373', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user` VALUES (76, '13274664831', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-07 16:44:23', '2022-01-13 18:12:45');
INSERT INTO `tb_user` VALUES (77, '13011170045', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-08 16:44:23', '2022-01-08 16:44:23');
INSERT INTO `tb_user` VALUES (78, '13439410129', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-08 16:44:23', '2022-01-08 16:44:23');
INSERT INTO `tb_user` VALUES (79, '13374944361', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-08 16:44:23', '2022-01-12 16:00:21');
INSERT INTO `tb_user` VALUES (80, '13876020928', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-08 16:44:23', '2022-01-13 17:46:35');
INSERT INTO `tb_user` VALUES (81, '13783165166', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (82, '13278257374', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2022-01-13 18:23:00');
INSERT INTO `tb_user` VALUES (83, '17766665555', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-14 08:44:21', '2022-01-15 10:29:07');
INSERT INTO `tb_user` VALUES (84, '13861882191', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2022-01-13 18:04:00');
INSERT INTO `tb_user` VALUES (85, '13789549699', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (86, '13762048705', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2022-01-12 15:59:26');
INSERT INTO `tb_user` VALUES (87, '13816921897', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (88, '13790600059', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (89, '13501033423', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (90, '13185229233', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (91, '13867747315', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (92, '13201151587', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (93, '13992764840', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (94, '13162800744', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (95, '13626499286', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (96, '13790972847', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (97, '13806542590', 'e10adc3949ba59abbe56e057f20f883e', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user` VALUES (98, '13605370098', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-10 16:44:23', '2022-01-10 16:44:23');
INSERT INTO `tb_user` VALUES (99, '13745677232', 'e10adc3949ba59abbe56e057f20f883e', '2022-01-11 16:44:23', '2022-01-11 16:44:23');

-- ----------------------------
-- Table structure for tb_user_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_info`;
CREATE TABLE `tb_user_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `nick_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `logo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `tags` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户标签：多个用逗号分隔',
  `sex` int(1) NULL DEFAULT 3 COMMENT '性别，1-男，2-女，3-未知',
  `age` int(11) NULL DEFAULT NULL COMMENT '用户年龄',
  `edu` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学历',
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '居住城市',
  `birthday` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生日',
  `cover_pic` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面图片',
  `industry` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行业',
  `income` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收入',
  `marriage` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '婚姻状态',
  `created` datetime(0) NULL DEFAULT NULL,
  `updated` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_user_info
-- ----------------------------
INSERT INTO `tb_user_info` VALUES (1, 1, 'heima', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/2019/10/30/15724146111289758.jpg', '单身,本科,年龄相仿', 2, 30, '本科', '北京市-北京城区-东城区', '2019-01-12', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/21.jpg', '计算机行业', '30', '已婚', '2022-01-09 16:43:46', '2022-01-14 09:23:23');
INSERT INTO `tb_user_info` VALUES (2, 2, 'heima_2', 'http://hestanhua.oss-cn-beijing.aliyuncs.com/images/2022/01/12/1641999184071228.jpg', '单身,本科,年龄相仿', 1, 30, '本科', '天津市-天津城区-南开区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/22.jpg', '物流行业', '30', '已婚', '2021-11-07 16:44:23', '2022-01-12 22:53:05');
INSERT INTO `tb_user_info` VALUES (3, 3, 'heima_3', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/18.jpg', '单身,本科,年龄相仿', 1, 45, '本科', '天津市-天津城区-南开区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/18.jpg', '文娱', '40', '未婚', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user_info` VALUES (4, 4, 'heima_4', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/5.jpg', '单身,本科,年龄相仿', 1, 34, '本科', '天津市-天津城区-南开区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/5.jpg', '医疗', '40', '未婚', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user_info` VALUES (5, 5, 'heima_5', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/16.jpg', '单身,本科,年龄相仿', 1, 23, '本科', '天津市-天津城区-南开区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/16.jpg', '金融', '40', '未婚', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user_info` VALUES (6, 6, 'heima_6', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '单身,本科,年龄相仿', 1, 49, '本科', '天津市-天津城区-南开区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '餐饮', '40', '未婚', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user_info` VALUES (7, 7, 'heima_7', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/11.jpg', '单身,本科,年龄相仿', 1, 43, '本科', '天津市-天津城区-南开区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/11.jpg', '教育', '40', '未婚', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user_info` VALUES (8, 8, 'heima_8', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/16.jpg', '单身,本科,年龄相仿', 1, 26, '本科', '天津市-天津城区-南开区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/16.jpg', '住宿', '40', '未婚', '2022-01-05 16:44:23', '2022-01-05 16:44:23');
INSERT INTO `tb_user_info` VALUES (9, 9, 'heima_9', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '单身,本科,年龄相仿', 1, 43, '本科', '天津市-天津城区-南开区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '地产', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (10, 10, 'heima_10', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '单身,本科,年龄相仿', 1, 45, '本科', '天津市-天津城区-南开区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '物流行业', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (11, 11, 'heima_11', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/5.jpg', '单身,本科,年龄相仿', 1, 38, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/5.jpg', '物流行业', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (12, 12, 'heima_12', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '单身,本科,年龄相仿', 1, 49, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '物流行业', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (13, 13, 'heima_13', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '单身,本科,年龄相仿', 2, 36, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '物流行业', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (14, 14, 'heima_14', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/18.jpg', '单身,本科,年龄相仿', 1, 21, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/18.jpg', '物流行业', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (15, 15, 'heima_15', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/17.jpg', '单身,本科,年龄相仿', 1, 48, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/17.jpg', '物流行业', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (16, 16, 'heima_16', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '单身,本科,年龄相仿', 1, 34, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '物流行业', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (17, 17, 'heima_17', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/17.jpg', '单身,本科,年龄相仿', 2, 47, '本科', '安徽省-合肥市-长丰县', '2016-01-12', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/17.jpg', '物流行业', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (18, 18, 'heima_18', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '单身,本科,年龄相仿', 1, 20, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '物流行业', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (19, 19, 'heima_19', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '单身,本科,年龄相仿', 1, 48, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '文娱', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (20, 20, 'heima_20', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '单身,本科,年龄相仿', 1, 21, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '文娱', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (21, 21, 'heima_21', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '单身,本科,年龄相仿', 1, 30, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '文娱', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (22, 22, 'heima_22', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/10.jpg', '单身,本科,年龄相仿', 1, 49, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/10.jpg', '文娱', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (23, 23, 'heima_23', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '单身,本科,年龄相仿', 1, 27, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '文娱', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (24, 24, 'heima_22', 'http://hestanhua.oss-cn-beijing.aliyuncs.com/images/2022/01/15/16422125192219853.jpg', '单身,本科,年龄相仿', 1, 35, '本科', '河北省-保定市-定兴县', '2021-01-15', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '文娱', '40', '未婚', '2021-01-04 16:44:23', '2022-01-15 10:08:43');
INSERT INTO `tb_user_info` VALUES (25, 25, 'heima_25', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/18.jpg', '单身,本科,年龄相仿', 1, 38, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/18.jpg', '医疗', '40', '未婚', '2021-01-04 16:44:23', '2021-01-04 16:44:23');
INSERT INTO `tb_user_info` VALUES (26, 26, 'heima_26', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/18.jpg', '单身,本科,年龄相仿', 1, 35, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/18.jpg', '医疗', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (27, 27, 'heima_27', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/7.jpg', '单身,本科,年龄相仿', 1, 45, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/7.jpg', '医疗', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (28, 28, 'heima_28', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '单身,本科,年龄相仿', 1, 37, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '医疗', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (29, 29, 'heima_29', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/7.jpg', '单身,本科,年龄相仿', 1, 40, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/7.jpg', '医疗', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (30, 30, 'heima_56', 'http://hestanhua.oss-cn-beijing.aliyuncs.com/images/2022/01/14/16421578734806787.jpg', '单身,本科,年龄相仿', 2, 49, '本科', '河北省-保定市-定兴县', '2021-01-14', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/8.jpg', '医疗', '40', '未婚', '2022-01-06 16:44:23', '2022-01-14 18:58:55');
INSERT INTO `tb_user_info` VALUES (31, 31, 'heima_31', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/4.jpg', '单身,本科,年龄相仿', 1, 37, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/4.jpg', '医疗', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (32, 32, 'heima_32', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/10.jpg', '单身,本科,年龄相仿', 1, 44, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/10.jpg', '医疗', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (33, 33, 'heima_33', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '单身,本科,年龄相仿', 1, 26, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '医疗', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (34, 34, 'heima_34', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '单身,本科,年龄相仿', 1, 22, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '医疗', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (35, 35, 'heima_35', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '单身,本科,年龄相仿', 1, 35, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '金融', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (36, 36, 'heima_36', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '单身,本科,年龄相仿', 1, 26, '本科', '河北省-保定市-定兴县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '金融', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (37, 37, 'heima_37', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '单身,本科,年龄相仿', 1, 28, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '金融', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (38, 38, 'heima_38', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '单身,本科,年龄相仿', 1, 48, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '金融', '40', '未婚', '2022-01-06 16:44:23', '2022-01-06 16:44:23');
INSERT INTO `tb_user_info` VALUES (39, 39, 'heima_39', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/1.jpg', '单身,本科,年龄相仿', 1, 42, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/1.jpg', '金融', '40', '未婚', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (40, 40, 'heima_40', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/5.jpg', '单身,本科,年龄相仿', 1, 21, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/5.jpg', '金融', '40', '未婚', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (41, 41, 'heima_41', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '单身,本科,年龄相仿', 1, 25, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '金融', '40', '未婚', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (42, 42, 'heima_42', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '单身,本科,年龄相仿', 1, 41, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '金融', '40', '未婚', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (43, 43, 'heima_43', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '单身,本科,年龄相仿', 1, 41, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '金融', '40', '未婚', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (44, 44, 'heima_44', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/5.jpg', '单身,本科,年龄相仿', 1, 46, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/5.jpg', '金融', '40', '未婚', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (45, 45, 'heima_45', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '单身,本科,年龄相仿', 1, 39, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '金融', '40', '未婚', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (46, 46, 'heima_46', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/3.jpg', '单身,本科,年龄相仿', 1, 27, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/3.jpg', '金融', '40', '未婚', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (47, 47, 'heima_47', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '单身,本科,年龄相仿', 1, 45, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '金融', '40', '未婚', '2021-12-30 16:44:23', '2021-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (48, 48, 'heima_48', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '单身,本科,年龄相仿', 1, 25, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '金融', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (49, 49, 'heima_49', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '单身,本科,年龄相仿', 1, 36, '本科', '云南省-玉溪市-华宁县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '金融', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (50, 50, 'heima_50', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '单身,本科,年龄相仿', 1, 23, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '金融', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (51, 51, 'heima_51', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/8.jpg', '单身,本科,年龄相仿', 1, 42, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/8.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (52, 52, 'heima_52', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/11.jpg', '单身,本科,年龄相仿', 1, 26, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/11.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (53, 53, 'heima_53', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '单身,本科,年龄相仿', 1, 23, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (54, 54, 'heima_54', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '单身,本科,年龄相仿', 1, 37, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (55, 55, 'heima_55', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/3.jpg', '单身,本科,年龄相仿', 1, 38, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/3.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (56, 56, 'heima_56', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '单身,本科,年龄相仿', 1, 40, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (57, 57, 'heima_57', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/4.jpg', '单身,本科,年龄相仿', 1, 40, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/4.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (58, 58, 'heima_58', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/7.jpg', '单身,本科,年龄相仿', 1, 33, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/7.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (59, 59, 'heima_59', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '单身,本科,年龄相仿', 1, 29, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (60, 60, 'heima_60', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/10.jpg', '单身,本科,年龄相仿', 1, 21, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/10.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (61, 61, 'heima_61', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/16.jpg', '单身,本科,年龄相仿', 1, 49, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/16.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (62, 62, 'heima_62', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/4.jpg', '单身,本科,年龄相仿', 1, 47, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/4.jpg', '餐饮', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (63, 63, 'heima_63', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '单身,本科,年龄相仿', 1, 35, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '教育', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (64, 64, 'heima_64', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/1.jpg', '单身,本科,年龄相仿', 1, 25, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/1.jpg', '教育', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (65, 65, 'heima_65', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '单身,本科,年龄相仿', 1, 43, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/13.jpg', '教育', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (66, 66, 'heima_66', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/16.jpg', '单身,本科,年龄相仿', 1, 28, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/16.jpg', '教育', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (67, 67, 'heima_67', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '单身,本科,年龄相仿', 1, 37, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '教育', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (68, 68, 'heima_68', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '单身,本科,年龄相仿', 1, 36, '本科', '吉林省-白山市-抚松县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '教育', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (69, 69, 'heima_69', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/8.jpg', '单身,本科,年龄相仿', 1, 30, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/8.jpg', '住宿', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (70, 70, 'heima_70', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/11.jpg', '单身,本科,年龄相仿', 1, 24, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/11.jpg', '住宿', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (71, 71, 'heima_71', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/10.jpg', '单身,本科,年龄相仿', 1, 48, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/10.jpg', '住宿', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (72, 72, 'heima_72', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/3.jpg', '单身,本科,年龄相仿', 1, 47, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/3.jpg', '住宿', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (73, 73, 'heima_73', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '单身,本科,年龄相仿', 1, 43, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '住宿', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (74, 74, 'heima_74', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/14.jpg', '单身,本科,年龄相仿', 1, 42, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/14.jpg', '住宿', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (75, 75, 'heima_75', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/1.jpg', '单身,本科,年龄相仿', 1, 37, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/1.jpg', '住宿', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (76, 76, 'heima_76', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '单身,本科,年龄相仿', 1, 24, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '住宿', '40', '未婚', '2022-01-07 16:44:23', '2022-01-07 16:44:23');
INSERT INTO `tb_user_info` VALUES (77, 77, 'heima_77', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/10.jpg', '单身,本科,年龄相仿', 1, 23, '本科', '安徽省-合肥市-长丰县', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/10.jpg', '地产', '40', '未婚', '2022-01-08 16:44:23', '2022-01-08 16:44:23');
INSERT INTO `tb_user_info` VALUES (78, 78, 'heima_78', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '单身,本科,年龄相仿', 1, 38, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '地产', '40', '未婚', '2022-01-08 16:44:23', '2022-01-08 16:44:23');
INSERT INTO `tb_user_info` VALUES (79, 79, 'heima_79', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/3.jpg', '单身,本科,年龄相仿', 1, 20, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/3.jpg', '地产', '40', '未婚', '2022-01-08 16:44:23', '2022-01-08 16:44:23');
INSERT INTO `tb_user_info` VALUES (80, 80, 'heima_80', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/17.jpg', '单身,本科,年龄相仿', 1, 47, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/17.jpg', '地产', '40', '未婚', '2022-01-08 16:44:23', '2022-01-08 16:44:23');
INSERT INTO `tb_user_info` VALUES (81, 81, 'heima_81', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '单身,本科,年龄相仿', 1, 41, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '制造', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (82, 82, 'heima_82', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/8.jpg', '单身,本科,年龄相仿', 1, 41, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/8.jpg', '制造', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (83, 83, 'heima_83', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '单身,本科,年龄相仿', 1, 41, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '制造', '40', '未婚', '2022-01-14 08:44:21', '2022-01-14 08:44:21');
INSERT INTO `tb_user_info` VALUES (84, 84, 'heima_84', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '单身,本科,年龄相仿', 1, 43, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '服务', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (85, 85, 'heima_85', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '单身,本科,年龄相仿', 1, 38, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '服务', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (86, 86, 'heima_86', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '单身,本科,年龄相仿', 1, 34, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/15.jpg', '服务', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (87, 87, 'heima_87', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '单身,本科,年龄相仿', 1, 29, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '服务', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (88, 88, 'heima_88', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '单身,本科,年龄相仿', 1, 34, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/6.jpg', '服务', '40', '未婚', '2022-01-14 16:44:23', '2022-01-14 16:44:23');
INSERT INTO `tb_user_info` VALUES (89, 89, 'heima_89', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '单身,本科,年龄相仿', 1, 34, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '服务', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (90, 90, 'heima_90', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/14.jpg', '单身,本科,年龄相仿', 1, 45, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/14.jpg', '地产', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (91, 91, 'heima_91', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/4.jpg', '单身,本科,年龄相仿', 1, 27, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/4.jpg', '地产', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (92, 92, 'heima_92', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '单身,本科,年龄相仿', 1, 45, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/2.jpg', '地产', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (93, 93, 'heima_93', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '单身,本科,年龄相仿', 1, 30, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '地产', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (94, 94, 'heima_94', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/8.jpg', '单身,本科,年龄相仿', 1, 48, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/8.jpg', '地产', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (95, 95, 'heima_95', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/11.jpg', '单身,本科,年龄相仿', 1, 32, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/11.jpg', '其他', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (96, 96, 'heima_96', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '单身,本科,年龄相仿', 1, 41, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/19.jpg', '其他', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (97, 97, 'heima_97', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '单身,本科,年龄相仿', 1, 23, '本科', '安徽省-合肥市-瑶海区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/12.jpg', '其他', '40', '未婚', '2020-12-30 16:44:23', '2020-12-30 16:44:23');
INSERT INTO `tb_user_info` VALUES (98, 98, 'heima_98', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/7.jpg', '单身,本科,年龄相仿', 1, 28, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/7.jpg', '其他', '40', '未婚', '2022-01-10 16:44:23', '2022-01-10 16:44:23');
INSERT INTO `tb_user_info` VALUES (99, 99, 'heima_99', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '单身,本科,年龄相仿', 1, 29, '本科', '北京市-北京城区-东城区', '2019-08-01', 'http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg', '其他', '40', '未婚', '2022-01-11 16:44:23', '2022-01-11 16:44:23');

SET FOREIGN_KEY_CHECKS = 1;
