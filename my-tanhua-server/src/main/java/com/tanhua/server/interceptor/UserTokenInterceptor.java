package com.tanhua.server.interceptor;

import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.common.pojo.User;
import com.tanhua.common.utils.NoAuthorization;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.dubbo.server.api.DashBoardApi;
import com.tanhua.server.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Configuration
public class UserTokenInterceptor implements HandlerInterceptor {
    @Autowired
    private UserService userService;
    @Reference
    private DashBoardApi dashBoardApi;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //校验是否为HandlerMethod对象
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        //直接放行包含NoAuthorization注解的方法
        if (((HandlerMethod) handler).hasMethodAnnotation(NoAuthorization.class)) {
            return true;
        }
        //从请求头中获取token
        String token = request.getHeader("Authorization");
        //校验token是否为空
        if (StringUtils.isNotEmpty(token)) {
            //解析token
            User user = null;
            try {
                user = userService.queryToken(token);
            } catch (Exception e) {
                response.setStatus(401);
                return false;
            }


            String requestURI = request.getRequestURI();//   /comment

            String method = request.getMethod();// GET POST


            String status = redisTemplate.opsForValue().get("FROZEN_" + user.getId());

            // 若用户冻结,返回主界面
            if (StrUtil.equals(status, "1")) {
                response.setStatus(401);
                return false;
            }

            /*if (StrUtil.equals(status, "2") && StrUtil.equals(requestURI, "/comments") && StrUtil.equals(method, "POST")) {
                response.setStatus(401);
                return false;
            }

            if (StrUtil.equals(status, "3") && StrUtil.equals(requestURI, "/movements") && StrUtil.equals(method, "POST")) {
                response.setStatus(401);
                return false;
            }*/


            //记录用户活跃
            dashBoardApi.saveUserActive(user.getId(), 0L);
            //token有效则存入本地线程栈
            if (user != null) {
                UserThreadLocal.set(user);
                return true;
            }
        }
        //token无效则响应状态401(无权限)
        response.setStatus(401);
        return false;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //从本地线程栈中移除user对象
        UserThreadLocal.remove();
    }
}
