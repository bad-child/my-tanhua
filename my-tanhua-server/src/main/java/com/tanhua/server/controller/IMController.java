package com.tanhua.server.controller;

import cn.hutool.core.convert.Convert;
import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.server.service.IMService;
import com.tanhua.server.vo.PageResp;
import com.tanhua.server.vo.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("messages")
public class IMController {
    @Autowired
    private IMService imService;

    /**
     * 根据环信用户名查询用户信息
     *
     * @param userName 环信用户名(HX_1)
     * @return
     */
    @GetMapping("userinfo")
    public ResponseEntity queryUserInfoByUserName(@RequestParam("huanxinId") String userName) {
        UserInfoVo userInfoVo = imService.queryUserInfoByUserName(userName);
        return ResponseEntity.ok(userInfoVo);
    }

    /**
     * 添加好友
     *
     * @param param 好友id
     * @return
     */
    @PostMapping("contacts")
    public ResponseEntity contactUser(@RequestBody Map<String, Integer> param) {
        imService.contactUser(Convert.toLong(param.get("userId")));
        return ResponseEntity.ok(null);
    }

    /**
     * 查询联系人列表
     *
     * @param page
     * @param pageSize
     * @param keyword  关键字
     * @return
     */
    @GetMapping("contacts")
    public ResponseEntity queryContactsList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize,
                                            @RequestParam(value = "keyword", required = false) String keyword) {
        PageResp pageResp = imService.queryContactsList(page, pageSize, keyword);
        return ResponseEntity.ok(pageResp);
    }

    /**
     * 查询消息点赞列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("likes")
    public ResponseEntity queryLikeCommentList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                               @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        PageResp resp = imService.queryCommentList(page, pageSize, CommentType.LIKE);
        return ResponseEntity.ok(resp);
    }

    /**
     * 查询消息评论列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("comments")
    public ResponseEntity queryUserCommentList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                               @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        PageResp resp = imService.queryCommentList(page, pageSize, CommentType.COMMENT);
        return ResponseEntity.ok(resp);
    }

    /**
     * 查询消息喜欢列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("loves")
    public ResponseEntity queryLoveCommentList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                               @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        PageResp resp = imService.queryCommentList(page, pageSize, CommentType.LOVE);
        return ResponseEntity.ok(resp);
    }

    /**
     * 查询系统公告
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("announcements")
    public ResponseEntity queryAnnouncementsList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                 @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        PageResp resp = imService.queryAnnouncementsList(page, pageSize);
        return ResponseEntity.ok(resp);
    }
}
