package com.tanhua.server.controller;

import com.tanhua.server.service.QuanZiService;
import com.tanhua.server.service.VideoService;
import com.tanhua.server.vo.PageResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequestMapping("smallVideos")
public class VideoController {
    @Autowired
    private VideoService videoService;
    @Autowired
    private QuanZiService quanZiService;

    /**
     * 查询视频列表
     *
     * @param page
     * @param pagesize
     * @return
     */
    @GetMapping
    public ResponseEntity queryVideoList(@RequestParam(defaultValue = "1") Integer page,
                                         @RequestParam(defaultValue = "10") Integer pagesize) {
        PageResp resp = videoService.queryVideoList(page, pagesize);
        return ResponseEntity.ok(resp);
    }

    /**
     * 发布视频
     *
     * @param videoThumbnail 视频封面
     * @param videoFile      视频
     * @return
     */
    @PostMapping
    public ResponseEntity saveVideo(MultipartFile videoThumbnail, MultipartFile videoFile) {
        videoService.saveVideo(videoThumbnail, videoFile);
        return ResponseEntity.ok(null);
    }

    /**
     * 查询视频评论列表
     *
     * @param videoId  视频id
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping({"{id}/comments"})
    public ResponseEntity queryCommentsList(@PathVariable("id") String videoId,
                                            @RequestParam(value = "page", defaultValue = "1") Integer page,
                                            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        PageResp resp = quanZiService.queryCommentList(videoId, page, pageSize);
        return ResponseEntity.ok(resp);
    }

    /**
     * 发布视频评论
     *
     * @param param   评论正文
     * @param videoId 视频id
     * @return
     */
    @PostMapping("{id}/comments")
    public ResponseEntity saveComments(@RequestBody Map<String, String> param,
                                       @PathVariable("id") String videoId) {
        String comment = param.get("comment");
        quanZiService.saveComment(videoId, comment);
        return ResponseEntity.ok(null);
    }

    /**
     * 视频用户关注
     *
     * @param id 视频发布者id
     * @return
     */
    @PostMapping("{uid}/userFocus")
    public ResponseEntity saveUserFocusComments(@PathVariable("uid") Long id) {
        videoService.saveUserFocusComments(id);
        return ResponseEntity.ok(null);
    }

    /**
     * 取消视频用户关注
     *
     * @param id 视频发布者id
     * @return
     */
    @PostMapping("{uid}/userUnFocus")
    public ResponseEntity saveUserUnFocusComments(@PathVariable("uid") Long id) {
        videoService.saveUserUnFocusComments(id);
        return ResponseEntity.ok(null);
    }

    /**
     * 点赞视频
     *
     * @param videoId 视频id
     * @return
     */
    @PostMapping("{id}/like")
    public ResponseEntity likeVideo(@PathVariable("id") String videoId) {
        quanZiService.likeComment(videoId);
        return ResponseEntity.ok(null);
    }

    /**
     * 取消点赞视频
     *
     * @param videoId 视频id
     * @return
     */
    @PostMapping("{id}/dislike")
    public ResponseEntity disLikeVideo(@PathVariable("id") String videoId) {
        quanZiService.disLikeComment(videoId);
        return ResponseEntity.ok(null);
    }

    /**
     * 点赞视频评论
     *
     * @param commentsId 评论id
     * @return
     */
    @PostMapping("comments/{id}/like")
    public ResponseEntity likeVideoComments(@PathVariable("id") String commentsId) {
        quanZiService.likeComment(commentsId);
        return ResponseEntity.ok(null);
    }

    /**
     * 取消点赞视频评论
     *
     * @param commentsId 评论id
     * @return
     */
    @PostMapping("comments/{id}/dislike")
    public ResponseEntity disLikeVideoComments(@PathVariable("id") String commentsId) {
        quanZiService.disLikeComment(commentsId);
        return ResponseEntity.ok(null);
    }
}
