package com.tanhua.server.controller;

import com.tanhua.dubbo.server.pojo.Answers;
import com.tanhua.server.service.TestSoulService;
import com.tanhua.dubbo.server.vo.QuestionnaireVo;
import com.tanhua.dubbo.server.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("testSoul")
public class TestSoulController {
    @Autowired
    private TestSoulService testSoulService;

    /**
     * 提交问卷
     *
     * @param answers
     * @return
     */
    @PostMapping
    public ResponseEntity<String> queryReport(@RequestBody Map<String, List<Answers>> answers) {
        List<Answers> answersList = answers.get("answers");
        String reportId=testSoulService.testSoul(answersList);
        return ResponseEntity.ok(reportId);
    }

    /**
     * 问卷列表
     *
     * @return
     */
    @GetMapping
    public List<QuestionnaireVo> SoulQuestionnaire() {
        List<QuestionnaireVo> questionnaireVos = testSoulService.queryQuestionnaire();
        return questionnaireVos;
    }

    /**
     * @param id 报告id
     * @return
     */
    @GetMapping("report/{id}")
    public ResponseEntity<ResultVo> TestResult(@PathVariable("id") String id) {
        ResultVo resultVo = testSoulService.testResult(id);
        return ResponseEntity.ok(resultVo);
    }

}
