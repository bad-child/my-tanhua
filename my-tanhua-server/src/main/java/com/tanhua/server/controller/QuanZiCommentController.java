package com.tanhua.server.controller;

import cn.hutool.core.util.StrUtil;
import com.tanhua.server.service.QuanZiService;
import com.tanhua.server.vo.PageResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("comments")
public class QuanZiCommentController {
    @Autowired
    private QuanZiService quanZiService;

    /**
     * 查询评论列表
     *
     * @param movementId 评论id
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping
    public ResponseEntity queryCommentsList(String movementId,
                                            @RequestParam(defaultValue = "1") Integer page,
                                            @RequestParam(defaultValue = "10") Integer pageSize) {
        PageResp pageResp = quanZiService.queryCommentList(movementId, page, pageSize);
        return ResponseEntity.ok(pageResp);
    }

    /**
     * 评论动态
     *
     * @param params
     * @return
     */
    @PostMapping
    public ResponseEntity saveComments(@RequestBody Map<String, String> params) {
        String publishId = params.get("movementId");
        String comment = params.get("comment");
        String result = quanZiService.saveComment(publishId, comment);

        if (StrUtil.isNotEmpty(result)){
            return ResponseEntity.status(500).body(result);
        }
        return ResponseEntity.ok(null);
    }

    /**
     * 点赞评论
     *
     * @param id
     * @return
     */
    @GetMapping("{id}/like")
    public ResponseEntity likeComment(@PathVariable("id") String id) {
        Long likeCount = quanZiService.likeComment(id);
        return ResponseEntity.ok(likeCount);
    }

    /**
     * 取消点赞评论
     *
     * @param id
     * @return
     */
    @GetMapping("{id}/dislike")
    public ResponseEntity disLikeComment(@PathVariable("id") String id) {
        Long likeCount = quanZiService.disLikeComment(id);
        return ResponseEntity.ok(likeCount);
    }

}
