package com.tanhua.server.controller;

import cn.hutool.core.convert.Convert;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.server.service.BaiduService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("baidu")
public class BaiduController {
    @Autowired
    private BaiduService baiduService;
    /**
     * 上报地理信息
     *
     * @param map latitude 纬度
     *            longitude 经度
     *            addrStr 位置描述
     * @return
     */
    @PostMapping("location")
    public ResponseEntity updateLocation(@RequestBody Map<String, Object> map) {
        Double latitude = Convert.toDouble(map.get("latitude"));
        Double longitude = Convert.toDouble(map.get("longitude"));
        String addrStr = map.get("addrStr").toString();
        baiduService.updateLocation(longitude, latitude, addrStr);
        return ResponseEntity.ok(null);
    }
}
