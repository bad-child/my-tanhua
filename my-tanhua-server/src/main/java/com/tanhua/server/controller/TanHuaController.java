package com.tanhua.server.controller;

import cn.hutool.core.convert.Convert;
import com.tanhua.server.service.TanHuaService;
import com.tanhua.server.vo.NearUserVo;
import com.tanhua.server.vo.TodayBest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("tanhua")
public class TanHuaController {
    @Autowired
    private TanHuaService tanHuaService;

    /**
     * 查询个人主页的个人信息
     *
     * @param userId 个人主页用户id
     * @return
     */
    @GetMapping("{id}/personalInfo")
    public ResponseEntity queryUserInfo(@PathVariable("id") Long userId) {
        TodayBest todayBest = tanHuaService.queryUserInfo(userId);
        return ResponseEntity.ok(todayBest);
    }

    /**
     * 查询陌生人问题
     *
     * @param userId 目标用户id
     * @return
     */
    @GetMapping("strangerQuestions")
    public ResponseEntity queryQuestion(Long userId) {
        String question = tanHuaService.queryQuestion(userId);
        return ResponseEntity.ok(question);
    }

    /**
     * 回复陌生人问题
     *
     * @param param 陌生人id  回答
     * @return
     */
    @PostMapping("strangerQuestions")
    public ResponseEntity replyQuestion(@RequestBody Map<String, Object> param) {
        Long userId = Long.valueOf(param.get("userId").toString());
        String reply = param.get("reply").toString();
        tanHuaService.replyQuestion(userId, reply);
        return ResponseEntity.ok(null);
    }

    /**
     * 搜附近
     *
     * @param gender   性别
     * @param distance 距离
     * @return
     */
    @GetMapping("search")
    public ResponseEntity queryNearUser(@RequestParam(value = "gender") String gender,
                                        @RequestParam(value = "distance", defaultValue = "2000") String distance) {
        List<NearUserVo> nearUserVoList = tanHuaService.queryNearUse(gender, distance);
        return ResponseEntity.ok(nearUserVoList);
    }

    /**
     * 探花-查询推荐列表
     *
     * @return
     */
    @GetMapping("cards")
    public ResponseEntity queryCardsList() {
        List<TodayBest> todayBestList = tanHuaService.queryCardsList();
        return ResponseEntity.ok(todayBestList);
    }


    /**
     * 探花-喜欢
     *
     * @param likeUserId 被喜欢用户id
     * @return
     */
    @GetMapping("{id}/love")
    public ResponseEntity likeUser(@PathVariable("id") String likeUserId) {
        tanHuaService.likeUser(Convert.toLong(likeUserId));
        return ResponseEntity.ok(null);
    }

    /**
     * 探花-不喜欢
     *
     * @param likeUserId 被不喜欢用户id
     * @return
     */
    @GetMapping("{id}/unlove")
    public ResponseEntity notLikeUser(@PathVariable("id") String likeUserId) {
        tanHuaService.notLikeUser(Convert.toLong(likeUserId));
        return ResponseEntity.ok(null);
    }
}
