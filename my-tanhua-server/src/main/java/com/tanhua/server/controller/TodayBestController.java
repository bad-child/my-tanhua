package com.tanhua.server.controller;

import com.tanhua.dubbo.server.vo.PageParams;
import com.tanhua.server.service.TodayBestService;
import com.tanhua.server.vo.PageResp;
import com.tanhua.server.vo.TodayBest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("tanhua")
public class TodayBestController {
    @Autowired
    private TodayBestService todayBestService;

    /**
     * 查询和当前用户score值最高的用户
     *
     * @return
     */
    @GetMapping("todayBest")
    //@Cache
    public ResponseEntity todayBest() {
        TodayBest todayBest = todayBestService.todayBest();
        return ResponseEntity.ok(todayBest);
    }

    /**
     * 查询当前用户的推荐列表
     *
     * @param pageParams 筛选条件
     * @return
     */
    @GetMapping("recommendation")
    //@Cache
    public ResponseEntity recommendation(PageParams pageParams) {
        PageResp pageResp = todayBestService.recommendation(pageParams);
        return ResponseEntity.ok(pageResp);
    }
}
