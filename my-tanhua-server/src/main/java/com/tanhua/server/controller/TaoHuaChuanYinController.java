package com.tanhua.server.controller;


import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.vo.ErrorResult;
import com.tanhua.server.service.TaoHuaChuanYinService;
import com.tanhua.server.vo.SoundVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("peachblossom")
public class TaoHuaChuanYinController {
    @Autowired
    private TaoHuaChuanYinService taoHuaChuanYinService;

    @PostMapping
    public String sendVoice(@RequestParam(value = "soundFile", required = false) MultipartFile soundFile) {
        boolean b = this.taoHuaChuanYinService.saveSound(soundFile);
        if (!b) {
            throw new BusinessException(ErrorResult.timeOver());
        }
        return "上传成功";
    }

    //要获取一个异性，不是自己的，不是重复的声音对象
    @GetMapping
    public SoundVo getVoice() {
        SoundVo soundVo = this.taoHuaChuanYinService.querySound();
        if (soundVo == null) {
            throw new BusinessException(ErrorResult.timeOver());
        } return soundVo;
    }
}