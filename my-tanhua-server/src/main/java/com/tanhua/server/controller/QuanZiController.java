package com.tanhua.server.controller;

import cn.hutool.core.util.StrUtil;
import com.tanhua.server.service.QuanZiService;
import com.tanhua.server.vo.PageResp;
import com.tanhua.server.vo.QuanZiVo;
import com.tanhua.server.vo.VisitorsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("movements")
public class QuanZiController {
    @Autowired
    private QuanZiService quanZiService;

    /**
     * 查询好友动态
     *
     * @param page
     * @param pagesize
     * @return
     */
    @GetMapping
    public ResponseEntity movements(Integer page, Integer pagesize) {
        PageResp pageResp = quanZiService.queryFriendList(page, pagesize);
        return ResponseEntity.ok(pageResp);
    }

    /**
     * 查询系统推荐动态
     *
     * @param page
     * @param pagesize
     * @return
     */
    @GetMapping("recommend")
    public ResponseEntity recommend(Integer page, Integer pagesize) {
        PageResp pageResp = quanZiService.queryRecommendList(page, pagesize);
        return ResponseEntity.ok(pageResp);
    }

    /**
     * 发布动态
     *
     * @param textContent
     * @param location
     * @param latitude
     * @param longitude
     * @param multipartFile
     * @return
     */
    @PostMapping
    public ResponseEntity savePublish(@RequestParam("textContent") String textContent,
                                      @RequestParam(value = "location", required = false) String location,
                                      @RequestParam(value = "latitude", required = false) String latitude,
                                      @RequestParam(value = "longitude", required = false) String longitude,
                                      @RequestParam(value = "imageContent", required = false) MultipartFile[] multipartFile) {
        String result = quanZiService.savePublish(textContent, location, latitude, longitude, multipartFile);

        if (StrUtil.isNotEmpty(result)){
            return ResponseEntity.status(500).body(result);
        }

        return ResponseEntity.ok(null);
    }

    /**
     * 点赞动态,返回操作后的点赞数
     *
     * @param publishId 动态id
     * @return
     */
    @GetMapping("{id}/like")
    public ResponseEntity likeComment(@PathVariable("id") String publishId) {
        Long count = quanZiService.likeComment(publishId);
        return ResponseEntity.ok(count);
    }

    /**
     * 取消点赞动态,返回操作后的点赞数
     *
     * @param publishId 动态id
     * @return
     */
    @GetMapping("{id}/dislike")
    public ResponseEntity disLikeComment(@PathVariable("id") String publishId) {
        Long count = quanZiService.disLikeComment(publishId);
        return ResponseEntity.ok(count);
    }

    /**
     * 喜欢动态,返回操作后的喜欢数
     *
     * @param publishId 动态id
     * @return
     */
    @GetMapping("{id}/love")
    public ResponseEntity loveComment(@PathVariable("id") String publishId) {
        Long count = quanZiService.loveComment(publishId);
        return ResponseEntity.ok(count);
    }

    /**
     * 取消喜欢动态,返回操作后的喜欢数
     *
     * @param publishId 动态id
     * @return
     */
    @GetMapping("{id}/unlove")
    public ResponseEntity disLoveComment(@PathVariable("id") String publishId) {
        Long count = quanZiService.disLoveComment(publishId);
        return ResponseEntity.ok(count);
    }

    /**
     * 查询单个动态信息
     *
     * @param publishId 动态id
     * @return
     */
    @GetMapping("{id}")
    public ResponseEntity queryById(@PathVariable("id") String publishId) {
        QuanZiVo quanZiVo = quanZiService.queryById(publishId);
        return ResponseEntity.ok(quanZiVo);
    }

    /**
     * 查询个人所有动态
     *
     * @param page
     * @param pageSize
     * @param userId   目标用户id
     * @return
     */
    @GetMapping("all")
    public ResponseEntity queryAlbumList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                         @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize,
                                         @RequestParam(value = "userId") Long userId) {
        PageResp pageResp = quanZiService.queryAlbumList(userId, page, pageSize);
        return ResponseEntity.ok(pageResp);
    }

    /**
     * 谁看过我
     *
     * @return
     */
    @GetMapping("visitors")
    public ResponseEntity<Object> queryVisitors() {
        List<VisitorsVo> list = quanZiService.queryVisitorsList();
        return ResponseEntity.ok(list);
    }
}
