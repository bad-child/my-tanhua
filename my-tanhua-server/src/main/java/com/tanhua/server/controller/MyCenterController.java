package com.tanhua.server.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Method;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.server.enums.LoveType;
import com.tanhua.server.service.MyCenterService;
import com.tanhua.server.vo.LoveCountVo;
import com.tanhua.server.vo.PageResp;
import com.tanhua.server.vo.SettingsVo;
import com.tanhua.server.vo.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("users")
public class MyCenterController {
    @Autowired
    private MyCenterService myCenterService;
    @Autowired
    private UserInfoMapper userInfoMapper;


    /**
     * 根据用户id查询用户信息
     *
     * @param userId 用户id,为空则表示查询当前登录者的信息
     * @return
     */
    @GetMapping
    public ResponseEntity queryUserInfoByUserId(@RequestParam(value = "userID", required = false) Long userId) {
        UserInfoVo userInfoVo = myCenterService.queryUserInfoByUserId(userId);
        return ResponseEntity.ok(userInfoVo);
    }

    /**
     * 查询黑名单列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("blacklist")
    public ResponseEntity queryBlackList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                         @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        PageResp pageResp = myCenterService.queryBlackList(page, pageSize);
        return ResponseEntity.ok(pageResp);
    }

    /**
     * 移除黑名单
     *
     * @param uid 被移除用户id
     * @return
     */
    @DeleteMapping("blacklist/{uid}")
    public ResponseEntity removeBlackList(@PathVariable("uid") String uid) {
        myCenterService.removeBlackList(Long.valueOf(uid));
        return ResponseEntity.ok(null);
    }

    /**
     * 发送修改手机号需要的验证码
     *
     * @return
     */
    @PostMapping("phone/sendVerificationCode")
    public ResponseEntity changePhoneSendVerificationCode() {
        myCenterService.changePhoneSendVerificationCode();
        return ResponseEntity.ok(null);
    }

    /**
     * 验证修改手机号需要的验证码
     *
     * @param map 验证码
     * @return
     */
    @PostMapping("phone/checkVerificationCode")
    public ResponseEntity checkPhoneSendVerificationCode(@RequestBody Map<String, String> map) {
        String verificationCode = map.get("verificationCode");
        Map<String, Object> result = myCenterService.checkPhoneSendVerificationCode(verificationCode);
        return ResponseEntity.ok(result);
    }

    /**
     * 保存修改后的手机号
     *
     * @param map 新手机号
     * @return
     */
    @PostMapping("phone")
    public ResponseEntity saveNewPhone(@RequestBody Map<String, String> map) {

        String phone = map.get("phone");
        myCenterService.saveNewPhone(phone);
        return ResponseEntity.ok(null);
    }

    //互相喜欢、喜欢、粉丝、谁看过我
    @GetMapping("counts")
    public ResponseEntity<LoveCountVo> queryLoveCount() {
        //调用查询互相喜欢、喜欢、粉丝、谁看过我queryLoveCount方法
        LoveCountVo loveCountVo = myCenterService.queryLoveCount();
        //判断返回loveCountVo对象是否为空再返回给前端
        if (ObjectUtil.isNotEmpty(loveCountVo)) {
            return ResponseEntity.ok(loveCountVo);
        }
        return null;
    }

    //互相喜欢、喜欢、粉丝、谁看过我 - 翻页列表
    @GetMapping("friends/{type}")
    public PageResp queryFriends(@PathVariable("type") String type,
                                 @RequestParam(value = "page", defaultValue = "1", required = false) Integer page,
                                 @RequestParam(value = "pagesize", defaultValue = "10", required = false) Integer pageSize,
                                 @RequestParam(value = "nickname", required = false) String nickName) {
        //调用互相喜欢、喜欢、粉丝、谁看过我 - 翻页列表queryLoveCountList方法
        LoveType loveType = LoveType.getTypeByString(type);
        PageResp pageResp = myCenterService.queryLoveCountList(loveType, page, pageSize,nickName);

        //判断返回的pageResp对象是否为空
        if (ObjectUtil.isNotEmpty(pageResp)) {
            return pageResp;
        }
        return null;
    }

    /**
     * 修改用户信息
     *
     * @param userInfoVo
     * @return
     */
    @PutMapping
    public ResponseEntity saveMessage(@RequestBody UserInfoVo userInfoVo) {
        myCenterService.saveMessage(userInfoVo);
        return ResponseEntity.ok(null);
    }

    /**
     * 取消喜欢
     *
     * @param userId
     * @return
     */
    @DeleteMapping("like/{uid}")
    private ResponseEntity cancelLike(@PathVariable("uid") Long userId) {
        myCenterService.cancelLike(userId);
        return ResponseEntity.ok(null);
    }


    /**
     * 回关粉丝
     *
     * @param userId
     * @return
     */
    @PostMapping("fans/{uid}")
    private ResponseEntity seeLike(@PathVariable("uid") Long userId) {

        myCenterService.seeLike(userId);
        return ResponseEntity.ok(null);
    }

    /**
     * 设置问题保存
     *
     * @param map
     * @return
     */
    @PostMapping("questions")
    public ResponseEntity saveQuestions(@RequestBody Map<String, String> map) {
        myCenterService.saveQuestions(map.get("content"));
        return ResponseEntity.ok(null);
    }


    /**
     * 通知设置
     *
     * @param map
     */
    @PostMapping("notifications/setting")
    private void notice(@RequestBody Map<String, Boolean> map) {

        Boolean likeNotification = map.get("likeNotification");
        Boolean pinglunNotification = map.get("pinglunNotification");
        Boolean gonggaoNotification = map.get("gonggaoNotification");

        myCenterService.notice(likeNotification, pinglunNotification, gonggaoNotification);

    }

    /**
     * 用户设置
     *
     * @return
     */
    @GetMapping("settings")
    private ResponseEntity<SettingsVo> queryUserSetting() {


        SettingsVo settingsVo = myCenterService.queryUserSetting();
        return ResponseEntity.ok(settingsVo);

    }

    /**
     * 是否喜欢
     *
     * @param userId
     * @return
     */
    @GetMapping("{uid}/alreadyLove")
    private boolean isNotLove(@PathVariable("uid") Long userId) {

        return myCenterService.isNotLove(userId);
    }

    /**
     * 保存头像
     *
     * @param token
     * @param headPhoto
     * @return
     */
    @PostMapping("header")
    public ResponseEntity head(@RequestHeader("Authorization") String token, MultipartFile headPhoto) {
        myCenterService.head(token, headPhoto);
        return ResponseEntity.ok(null);
    }

}


