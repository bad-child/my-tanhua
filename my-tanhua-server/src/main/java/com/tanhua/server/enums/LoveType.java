package com.tanhua.server.enums;

//互相喜欢、喜欢、粉丝、谁看过我枚举
public enum LoveType {

    FOCUS_ON_EACH_OTHER(1, "互相关注"),
    MY_FOCUS(2, "我关注"),
    FANS(3, "粉丝"),
    WHO_LOOK_ME(4,"谁看过我");

    private Integer type;
    private String desc;

    LoveType(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    //根据type获取decs
    public static LoveType getTypeByString(String type) {
        for (LoveType loveType : LoveType.values()) {
            if (loveType.type == Integer.parseInt(type)) {
                return loveType;
            }
        }
        return null;
    }
}
