package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.pojo.Publish;
import com.tanhua.dubbo.server.pojo.TimeLine;
import com.tanhua.dubbo.server.pojo.Users;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class TimeLineService {
    @Reference
    private QuanZiApi quanZiApi;

    /**
     * 向用户好友的时间线表中写入数据
     *
     * @param id      登录用户id
     * @param publish 动态id
     * @return
     */
    @Async
    public CompletableFuture<String> saveTimeLine(Long id, Publish publish) {
        try {
            List<Users> usersList = quanZiApi.queryFriendIds(id);
            List<Object> friendIds = CollUtil.getFieldValues(usersList, "friendId");
            for (Object friendId : friendIds) {
                TimeLine timeLine = new TimeLine();
                timeLine.setUserId(publish.getUserId());
                timeLine.setPublishId(publish.getId());
                timeLine.setDate(publish.getCreated());
                quanZiApi.saveTimeLine(friendId, timeLine);
            }
        } catch (Exception e) {
            return CompletableFuture.completedFuture("error");
        }
        return CompletableFuture.completedFuture("ok");
    }
}
