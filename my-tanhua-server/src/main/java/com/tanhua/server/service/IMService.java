package com.tanhua.server.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.mapper.AnnouncementMapper;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.Announcement;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.common.vo.ErrorResult;
import com.tanhua.dubbo.server.api.HuanXinApi;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.api.UsersApi;
import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.pojo.HuanXinUser;
import com.tanhua.dubbo.server.pojo.Users;
import com.tanhua.server.vo.MessageCommentVo;
import com.tanhua.server.vo.PageResp;
import com.tanhua.server.vo.UserInfoVo;
import com.tanhua.server.vo.UsersVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class IMService {
    @Reference
    private HuanXinApi huanXinApi;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Reference
    private UsersApi usersApi;
    @Reference
    private QuanZiApi quanZiApi;
    @Autowired
    private AnnouncementMapper announcementMapper;
    @Autowired
    private MyCenterService myCenterService;

    /**
     * 根据环信用户名查询用户信息
     * 调用dubbo服务查询环信用户信息
     * 根据环信用户信息中的用户id查询数据库(mysql)获取用户信息
     * 封装结果对象
     *
     * @param userName 环信用户名(HX_1)
     * @return
     */
    public UserInfoVo queryUserInfoByUserName(String userName) {
        //调用dubbo服务查询环信用户信息
        HuanXinUser huanXinUser = huanXinApi.queryUserByUserName(userName);
        if (ObjectUtil.isEmpty(huanXinUser)) {
            throw new BusinessException(ErrorResult.errorHuanxinQueryFail());
        }
        //根据环信用户信息中的用户id查询数据库(mysql)获取用户信息
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper();
        wrapper.eq(UserInfo::getUserId, huanXinUser.getUserId());
        UserInfo userInfo = userInfoMapper.selectOne(wrapper);
        if (ObjectUtil.isEmpty(userInfo)) {
            throw new BusinessException(ErrorResult.errorHuanxinQueryFail());
        }
        //封装结果对象
        UserInfoVo userInfoVo = BeanUtil.copyProperties(userInfo, UserInfoVo.class, "marriage");
        //性别与marriage属性单独处理
        userInfoVo.setGender(userInfo.getSex().getValue() == 1 ? "man" : "woman");
        userInfoVo.setMarriage(StrUtil.equals(userInfo.getMarriage(), "未婚") ? 0 : 1);
        return userInfoVo;
    }

    /**
     * 添加好友
     * 获取当前登录用户
     * 保存好友关系(mysql)
     * 注册环信好友关系
     *
     * @param friendId 好友id
     */
    public void contactUser(Long friendId) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //判断当前用户是否在陌生人黑名单中
        Boolean resultBlack = myCenterService.isBlackUser(friendId, user.getId());
        if (resultBlack) {
            throw new BusinessException(ErrorResult.blackListError());
        }
        //保存好友关系(MongoDB)
        String id = usersApi.saveUsers(user.getId(), Long.valueOf(friendId));
        //数据库保存失败则抛出异常
        if (StrUtil.isEmpty(id)) {
            throw new BusinessException(ErrorResult.addFriendError());
        }
        //注册环信好友关系
        Boolean result = huanXinApi.addUserFriend(user.getId(), friendId);
        if (!result) {
            //throw new BusinessException(ErrorResult.addHuanXinFriendError());
        }
    }

    /**
     * 查询联系人列表
     * 关键字为空则进行分页查询
     * 关键字不为空则查询所有,在查询MySql时根据关键字对昵称筛选
     * 查询mysql补充属性
     * 封装结果对象
     *
     * @param page
     * @param pageSize
     * @param keyword  关键字
     * @return
     */
    public PageResp queryContactsList(Integer page, Integer pageSize, String keyword) {
        PageResp pageResp = new PageResp();
        pageResp.setPage(page);
        pageResp.setPagesize(pageSize);
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //判断关键字是否为空
        List<Users> usersList = null;
        if (StringUtils.isEmpty(keyword)) {
            //无关键字则进行分页查询
            usersList = usersApi.queryUsersList(user.getId(), page, pageSize);
        } else {
            //有关键字则查询所有
            usersList = usersApi.queryAllUsersList(user.getId());
        }
        if (CollUtil.isEmpty(usersList)) {
            return pageResp;
        }
        //将好友id组成集合
        List<Object> friendIds = CollUtil.getFieldValues(usersList, "friendId");
        //根据好友id集合查找用户信息(mysql)
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(UserInfo::getUserId, friendIds);
        //有关键字则将其作为条件
        if (StringUtils.isNotEmpty(keyword)) {
            wrapper.like(UserInfo::getNickName, keyword);
        }
        List<UserInfo> userInfoList = userInfoMapper.selectList(wrapper);
        //封装结果对象
        List<UsersVo> usersVoList = new ArrayList<>();
        for (UserInfo userInfo : userInfoList) {
            UsersVo usersVo = new UsersVo();
            usersVo.setId(userInfo.getUserId());
            //环信id
            usersVo.setUserId("HX_" + userInfo.getUserId());
            usersVo.setAvatar(userInfo.getLogo());
            usersVo.setNickname(userInfo.getNickName());
            usersVo.setGender(userInfo.getSex().getValue() == 1 ? "man" : "woman");
            usersVo.setAge(userInfo.getAge());
            usersVo.setCity(StringUtils.substringBefore(userInfo.getCity(), "-"));
            usersVoList.add(usersVo);
        }
        pageResp.setItems(usersVoList);
        return pageResp;
    }

    /**
     * 查询消息点赞/评论/喜欢列表
     * 查询comment表获取数据
     * 根据comment表中的userId属性在mysql中查询用户信息
     * 封装结果对象
     *
     * @param page
     * @param pageSize
     * @param commentType 操作类型
     * @return
     */
    public PageResp queryCommentList(Integer page, Integer pageSize, CommentType commentType) {
        //获取当前登录对象
        User user = UserThreadLocal.get();
        //查询comment表
        List<Comment> commentList = quanZiApi.queryCommentListByUser(user.getId(), page, pageSize, commentType);
        //将userId属性封装为集合
        List<Object> userIds = CollUtil.getFieldValues(commentList, "userId");
        //在mysql中查询用户数据
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(UserInfo::getUserId, userIds);
        List<UserInfo> userInfoList = userInfoMapper.selectList(wrapper);
        //封装结果对象
        List<MessageCommentVo> messageCommentVoList = new ArrayList<>();
        for (Comment comment : commentList) {
            for (UserInfo userInfo : userInfoList) {
                if (ObjectUtil.equal(comment.getUserId(), userInfo.getUserId())) {
                    MessageCommentVo messageCommentVo = new MessageCommentVo();
                    messageCommentVo.setId(userInfo.getUserId().toString());
                    messageCommentVo.setAvatar(userInfo.getLogo());
                    messageCommentVo.setNickname(userInfo.getNickName());
                    messageCommentVo.setCreateDate(DateUtil.format(new Date(comment.getCreated()), "yyyy-MM-dd HH:mm"));
                    messageCommentVoList.add(messageCommentVo);
                    break;
                }
            }
        }
        PageResp resp = new PageResp();
        resp.setPage(page);
        resp.setPagesize(pageSize);
        resp.setItems(messageCommentVoList);
        return resp;
    }

    /**
     * 查询系统公告
     *
     * @param page
     * @param pageSize
     * @return
     */
    public PageResp queryAnnouncementsList(Integer page, Integer pageSize) {
        //查询mysql
        List<Announcement> announcementList = announcementMapper.selectList(null);
        System.out.println(announcementList);
        //封装结果对象
        PageResp pageResp = new PageResp();
        pageResp.setPage(page);
        pageResp.setPagesize(pageSize);
        pageResp.setItems(announcementList);
        return pageResp;
    }
}
