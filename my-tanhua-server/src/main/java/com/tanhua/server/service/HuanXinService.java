package com.tanhua.server.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.common.pojo.User;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.dubbo.server.api.HuanXinApi;
import com.tanhua.dubbo.server.pojo.HuanXinUser;
import com.tanhua.server.vo.HuanXinUserVo;
import org.springframework.stereotype.Service;

@Service
public class HuanXinService {
    @Reference
    private HuanXinApi huanXinApi;

    /**
     * 查询环信用户信息
     * 调用dubbo服务查询当前用户的环信信息
     * 封装对象
     *
     * @return
     */
    public HuanXinUserVo queryHuanXinUser() {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //调用dubbo服务查询当前用户的环信信息
        HuanXinUser huanXinUser = huanXinApi.queryHuanXinUser(user.getId());
        //封装对象
        HuanXinUserVo huanXinUserVo = new HuanXinUserVo();
        huanXinUserVo.setUsername(huanXinUser.getUsername());
        huanXinUserVo.setPassword(huanXinUser.getPassword());
        return huanXinUserVo;
    }
}
