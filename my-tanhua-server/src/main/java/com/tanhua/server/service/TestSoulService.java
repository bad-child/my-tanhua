package com.tanhua.server.service;


import cn.hutool.core.convert.Convert;
import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.dubbo.server.api.TestSoulApi;
import com.tanhua.dubbo.server.pojo.Answers;
import com.tanhua.dubbo.server.pojo.QuestionnaireReport;
import com.tanhua.dubbo.server.pojo.QuestionnaireResult;
import com.tanhua.dubbo.server.vo.QuestionnaireVo;
import com.tanhua.dubbo.server.vo.ResultVo;
import com.tanhua.dubbo.server.vo.SimilarYouVo;
import com.tanhua.dubbo.server.vo.DimensionsVo;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TestSoulService {

    @Reference
    private TestSoulApi testSoulApi;
    @Autowired
    private UserInfoService userInfoService;


    public List<QuestionnaireVo> queryQuestionnaire() {
        User user = UserThreadLocal.get();
        //要返回的vo对象
        List<QuestionnaireVo> list = testSoulApi.queryQuestinnaireVoList(user.getId());
        return list;
    }

    public String testSoul(List<Answers> answersList) {
            Integer score=0;
            String questionnaireId="null";
        for (Answers answers : answersList) {
            String questionId = answers.getQuestionId();
            String optionId = answers.getOptionId();
            score+=testSoulApi.queryScore(optionId);
            questionnaireId=testSoulApi.queryQuestinnaireId(questionId);
        }
        List<String> list=testSoulApi.queryScope();
        QuestionnaireReport questionnaireReport=new QuestionnaireReport();
        for (String scope : list) {
            String[] split = scope.split(",");
            if (Convert.toInt(split[0])<=score  && score<=Convert.toInt(split[1])){
                QuestionnaireResult result=testSoulApi.queryResult(scope,questionnaireId);
                questionnaireReport.setId(new ObjectId());
                questionnaireReport.setCover(result.getCover());
                questionnaireReport.setResultId(Convert.toStr(result.getId()));
                questionnaireReport.setUserId(Convert.toStr(UserThreadLocal.get().getId()));
                testSoulApi.saveResult(questionnaireReport);
                break;
            }
        }

        testSoulApi.saveNewReport(questionnaireId,questionnaireReport.getId());
        return questionnaireReport.getId().toHexString();
    }

    public ResultVo testResult(String id) {
        QuestionnaireReport questionnaireReport=testSoulApi.queryReport(id);
        ResultVo resultVo=new ResultVo();
        resultVo.setCover(questionnaireReport.getCover());
        String resultId = questionnaireReport.getResultId();
        QuestionnaireResult result = testSoulApi.queryQuestionsResult(resultId);
        resultVo.setConclusion(result.getContent());
        List<DimensionsVo> dimensionsVoList=new ArrayList<>();
       dimensionsVoList.add(new DimensionsVo("外向","90%"));
       dimensionsVoList.add(new DimensionsVo("判断","60%"));
       dimensionsVoList.add(new DimensionsVo("抽象","80%"));
       dimensionsVoList.add(new DimensionsVo("理性","70%"));
        Set<SimilarYouVo> likeYouList=new HashSet<>();
        List<QuestionnaireReport> questionnaireReportList=testSoulApi.findAllReport();
        for (QuestionnaireReport report : questionnaireReportList) {
                QuestionnaireReport questionnaireReport1 = testSoulApi.queryReport(id);
                if(report.getUserId().equals(UserThreadLocal.get().getId().toString())){
                    continue;
                }
                if (questionnaireReport1.getResultId().equals(report.getResultId())){
                    SimilarYouVo youVo=new SimilarYouVo();
                    UserInfo userInfo = userInfoService.queryById(Convert.toLong(report.getUserId()));
                    youVo.setAvatar(userInfo.getLogo());
                    youVo.setId(Convert.toInt(userInfo.getUserId()));
                    likeYouList.add(youVo);
                }
            }
        resultVo.setSimilarYou(likeYouList);
        resultVo.setDimensions(dimensionsVoList);
        return resultVo;
    }
}




