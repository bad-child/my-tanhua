package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.common.enums.SexEnum;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.common.vo.ErrorResult;
import com.tanhua.dubbo.server.api.HuanXinApi;
import com.tanhua.dubbo.server.api.UserLikeApi;
import com.tanhua.dubbo.server.api.UserLocationApi;
import com.tanhua.dubbo.server.api.VisitorsApi;
import com.tanhua.dubbo.server.enums.HuanXinMessageType;
import com.tanhua.dubbo.server.pojo.RecommendUser;
import com.tanhua.dubbo.server.vo.UserLocationVo;
import com.tanhua.server.vo.NearUserVo;
import com.tanhua.server.vo.TodayBest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TanHuaService {
    @Value("${tanhua.default.recommend.users}")
    private String defaultRecommendUsers;
    @Autowired
    private RecommendUserService recommendUserService;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private QuestionService questionService;
    @Reference
    private HuanXinApi huanXinApi;
    @Reference
    private VisitorsApi visitorsApi;
    @Autowired
    private MyCenterService myCenterService;
    @Reference
    private UserLocationApi userLocationApi;
    @Reference
    private UserLikeApi userLikeApi;
    @Autowired
    private IMService imService;

    /**
     * 查询个人主页的个人信息
     * 根据目标用户id与登陆者用户id查询缘分值
     * 获取目标用户的用户信息
     * 记录来访用户
     * 封装结果对象
     *
     * @param userId 个人主页的用户id
     * @return
     */
    public TodayBest queryUserInfo(Long userId) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //根据目标用户id与登陆者用户id查询缘分值
        Double score = recommendUserService.queryScore(userId, user.getId());
        //获取目标用户的用户信息
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserInfo::getUserId, userId);
        UserInfo userInfo = userInfoMapper.selectOne(wrapper);
        //封装结果对象
        TodayBest todayBest = new TodayBest();
        todayBest.setId(Convert.toInt(userId));
        todayBest.setFateValue(Convert.toInt(score));
        todayBest.setGender(userInfo.getSex().getValue() == 1 ? "man" : "woman");
        todayBest.setNickname(userInfo.getNickName());
        todayBest.setAge(userInfo.getAge());
        todayBest.setTags(userInfo.getTags().split(","));
        todayBest.setAvatar(userInfo.getLogo());
        //记录来访用户
        visitorsApi.saveVisitor(userId, user.getId(), "个人主页");
        return todayBest;
    }

    /**
     * 查询陌生人问题
     *
     * @param userId 目标用户id
     * @return
     */
    public String queryQuestion(Long userId) {
        String question = questionService.queryQuestion(userId);
        return question;
    }

    /**
     * 回复陌生人问题
     *
     * @param userId 陌生人id
     * @param reply  答案
     */
    public void replyQuestion(Long userId, String reply) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //判断当前用户是否在陌生人黑名单中
        Boolean resultBlack = myCenterService.isBlackUser(userId, user.getId());
        if (resultBlack) {
            throw new BusinessException(ErrorResult.blackListError());
        }
        //获取登录用户的用户信息
        LambdaQueryWrapper<UserInfo> wrapper1 = new LambdaQueryWrapper<>();
        wrapper1.eq(UserInfo::getUserId, user.getId());
        UserInfo userInfo = userInfoMapper.selectOne(wrapper1);
        //构建消息内容
        Map<String, Object> msg = new HashMap<>();
        msg.put("userId", user.getId());
        msg.put("huanXinId", "HX_" + user.getId());
        msg.put("nickname", userInfo.getNickName());
        msg.put("strangerQuestion", queryQuestion(userId));
        msg.put("reply", reply);
        //发送环信消息
        Boolean result = huanXinApi.sendMsgFromAdmin("HX_" + userId, HuanXinMessageType.TXT, JSONUtil.toJsonStr(msg));
        if (!result) {
            throw new BusinessException(ErrorResult.errorHuanxinReplyFail());
        }
    }

    /**
     * 搜附近
     *
     * @param gender   性别(man,woman)
     * @param distance 距离
     * @return
     */
    public List<NearUserVo> queryNearUse(String gender, String distance) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //查询当前登录用户的位置
        UserLocationVo userLocationVo = userLocationApi.queryByUserId(user.getId());
        //未查询到当前登录用户的位置则返回空集合
        if (ObjectUtil.isEmpty(userLocationVo)) {
            return ListUtil.empty();
        }
        //搜附近
        List<UserLocationVo> userLocationVoList = userLocationApi.queryUserFromLocation(userLocationVo.getLongitude(), userLocationVo.getLatitude(), Convert.toDouble(distance));
        //未查询到附近用户则返回空集合
        if (ObjectUtil.isEmpty(userLocationVoList)) {
            return ListUtil.empty();
        }
        //根据条件查询数据库
        List<Object> userIds = CollUtil.getFieldValues(userLocationVoList, "userId");
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(UserInfo::getUserId, userIds);
        wrapper.eq(UserInfo::getSex, StrUtil.equals(gender, "man") ? SexEnum.MAN : SexEnum.WOMAN);
        List<UserInfo> userInfoList = userInfoMapper.selectList(wrapper);
        //封装结果对象
        List<NearUserVo> nearUserVoList = new ArrayList<>();
        for (UserLocationVo locationVo : userLocationVoList) {
            //排除登录用户自身
            if (ObjectUtil.equal(locationVo.getUserId(), user.getId())) {
                continue;
            }
            for (UserInfo userInfo : userInfoList) {
                if (ObjectUtil.equal(locationVo.getUserId(), userInfo.getUserId())) {
                    NearUserVo nearUserVo = new NearUserVo();
                    nearUserVo.setUserId(locationVo.getUserId());
                    nearUserVo.setAvatar(userInfo.getLogo());
                    nearUserVo.setNickname(userInfo.getNickName());
                    nearUserVoList.add(nearUserVo);
                    break;
                }
            }
        }
        return nearUserVoList;
    }

    /**
     * 探花-查询推荐列表
     *
     * @return
     */
    public List<TodayBest> queryCardsList() {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //获取推荐列表
        List<RecommendUser> recommendUserList = recommendUserService.queryCardsList(user.getId());
        if (CollUtil.isEmpty(recommendUserList)) {
            recommendUserList = new ArrayList<>();
            //默认推荐列表
            List<String> defaultIds = StrUtil.split(defaultRecommendUsers, ",");
            for (String defaultId : defaultIds) {
                RecommendUser recommendUser = new RecommendUser();
                recommendUser.setToUserId(user.getId());
                recommendUser.setUserId(Convert.toLong(defaultId));
                recommendUserList.add(recommendUser);
            }
        }
        //计算展示的数量,默认10个
        int showCount = Math.min(10, recommendUserList.size());
        //有序不重复set集合
        Set<RecommendUser> recommendUserSet = new HashSet<>();
        //随机10个推荐用户
        while (recommendUserSet.size() < 10) {
            int index = RandomUtil.randomInt(0, recommendUserList.size());
            recommendUserSet.add(recommendUserList.get(index));
        }
        //查询推荐用户的信息
        List<Object> userIds = CollUtil.getFieldValues(recommendUserSet, "userId");

        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(UserInfo::getUserId, userIds);
        List<UserInfo> userInfoList = userInfoMapper.selectList(wrapper);
        //封装结果对象
        List<TodayBest> todayBestList = new ArrayList<>();
        for (UserInfo userInfo : userInfoList) {
            TodayBest todayBest = new TodayBest();
            todayBest.setId(userInfo.getUserId().intValue());
            todayBest.setGender(userInfo.getSex().getValue() == 1 ? "man" : "woman");
            todayBest.setNickname(userInfo.getNickName());
            todayBest.setAge(userInfo.getAge());
            todayBest.setTags(userInfo.getTags().split(","));
            todayBest.setAvatar(userInfo.getLogo());
            todayBestList.add(todayBest);
        }
        return todayBestList;
    }

    /**
     * 探花-喜欢
     *
     * @param likeUserId 被喜欢用户id
     */
    public void likeUser(Long likeUserId) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //执行喜欢操作
        Boolean result = userLikeApi.likeUser(user.getId(), likeUserId);
        if (!result) {
            throw new BusinessException(ErrorResult.likeUserError());
        }
        //判断是否相互喜欢
        if (userLikeApi.isMutualLike(user.getId(), likeUserId)) {
            //添加好友
            imService.contactUser(likeUserId);
        }
    }

    /**
     * 探花-不喜欢
     *
     * @param likeUserId 被不喜欢用户id
     */
    public void notLikeUser(Long likeUserId) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //执行不喜欢操作
        Boolean result = userLikeApi.notLikeUser(user.getId(), likeUserId);
        if (!result) {
            throw new BusinessException(ErrorResult.notLikeUserError());
        }
    }
}
