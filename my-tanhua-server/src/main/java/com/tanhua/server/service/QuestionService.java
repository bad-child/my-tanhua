package com.tanhua.server.service;

import cn.hutool.core.util.ObjectUtil;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.common.mapper.QuestionMapper;
import com.tanhua.common.pojo.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class QuestionService {
    @Autowired
    private QuestionMapper questionMapper;

    public String queryQuestion(Long userId) {
        LambdaQueryWrapper<Question> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Question::getUserId, userId);
        Question question = questionMapper.selectOne(wrapper);
        if (ObjectUtil.isNotEmpty(question)) {
            return question.getTxt();
        }
        return "你的爱好是什么?";
    }
}
