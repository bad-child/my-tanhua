package com.tanhua.server.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.db.PageResult;
import cn.hutool.db.sql.Query;
import cn.hutool.http.Method;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tanhua.common.enums.SexEnum;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.mapper.*;
import com.tanhua.common.pojo.*;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.common.vo.ErrorResult;
import com.tanhua.dubbo.server.api.*;
import com.tanhua.dubbo.server.pojo.UserLike;
import com.tanhua.dubbo.server.pojo.Visitors;
import com.tanhua.oss.OssTemplate;
import com.tanhua.server.enums.LoveType;
import com.tanhua.server.vo.*;
import com.tanhua.sms.SmsTemplate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Duration;
import java.util.*;

@Service
public class MyCenterService {
    @Reference
    private HuanXinApi huanXinApi;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private BlackListMapper blackListMapper;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private SmsTemplate smsTemplate;
    @Autowired
    private UserMapper userMapper;
    @Reference
    private UserLikeApi userLikeApi;
    @Reference
    private VideoApi videoApi;
    @Autowired
    private QuestionMapper questionMapper;
    @Reference
    private UsersApi usersApi;
    @Reference
    private VisitorsApi visitorsApi;
    @Reference
    private RecommendApi recommendApi;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private SettingsMapper settingsMapper;
    @Autowired
    @Qualifier("face")
    private FaceEngineService faceEngineService;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private OssTemplate ossTemplate;

    private static final String[] IMAGE_TYPE = new String[]{
            ".bmp",
            ".jpg",
            ".jpeg",
            ".gif",
            ".png",
    };


    /**
     * 根据用户id查询用户信息
     * 判断用户id是否为空,为空则将当前登录者id作为用户id
     * 根据用户id查询环信用户信息
     * 根据用户id查询用户信息
     * 封装结果对象
     *
     * @param userId 用户id,为空则表示查询当前登录者的信息
     * @return
     */
    public UserInfoVo queryUserInfoByUserId(Long userId) {
        //判断用户id是否为空
        if (ObjectUtil.isEmpty(userId)) {
            //空则将当前登录用户id作为用户id
            userId = UserThreadLocal.get().getId();
        }
        //根据用户id查询环信用户信息
        //HuanXinUser huanXinUser = huanXinApi.queryHuanXinUser(userId);
        //根据用户id查询用户信息
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserInfo::getUserId, userId);
        UserInfo userInfo = userInfoMapper.selectOne(wrapper);
        //封装对象
        UserInfoVo userInfoVo = BeanUtil.copyProperties(userInfo, UserInfoVo.class, "marriage");
        //性别与marriage属性单独处理
        userInfoVo.setGender(userInfo.getSex().getValue() == 1 ? "man" : "woman");
        userInfoVo.setMarriage(StrUtil.equals(userInfo.getMarriage(), "未婚") ? 0 : 1);
        return userInfoVo;
    }

    /**
     * 添加黑名单
     *
     * @param blackId 被拉黑用户id
     * @return
     */
    public Boolean saveBlackList(Long blackId) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //判断用户id与被拉黑用户id是否都不为空
        if (!ObjectUtil.isAllNotEmpty(user.getId(), blackId)) {
            return null;
        }
        //判断黑名单是否已存在
        LambdaQueryWrapper<BlackList> wrapper = new LambdaQueryWrapper<>();
        //添加条件->userId=当前登录用户id
        wrapper.eq(BlackList::getUserId, user.getId());
        //添加条件->blackUserId=被拉黑用户id
        wrapper.eq(BlackList::getBlackUserId, blackId);
        //查询符合条件的记录数
        long count = blackListMapper.selectCount(wrapper);
        //记录数>0则添加失败
        if (count > 0) {
            return false;
        }
        //注册用户黑名单的关系
        BlackList black = new BlackList();
        //设置userId为当前登录用户id
        black.setUserId(user.getId());
        //设置blackUserId为被拉黑用户id
        black.setBlackUserId(blackId);
        blackListMapper.insert(black);
        //在环信中添加黑名单
        Boolean result = huanXinApi.addUserBlacklist(user.getId(), blackId);
        //添加环信黑名单失败
        if (!result) {
            throw new BusinessException(ErrorResult.addHuanxinBlackListError());
        }
        return true;
    }

    /**
     * 移除黑名单
     *
     * @param blackId 被拉黑用户id
     * @return
     */
    public Boolean removeBlackList(Long blackId) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //判断用户id与被拉黑用户id是否都不为空
        if (!ObjectUtil.isAllNotEmpty(user.getId(), blackId)) {
            return null;
        }
        //移除黑名单
        LambdaQueryWrapper<BlackList> wrapper = new LambdaQueryWrapper<>();
        //添加条件->userId=当前登录用户id
        wrapper.eq(BlackList::getUserId, user.getId());
        //添加条件->blackUserId=被拉黑用户id
        wrapper.eq(BlackList::getBlackUserId, blackId);
        //移除符合条件的记录并返回影响记录数
        long count = blackListMapper.delete(wrapper);
        //记录数=0则被拉黑用户原本不在黑名单中
        if (count == 0) {
            return false;
        }
        //在环信中移除黑名单
        Boolean result = huanXinApi.removeBlacklist(user.getId(), blackId);
        //移除环信黑名单失败
        if (!result) {
            throw new BusinessException(ErrorResult.removeHuanxinBlackListError());
        }
        return true;
    }

    /**
     * 判断被查询用户是否在陌生人黑名单中
     *
     * @param userId      陌生人
     * @param blackUserId 被查询用户
     * @return true 为在黑名单中
     */
    public Boolean isBlackUser(Long userId, Long blackUserId) {
        LambdaQueryWrapper<BlackList> wrapper = new LambdaQueryWrapper<>();
        //添加条件->userId=陌生人id
        wrapper.eq(BlackList::getUserId, userId);
        //添加条件->blackUserId=被查询用户id
        wrapper.eq(BlackList::getBlackUserId, blackUserId);
        //查询符合条件的记录数
        Integer count = blackListMapper.selectCount(wrapper);
        //记录数大于0则用户在陌生人黑名单中
        return count > 0;
    }

    /**
     * 查询黑名单列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    public PageResp queryBlackList(Integer page, Integer pageSize) {
        PageResp resp = new PageResp();
        resp.setPage(page);
        resp.setPagesize(pageSize);
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //查询黑名单列表
        LambdaQueryWrapper<BlackList> wrapper = new LambdaQueryWrapper<>();
        //添加条件->userId=当前登录用户id
        wrapper.eq(BlackList::getUserId, user.getId());
        //查询黑名单列表
        List<BlackList> blackLists = blackListMapper.selectList(wrapper);
        //黑名单列表为空则返回空集合
        if (CollUtil.isEmpty(blackLists)) {
            resp.setItems(ListUtil.empty());
            return resp;
        }
        //根据黑名单用户id查询用户信息
        List<Object> userIds = CollUtil.getFieldValues(blackLists, "blackUserId");
        LambdaQueryWrapper<UserInfo> wrapper1 = new LambdaQueryWrapper<>();
        //添加条件->userId在黑名单用户id集合中
        wrapper1.in(UserInfo::getUserId, userIds);
        //查询黑名单用户集合的用户信息
        List<UserInfo> userInfoList = userInfoMapper.selectList(wrapper1);
        //封装结果对象
        List<BlacklistVo> blacklistVoList = new ArrayList<>();
        for (BlackList blackList : blackLists) {
            for (UserInfo userInfo : userInfoList) {
                //userId相同则为同一用户
                if (ObjectUtil.equal(blackList.getBlackUserId(), userInfo.getUserId())) {
                    BlacklistVo blacklistVo = new BlacklistVo();
                    blacklistVo.setId(blackList.getBlackUserId());
                    blacklistVo.setAvatar(userInfo.getLogo());
                    blacklistVo.setNickname(userInfo.getNickName());
                    blacklistVo.setGender(userInfo.getSex().getValue() == 1 ? "man" : "woman");
                    blacklistVo.setAge(userInfo.getAge());
                    blacklistVoList.add(blacklistVo);
                    break;
                }
            }
        }
        resp.setItems(blacklistVoList);
        return resp;
    }

    /**
     * 修改手机号-发送验证码
     */
    public void changePhoneSendVerificationCode() {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //发送验证码
        //redisKey
        String redisKey = "PHONE_CHECK_CODE_" + user.getMobile();
        //判断redis中是否有当前key的存在
        if (Boolean.TRUE.equals(redisTemplate.hasKey(redisKey))) {
            //有则抛出异常
            throw new BusinessException(ErrorResult.redisKeyError());
        }
        String code = "123456";
        //随机生成6位数字验证码
        //String code = RandomUtils.nextInt(100000, 999999) + "";
        //生成验证码并发送给该手机
        //code = smsTemplate.sendSms(phone, code);
        //验证码为null则抛出异常
        if (code == null) {
            throw new BusinessException(ErrorResult.fail());
        }
        //存入redis                                       设置有效时间
        redisTemplate.opsForValue().set(redisKey, code, Duration.ofMinutes(1));
    }

    /**
     * 修改手机号-验证验证码
     *
     * @param code 验证码
     */
    public Map<String, Object> checkPhoneSendVerificationCode(String code) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //redisKey
        String redisKey = "PHONE_CHECK_CODE_" + user.getMobile();
        //从redis获取验证码
        String redisCode = redisTemplate.opsForValue().get(redisKey);
        //将redis中的验证码清除
        redisTemplate.delete(redisKey);
        //验证码为null则抛出异常
        if (redisCode == null) {
            throw new BusinessException(ErrorResult.loginError());
        }
        //将结果封装为map对象返回
        Map<String, Object> map = new HashMap<>();
        map.put("verification", code.equals(redisCode));
        return map;
    }

    /**
     * 修改手机号-保存新手机号
     *
     * @param phone 新手机号
     */
    public void saveNewPhone(String phone) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        String mobile = user.getMobile();

        if (StrUtil.equals(mobile, phone)) {
            throw new BusinessException(ErrorResult.updateMobileFile());
        }
        //保存新手机号
        user.setMobile(phone);
        //更新数据库中的记录
        userMapper.updateById(user);
    }

    /**
     * 修改用户信息
     *
     * @param userInfoVo
     */
    public void saveMessage(UserInfoVo userInfoVo) {
        //获取用户的对象
        User user = UserThreadLocal.get();
        UserInfo userInfo = new UserInfo();
        //获取属性
        userInfo.setNickName(userInfoVo.getNickname());
        userInfo.setBirthday(userInfoVo.getBirthday());
        userInfo.setSex(StrUtil.equals(userInfoVo.getGender(), "man") ? SexEnum.MAN : SexEnum.WOMAN);
        userInfo.setCity(userInfoVo.getCity());
        userInfo.setEdu(userInfoVo.getEducation());
        userInfo.setIncome(userInfoVo.getIncome());
        userInfo.setIndustry(userInfoVo.getProfession());
        userInfo.setMarriage(1 == userInfoVo.getMarriage() ? "已婚" : "未婚");
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserInfo::getUserId, user.getId());
        //判断是否更新成功
        int i = userInfoMapper.update(userInfo, wrapper);
        if (i == 0) {
            throw new BusinessException(ErrorResult.updateFile());
        }
    }

    /**
     * 取消喜欢
     *
     * @param userId
     */
    public void cancelLike(Long userId) {
        User user = UserThreadLocal.get();
        userLikeApi.cancelLike(user.getId(), userId);
    }


    /**
     * 回关粉丝
     *
     * @param userId
     */
    public void seeLike(Long userId) {
        User user = UserThreadLocal.get();
        //videoApi.followUser(user.getId(), userId);
        userLikeApi.likeUser(user.getId(), userId);
    }

    /**
     * 设置问题保存
     *
     * @param content
     */
    public void saveQuestions(String content) {
        //解析token
        User user = UserThreadLocal.get();
        Question question = new Question();
        question.setUserId(user.getId());
        question.setTxt(content);
        LambdaQueryWrapper<Question> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Question::getUserId, user.getId());
        questionMapper.update(question, wrapper);
    }


    /**
     * 保存通知
     *
     * @param likeNotification    喜欢
     * @param pinglunNotification 评论
     * @param gonggaoNotification 公告
     */
    public void notice(Boolean likeNotification, Boolean pinglunNotification, Boolean gonggaoNotification) {

        User user = UserThreadLocal.get();

        LambdaQueryWrapper<Settings> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Settings::getUserId, user.getId());
        Settings settings = settingsMapper.selectOne(wrapper);

        settings.setLikeNotification(likeNotification);
        settings.setPinglunNotification(pinglunNotification);
        settings.setGonggaoNotification(gonggaoNotification);

        settingsMapper.update(settings, wrapper);

    }

    /**
     * 读取用户设置
     *
     * @return
     */
    public SettingsVo queryUserSetting() {
        User user = UserThreadLocal.get();

        LambdaQueryWrapper<Settings> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Settings::getUserId, user.getId());

        Settings settings = settingsMapper.selectOne(wrapper);

        SettingsVo settingsVo = new SettingsVo();

        // 封装
        settingsVo.setId(Convert.toInt(user.getId()));
        settingsVo.setPhone(user.getMobile());

        if (ObjectUtil.isEmpty(settings)) {
            settings = new Settings();
            settings.setUserId(user.getId());
            settings.setLikeNotification(false);
            settings.setGonggaoNotification(false);
            settings.setPinglunNotification(false);

            settingsMapper.insert(settings);
        }

        settingsVo.setStrangerQuestion(questionService.queryQuestion(user.getId()));

        settingsVo.setLikeNotification(settings.isLikeNotification());
        settingsVo.setGonggaoNotification(settings.isGonggaoNotification());
        settingsVo.setPinglunNotification(settings.isPinglunNotification());


        return settingsVo;
    }

    //互相喜欢、喜欢、粉丝、谁看过我
    public LoveCountVo queryLoveCount() {
        //解析token，获取登录用户
        User user = UserThreadLocal.get();
        //获取获取互相喜欢数
        Integer likeEachOthersCount = userLikeApi.queryLikeEachOtherCount(user.getId());
        //获取喜欢数
        Integer likedCount = userLikeApi.queryLikeCount2(user.getId());
        //获取粉丝数
        Integer fansCount = userLikeApi.queryBeLikedCount(user.getId());
        //封装互相喜欢、喜欢、粉丝、谁看过我LoveCountVo对象
        LoveCountVo loveCountVo = new LoveCountVo();
        loveCountVo.setEachLoveCount(likeEachOthersCount);
        loveCountVo.setLoveCount(likedCount);
        loveCountVo.setFanCount(fansCount);
        return loveCountVo;
    }

    //互相喜欢、喜欢、粉丝、谁看过我 - 翻页列表
    public PageResp queryLoveCountList(LoveType type, Integer page, Integer pageSize, String nickName) {

        //解析token，获取登录用户
        User user = UserThreadLocal.get();
        List<LoveVo> loveVoList = new ArrayList<>();
        PageResp pageResp = new PageResp();

        switch (type) {
            case FOCUS_ON_EACH_OTHER: {
                //查询互相喜欢列表
                List<Long> likeEachOtherIdsList = userLikeApi.querylikeEachOtherIdsList(user.getId());
                loveVoList = backLoveVo(likeEachOtherIdsList, user, loveVoList, nickName, page, pageSize);
                break;
            }
            case MY_FOCUS: {
                //查询喜欢列表
                List<UserLike> likeList = userLikeApi.queryLikeList2(user.getId());
                List<Long> listListNew = new ArrayList<>();
                for (UserLike userLike : likeList) {
                    listListNew.add(userLike.getLikeUserId());
                }
                loveVoList = backLoveVo(listListNew, user, loveVoList, nickName, page, pageSize);
                break;
            }
            case FANS: {
                //查询粉丝列表
                List<Long> fansList = userLikeApi.queryFansList(user.getId());
                loveVoList = backLoveVo(fansList, user, loveVoList, nickName, page, pageSize);
                break;
            }
            case WHO_LOOK_ME: {
                //查询谁看过我列表
                List<Visitors> visitorsList = visitorsApi.queryMyVisitor(user.getId());
                List<Long> listListNew = new ArrayList<>();
                for (Visitors visitors : visitorsList) {
                    listListNew.add(visitors.getVisitorUserId());
                }
                loveVoList = backLoveVo(listListNew, user, loveVoList, nickName, page, pageSize);
                break;
            }
            default: {
                return null;
            }
        }

        pageResp.setPage(page);
        pageResp.setPagesize(pageSize);
        pageResp.setItems(loveVoList);
        return pageResp;
    }

    //抽取一个封装LoveVo方法
    public List<LoveVo> backLoveVo(List<Long> list, User user, List<LoveVo> loveVoList, String nickName, Integer page, Integer pageSize) {

        //模糊查询
        if (StrUtil.isNotEmpty(nickName)) {
            //遍历visitorsList获取每个用户的userId
            for (Long likeEachOtherId : list) {
                //根据条件查询每个用户的userInfo
                QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
                wrapper.eq("user_id", likeEachOtherId);
                wrapper.like("nick_name", nickName);
                UserInfo userInfo = userInfoMapper.selectOne(wrapper);
                if (ObjectUtil.isNotEmpty(userInfo)) {
                    //封装LoveVo
                    LoveVo loveVo = BeanUtil.copyProperties(userInfo, LoveVo.class, "id", "marriage");
                    loveVo.setGender(userInfo.getSex().name().toLowerCase());
                    Double score = recommendApi.queryScore(user.getId(), userInfo.getUserId());
                    loveVo.setMarriage(userInfo.getMarriage().equals("已婚") ? 1 : 0);
                    loveVo.setMatchRate(Convert.toInt(score));
                    Boolean likeUser = userLikeApi.likeUser2(user.getId(), userInfo.getUserId());
                    loveVo.setAlreadyLove(likeUser);
                    loveVoList.add(loveVo);
                }
            }
            if (ObjectUtil.isNotEmpty(loveVoList)) {
                return loveVoList;
            }
        }

        if (list.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        List<UserInfo> newList = userInfoService.queryPage(page, pageSize, list);
        //遍历visitorsList获取每个用户的userId
        for (UserInfo userInfo : newList) {
            //封装LoveVo
            LoveVo loveVo = BeanUtil.copyProperties(userInfo, LoveVo.class, "id", "marriage");
            loveVo.setGender(userInfo.getSex().name().toLowerCase());
            Double score = recommendApi.queryScore(user.getId(), userInfo.getUserId());
            loveVo.setMarriage(userInfo.getMarriage().equals("已婚") ? 1 : 0);
            loveVo.setMatchRate(Convert.toInt(score));
            Boolean likeUser = userLikeApi.likeUser2(user.getId(), userInfo.getUserId());
            loveVo.setAlreadyLove(likeUser);
            loveVoList.add(loveVo);
        }
        return loveVoList;
    }

    /**
     * 是否喜欢
     *
     * @param userId
     * @return
     */
    public boolean isNotLove(Long userId) {
        User user = UserThreadLocal.get();
        Boolean like = userLikeApi.isLike(user.getId(), userId);

        return like;
    }

    /**
     * 验证并解析
     * 验证图片是否为真人
     * 将图片上传至阿里云oss服务器
     * 将图片地址写入数据库
     *
     * @param token
     * @param headPhoto
     */
    public void head(String token, MultipartFile headPhoto) {
        //验证并解析token
        User user = UserThreadLocal.get();
        boolean isLegal = false;
        //校验图片格式(比对后缀名
        for (String type : IMAGE_TYPE) {
            if (StringUtils.endsWithIgnoreCase(headPhoto.getOriginalFilename(), type)) {
                isLegal = true;
                break;
            }
        }
        //类型错误则通知用户
        if (!isLegal) {
            throw new BusinessException(ErrorResult.picTypeFail());
        }
        //人脸识别
        boolean isPerson = false;
        try {
            isPerson = faceEngineService.checkIsPortrait(headPhoto.getBytes());
        } catch (IOException e) {
            throw new BusinessException(ErrorResult.picParseFail());
        }
        if (!isPerson) {
            throw new BusinessException(ErrorResult.faceError());
        }
        //验证图片并上传
        String picUrl = null;
        try {
            picUrl = ossTemplate.upload(headPhoto);
        } catch (Exception e) {
            throw new BusinessException(ErrorResult.picUplodFail());
        }
        /*if (picUrl == null) {
            throw new BusinessException(ErrorResult.faceError());
        }*/
        //String picUrl = "123";
        //将图片地址写入数据库
        UserInfo userInfo = new UserInfo();
        userInfo.setLogo(picUrl);
        LambdaQueryWrapper<UserInfo> warpper = new LambdaQueryWrapper<>();
        warpper.eq(UserInfo::getUserId, user.getId());
        int update = userInfoMapper.update(userInfo, warpper);
        //更新记录数为0 则更新失败
        if (update == 0) {
            throw new BusinessException(ErrorResult.updateLogoFail());
        }
    }
}
