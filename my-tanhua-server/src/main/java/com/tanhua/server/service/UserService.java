package com.tanhua.server.service;

import com.tanhua.common.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserService {
    @Autowired
    private RestTemplate restTemplate;
    @Value("${tanhua.sso.url}")
    private String ssoUrl;

    /**
     * 调用sso系统解析token
     *
     * @param token
     * @return
     */
    public User queryToken(String token) {
        //String url = "http://localhost:18080/user/"+token;
        String url = ssoUrl + "/user/" + token;
        return restTemplate.getForObject(url, User.class);
    }
}
