package com.tanhua.server.service;

import com.tanhua.server.enums.IdType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class IdService {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    //生成自增长的id，原理：使用redis的自增长值
    public Long createId(IdType idType) {
        String idKey = "TANHUA_ID_" + idType.toString();
        return this.redisTemplate.opsForValue().increment(idKey);
    }
}
