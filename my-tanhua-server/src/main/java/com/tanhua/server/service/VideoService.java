package com.tanhua.server.service;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.PageUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.api.VideoApi;
import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.pojo.Video;
import com.tanhua.oss.OssTemplate;
import com.tanhua.server.enums.IdType;
import com.tanhua.server.vo.PageResp;
import com.tanhua.server.vo.VideoVo;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Service
public class VideoService {
    @Reference
    private VideoApi videoApi;
    @Reference
    private QuanZiApi quanZiApi;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private OssTemplate ossTemplate;
    @Autowired
    private IdService idService;
    @Autowired
    private MyCenterService myCenterService;

    /**
     * 查询小视频列表
     * 查询数据库获取数据
     * 封装结果对象
     *
     * @param page
     * @param pageSize
     * @return
     */
    public PageResp queryVideoList(Integer page, Integer pageSize) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //查询数据库获取video集合
        List<Video> videoList = videoApi.queryVideoList(user.getId(), page, pageSize);
        //封装对象
        List<VideoVo> videoVoList = new ArrayList<>();
        for (Video video : videoList) {
            VideoVo videoVo = new VideoVo();
            //根据视频发布者的id查询用户信息
            LambdaQueryWrapper<UserInfo> warpper = new LambdaQueryWrapper<>();
            warpper.eq(UserInfo::getUserId, video.getUserId());
            UserInfo userInfo = userInfoMapper.selectOne(warpper);
            //视频id
            videoVo.setId(video.getId().toHexString());
            //视频发布者id
            videoVo.setUserId(video.getUserId());
            //视频发布者的头像
            videoVo.setAvatar(userInfo.getLogo());
            //视频发布者的昵称
            videoVo.setNickname(userInfo.getNickName());
            //视频的封面
            videoVo.setCover(video.getPicUrl());
            //视频的URL
            videoVo.setVideoUrl(video.getVideoUrl());
            //视频发布者的签名
            videoVo.setSignature("1234567");//TODO
            //视频的点赞数量
            videoVo.setLikeCount(Convert.toInt(quanZiApi.queryCommentCount(video.getId().toHexString(), CommentType.LIKE)));
            //是否已赞(1/0)
            videoVo.setHasLiked(quanZiApi.queryUserIsLikeOrLove(user.getId(), video.getId().toHexString(), CommentType.LIKE) ? 1 : 0);
            //是否已关注(1/0)
            videoVo.setHasFocus(videoApi.isFollowUser(user.getId(), video.getUserId()) ? 1 : 0);
            //视频的评论数量
            videoVo.setCommentCount(Convert.toInt(quanZiApi.queryCommentCount(video.getId().toHexString(), CommentType.COMMENT)));
            //把对象加入集合
            videoVoList.add(videoVo);
        }
        //封装结果对象
        PageResp resp = new PageResp();
        resp.setCounts(videoVoList.size());
        resp.setPage(page);
        resp.setPagesize(pageSize);
        resp.setPages(PageUtil.totalPage(resp.getCounts(), pageSize));
        resp.setItems(videoVoList);
        return resp;
    }

    /**
     * 发布小视频
     * 上传封面
     * 上传视频
     * 写入数据库
     *
     * @param picFile   封面文件
     * @param videoFile 视频文件
     */
    public void saveVideo(MultipartFile picFile, MultipartFile videoFile) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //上传封面并获取封面URL
        String picUrl = ossTemplate.upload(picFile);
        //上传视频并获取视频URL
        String videoUrl = ossTemplate.upload(videoFile);
        //封装video对象
        Video video = new Video();
        video.setId(new ObjectId());
        //设置自增长id
        video.setVid(idService.createId(IdType.VIDEO));
        //设置发布者id
        video.setUserId(user.getId());
        //设置封面URL
        video.setPicUrl(picUrl);
        //设置视频URL
        video.setVideoUrl(videoUrl);
        //设置创建时间
        video.setCreated(System.currentTimeMillis());
        //设置谁可以看
        video.setSeeType(1);
        //写入数据库
        videoApi.saveVideo(video);
    }

    /**
     * 视频用户关注
     * 写入数据库(MongoDB)
     * 写入缓存(Redis)
     *
     * @param id 视频发布者id
     */
    public void saveUserFocusComments(Long id) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //写入数据库及缓存
        videoApi.followUser(user.getId(), id);
    }

    /**
     * 取消视频用户关注
     * 写入数据库(MongoDB)
     * 写入缓存(Redis)
     *
     * @param id 视频发布者id
     */
    public void saveUserUnFocusComments(Long id) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //写入数据库及缓存
        videoApi.disFollowUser(user.getId(), id);
    }

}
