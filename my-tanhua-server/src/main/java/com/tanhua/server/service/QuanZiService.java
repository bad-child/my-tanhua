package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.PageUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.mapper.BlackListMapper;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.BlackList;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.utils.RelativeDateFormat;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.common.vo.ErrorResult;
import com.tanhua.dubbo.server.api.FrozenUserApi;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.api.VisitorsApi;
import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.pojo.*;
import com.tanhua.dubbo.server.vo.CommentVo;
import com.tanhua.oss.OssTemplate;
import com.tanhua.server.enums.IdType;
import com.tanhua.server.vo.PageResp;
import com.tanhua.server.vo.QuanZiVo;
import com.tanhua.server.vo.VisitorsVo;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class QuanZiService {
    @Reference
    private QuanZiApi quanZiApi;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private OssTemplate ossTemplate;
    @Autowired
    private IdService idService;
    @Autowired
    private TimeLineService timeLineService;
    @Reference
    private VisitorsApi visitorsApi;
    @Autowired
    private MyCenterService myCenterService;
    @Autowired
    private BlackListMapper blackListMapper;

    @Reference
    private FrozenUserApi frozenUserApi;

    /**
     * 查询好友动态
     * 获取当前登录用户
     * 查询登录用户的时间线获取所有的动态id(MongoDB)
     * 根据动态id在动态表中获取动态信息(MongoDB)
     * 封装结果对象
     *
     * @param page
     * @param pagesize
     * @return
     */
    public PageResp queryFriendList(Integer page, Integer pagesize) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //查询登录用户的时间线,获取所有动态
        List<TimeLine> timeLineList = quanZiApi.queryTimeLine(user.getId());
        //将publishId属性组成成集合
        List<Object> publishIdList = CollUtil.getFieldValues(timeLineList, "publishId");
        //根据publishId获取动态信息
        List<Publish> publishList = quanZiApi.queryPublishList(publishIdList, page, pagesize);
        //将publish对象转化为QuanZiVo对象
        List<QuanZiVo> quanZiVoList = toQuanZiVoList(publishList, user.getId());

        PageResp resp = new PageResp();
        resp.setCounts(CollUtil.size(timeLineList));
        resp.setPagesize(pagesize);
        resp.setPages(PageUtil.totalPage(resp.getCounts(), resp.getPagesize()));
        resp.setPage(page);
        resp.setItems(quanZiVoList);
        return resp;
    }

    /**
     * 查询系统推荐动态
     * 获取当前登录用户
     * 查询当前用户的系统推荐动态(redis)
     * 根据动态pid查找动态信息(MongoDB)
     * 封装结果对象
     *
     * @param page
     * @param pagesize
     * @return
     */
    public PageResp queryRecommendList(Integer page, Integer pagesize) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //查询系统推荐动态
        String redisKey = "QUANZI_PUBLISH_RECOMMEND_" + user.getId();
        String redisData = redisTemplate.opsForValue().get(redisKey);
        List<String> split = StrUtil.split(redisData, ",");
        List<Long> pids = new ArrayList<>();
        for (String pid : split) {
            pids.add(Long.valueOf(pid));
        }
        //根据动态pid查找动态信息
        List<Publish> publishList = quanZiApi.queryRecommendList(pids, page, pagesize);
        //将publish对象转化为QuanZiVo对象
        List<QuanZiVo> quanZiVoList = toQuanZiVoList(publishList, user.getId());
        //去除黑名单中的用户
        for (int i = 0; i < quanZiVoList.size(); i++) {
            LambdaQueryWrapper<BlackList> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(BlackList::getUserId, user.getId());
            wrapper.eq(BlackList::getBlackUserId, quanZiVoList.get(i).getUserId());
            Integer count = blackListMapper.selectCount(wrapper);
            if (count > 0) {
                quanZiVoList.remove(i);
                i--;
            }
        }
        //封装为结果对象
        PageResp resp = new PageResp();
        resp.setCounts(CollUtil.size(pids));
        resp.setPagesize(pagesize);
        resp.setPages(PageUtil.totalPage(resp.getCounts(), resp.getPagesize()));
        resp.setPage(page);
        resp.setItems(quanZiVoList);
        return resp;
    }

    //将Publish集合封装为QuanZiVo集合
    private List<QuanZiVo> toQuanZiVoList(List<Publish> publishList, Long id) {
        List<QuanZiVo> quanZiVoList = new ArrayList<>();
        for (Publish publish : publishList) {
            QuanZiVo quanZiVo = new QuanZiVo();
            quanZiVo.setId(publish.getId().toHexString());
            quanZiVo.setUserId(publish.getUserId());
            quanZiVo.setTextContent(publish.getText());
            quanZiVo.setImageContent(publish.getMedias().toArray(new String[]{}));
            quanZiVo.setCreateDate(RelativeDateFormat.format(new Date(publish.getCreated())));
            //根据用户id在MySQL中查询用户信息
            LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(UserInfo::getUserId, publish.getUserId());
            UserInfo userInfo = userInfoMapper.selectOne(wrapper);

            quanZiVo.setAvatar(userInfo.getLogo());
            quanZiVo.setNickname(userInfo.getNickName());
            quanZiVo.setAge(userInfo.getAge());
            quanZiVo.setGender(userInfo.getSex().name().toLowerCase());
            quanZiVo.setTags(StringUtils.split(userInfo.getTags(), ','));
            quanZiVo.setCommentCount(Convert.toInt(quanZiApi.queryCommentCount(publish.getId().toHexString(), CommentType.COMMENT))); // 评论数
            quanZiVo.setDistance("1.2公里"); //TODO 距离
            quanZiVo.setHasLiked(quanZiApi.queryUserIsLikeOrLove(id, publish.getId().toHexString(), CommentType.LIKE) ? 1 : 0); // 是否点赞（1是，0否）
            quanZiVo.setLikeCount(Convert.toInt(quanZiApi.queryCommentCount(publish.getId().toHexString(), CommentType.LIKE))); // 点赞数
            quanZiVo.setHasLoved(quanZiApi.queryUserIsLikeOrLove(id, publish.getId().toHexString(), CommentType.LOVE) ? 1 : 0); // 是否喜欢（1是，0否）
            quanZiVo.setLoveCount(Convert.toInt(quanZiApi.queryCommentCount(publish.getId().toHexString(), CommentType.LOVE))); // 喜欢数

            quanZiVoList.add(quanZiVo);
        }
        return quanZiVoList;
    }

    /**
     * 发布动态
     * 获取当前登录用户
     * 上传图片
     * 将数据封装为Publish对象
     * 将Publish对象写入动态表
     * 写入信息到相册表
     * 查询当前登录用户的好友列表
     * 写入信息到好友的时间线
     *
     * @param textContent
     * @param location
     * @param latitude
     * @param longitude
     * @param multipartFile
     */
    public String savePublish(String textContent, String location, String latitude, String longitude, MultipartFile[] multipartFile) {
        User user = UserThreadLocal.get();

        String status = redisTemplate.opsForValue().get("FROZEN_" + user.getId());
        Long expireTime = redisTemplate.opsForValue().getOperations().getExpire("FROZEN_" + user.getId());


        FrozenUser frozenUser = frozenUserApi.getFrozenUser(user.getId());

        if (ObjectUtil.isNotEmpty(frozenUser)){
            String reason = frozenUser.getReasonsForFreezing();

            if (expireTime == -1) {
                return "您的发布动态功能已被永久冻结,冻结原因:"+reason+",如有疑问,请联系管理员";
            }


            // 剩余小时与分钟
            Long hour = expireTime / 60 / 60;
            Long minute = expireTime / 60 % 60;
            Long second = expireTime % 60;



            if (StrUtil.equals(status, "3")) {
                return "您的发布动态功能已被冻结,冻结原因:" + reason + ",剩余冻结时长:" + hour + "小时" + minute + "分钟" + second + "秒";
            }
        }



        List<String> uploads = ossTemplate.uploads(multipartFile);
        //将数据封装为Publish对象
        Publish publish = new Publish();
        publish.setId(new ObjectId());
        publish.setUserId(user.getId());
        publish.setMedias(uploads);
        publish.setText(textContent);
        publish.setLocationName(location);
        publish.setLatitude(latitude);
        publish.setLongitude(longitude);
        publish.setCreated(System.currentTimeMillis());
        publish.setSeeType(1);
        publish.setPid(idService.createId(IdType.PUBLISH));
        //将Publish对象写入动态表
        quanZiApi.savePublish(publish);
        //写入信息到相册表
        Album album = new Album();
        album.setPublishId(publish.getId());
        album.setCreated(publish.getCreated());
        quanZiApi.saveAlbum(album, user.getId());
        //查询当前登录用户的好友列表
        //写入信息到好友的时间线(异步执行)
        timeLineService.saveTimeLine(user.getId(), publish);

        return null;
    }

    /**
     * 点赞动态并查询动态的点赞数
     * 获取当前登录用户
     * 向数据库中写入信息
     * 查询动态的点赞数
     *
     * @param publishId 动态id
     * @return
     */
    public Long likeComment(String publishId) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //向评论表写入信息
        Boolean result = quanZiApi.insertComment(user.getId(), publishId, CommentType.LIKE, null);
        //点赞成功,则查询点赞数量
        if (result) {
            return quanZiApi.queryCommentCount(publishId, CommentType.LIKE);
        }
        throw new BusinessException(ErrorResult.likeError());
    }

    /**
     * 取消点赞动态并查询动态的点赞数
     * 获取当前登录用户
     * 从数据库中删除数据
     * 查询动态的点赞数
     *
     * @param publishId
     * @return
     */
    public Long disLikeComment(String publishId) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //从评论表中删除数据
        Boolean result = quanZiApi.delComment(user.getId(), publishId, CommentType.LIKE);
        //取消点赞成功,则查询点赞数量
        if (result) {
            return quanZiApi.queryCommentCount(publishId, CommentType.LIKE);
        }
        throw new BusinessException(ErrorResult.disLikeError());
    }

    /**
     * 喜欢动态并查询动态的喜欢数
     * 获取当前登录用户
     * 向数据库中写入信息
     * 查询动态的喜欢数
     *
     * @param publishId 动态id
     * @return
     */
    public Long loveComment(String publishId) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //向评论表写入信息
        Boolean result = quanZiApi.insertComment(user.getId(), publishId, CommentType.LOVE, null);
        //点赞成功,则查询点赞数量
        if (result) {
            return quanZiApi.queryCommentCount(publishId, CommentType.LOVE);
        }
        throw new BusinessException(ErrorResult.loveError());
    }

    /**
     * 取消喜欢动态并查询动态的喜欢数
     * 获取当前登录用户
     * 从数据库中删除数据
     * 查询动态的喜欢数
     *
     * @param publishId 动态id
     * @return
     */
    public Long disLoveComment(String publishId) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //从评论表中删除数据
        Boolean result = quanZiApi.delComment(user.getId(), publishId, CommentType.LOVE);
        //取消点赞成功,则查询点赞数量
        if (result) {
            return quanZiApi.queryCommentCount(publishId, CommentType.LOVE);
        }
        throw new BusinessException(ErrorResult.disloveError());
    }

    /**
     * 查询单个动态信息
     * 获取当前登录用户
     * 从MongoDB中查询动态信息
     * 从mysql中查询用户信息
     * 封装结果对象
     *
     * @param publishId 动态id
     * @return
     */
    public QuanZiVo queryById(String publishId) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //从MongoDB中查询动态信息
        List<Object> list = new ArrayList<>();
        list.add(publishId);
        List<Publish> publishList = quanZiApi.queryPublishList(list, 1, 1);
        if (publishList.isEmpty()) {
            throw new BusinessException(ErrorResult.contentError());
        }
        //封装结果对象
        return toQuanZiVoList(publishList, user.getId()).get(0);
    }

    /**
     * 查询动态/视频评论列表
     * 获取当前登录用户
     * 根据动态id从MongoDB查询评论信息
     * 从mysql中查询用户信息
     * 封装结果对象
     *
     * @param publishId 动态id/视频id
     * @param page
     * @param pageSize
     * @return
     */
    public PageResp queryCommentList(String publishId, Integer page, Integer pageSize) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //根据动态id从MongoDB查询评论信息
        List<Comment> commentList = quanZiApi.queryCommentList(publishId);
        if (commentList.isEmpty()) {
            return null;
        }

        List<CommentVo> commentVoList = new ArrayList<>();
        for (Comment comment : commentList) {
            //从mysql中查询用户信息
            LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(UserInfo::getUserId, comment.getUserId());
            UserInfo userInfo = userInfoMapper.selectOne(wrapper);
            //封装对象
            CommentVo commentVo = new CommentVo();
            commentVo.setId(comment.getId().toHexString());
            commentVo.setAvatar(userInfo.getLogo());
            commentVo.setNickname(userInfo.getNickName());
            commentVo.setContent(comment.getContent());
            commentVo.setCreateDate(DateUtil.format(new Date(comment.getCreated()), "HH:mm"));
            commentVo.setLikeCount(Convert.toInt(quanZiApi.queryCommentCount(comment.getId().toHexString(), CommentType.LIKE)));
            commentVo.setHasLiked(quanZiApi.queryUserIsLikeOrLove(user.getId(), comment.getId().toHexString(), CommentType.LIKE) ? 1 : 0);

            commentVoList.add(commentVo);
        }
        //封装结果对象
        PageResp resp = new PageResp();
        resp.setCounts(commentVoList.size());
        resp.setPagesize(pageSize);
        resp.setPage(page);
        resp.setPages(PageUtil.totalPage(commentList.size(), pageSize));
        resp.setItems(commentVoList);
        return resp;
    }

    /**
     * 保存动态/视频评论
     * 获取当前登录用户
     * 在comment表中插入数据
     * 更新redis中的评论数
     *
     * @param publishId 动态id/视频id
     * @param comment   评论正文
     */
    public String saveComment(String publishId, String comment) {
        //获取当前登录用户
        User user = UserThreadLocal.get();


        // 判断用户发言是否被冻结
        String status = redisTemplate.opsForValue().get("FROZEN_" + user.getId());
        Long expireTime = redisTemplate.opsForValue().getOperations().getExpire("FROZEN_" + user.getId());

        FrozenUser frozenUser = frozenUserApi.getFrozenUser(user.getId());

        if (ObjectUtil.isNotEmpty(frozenUser)){
            String reason = frozenUser.getReasonsForFreezing();

            if (expireTime == -1) {
                return "您的发言已被永久冻结,冻结原因:"+reason+",如有疑问,请联系管理员";
            }


            // 剩余小时与分钟
            Long hour = expireTime / 60 / 60;
            Long minute = expireTime / 60 % 60;
            Long second = expireTime % 60;




            if (StrUtil.equals(status, "2")) {
                return "您的发言已被冻结,冻结原因:" + reason + ",剩余冻结时长:" + hour + "小时" + minute + "分钟" + second + "秒";
            }

        }





        /*//判断当前用户是否在陌生人黑名单中
        Boolean resultBlack = myCenterService.isBlackUser(Long.valueOf(publishId), user.getId());
        if (resultBlack) {
            throw new BusinessException(ErrorResult.blackListError());
        }*/
        //在comment表中插入数据
        quanZiApi.insertComment(user.getId(), publishId, CommentType.COMMENT, comment);

        return null;
    }

    /**
     * 查询个人所有动态
     *
     * @param userId   目标用户id
     * @param page
     * @param pageSize
     * @return
     */
    public PageResp queryAlbumList(Long userId, Integer page, Integer pageSize) {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //查询动态信息
        List<Publish> publishList = quanZiApi.queryPublishList(userId, page, pageSize);
        //封装对象
        List<QuanZiVo> quanZiVoList = toQuanZiVoList(publishList, user.getId());
        PageResp pageResp = new PageResp();
        pageResp.setPage(page);
        pageResp.setPages(pageSize);
        pageResp.setItems(quanZiVoList);
        return pageResp;
    }

    /**
     * 查询来访者列表
     *
     * @return
     */
    public List<VisitorsVo> queryVisitorsList() {
        //获取当前登录用户
        User user = UserThreadLocal.get();
        //获取来访者列表
        List<Visitors> visitorsList = visitorsApi.queryMyVisitor(user.getId());
        if (CollUtil.isEmpty(visitorsList)) {
            return Collections.emptyList();
        }
        //根据来访者id查询来访者用户信息
        List<Object> userIds = CollUtil.getFieldValues(visitorsList, "visitorUserId");
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(UserInfo::getUserId, userIds);
        List<UserInfo> userInfoList = userInfoMapper.selectList(wrapper);
        //封装结果对象
        List<VisitorsVo> visitorsVoList = new ArrayList<>();
        for (Visitors visitor : visitorsList) {
            for (UserInfo userInfo : userInfoList) {
                if (ObjectUtil.equal(visitor.getVisitorUserId(), userInfo.getUserId())) {
                    VisitorsVo visitorsVo = new VisitorsVo();
                    visitorsVo.setAge(userInfo.getAge());
                    visitorsVo.setAvatar(userInfo.getLogo());
                    visitorsVo.setGender(userInfo.getSex().getValue() == 1 ? "man" : "woman");
                    visitorsVo.setId(userInfo.getUserId());
                    visitorsVo.setNickname(userInfo.getNickName());
                    visitorsVo.setTags(StringUtils.split(userInfo.getTags(), ','));
                    visitorsVo.setFateValue(visitor.getScore().intValue());
                    visitorsVoList.add(visitorsVo);
                    break;
                }
            }
        }
        return visitorsVoList;
    }
}
