package com.tanhua.server.service;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.pojo.User;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.common.vo.ErrorResult;
import com.tanhua.dubbo.server.api.DashBoardApi;
import com.tanhua.dubbo.server.api.UserLocationApi;
import com.tanhua.dubbo.server.pojo.UserActiveRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BaiduService {
    @Reference
    private UserLocationApi userLocationApi;

    @Reference
    private DashBoardApi dashBoardApi;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    /**
     * 更新地理位置
     *
     * @param longitude 经度
     * @param latitude  纬度
     * @param addrStr   位置描述
     */
    public void updateLocation(Double longitude, Double latitude, String addrStr) {
        //获取当前登录用户
        User user = UserThreadLocal.get();

        // 将登录地址存入redis
        String redisKey = "login" + user.getId();
        List<UserActiveRecord> userActiveRecordList = dashBoardApi.queryLoginUser(user.getId());

        Long activeTime = userActiveRecordList.get(0).getActiveTime();
        String hashKey = activeTime.toString();
        Object o = redisTemplate.opsForHash().get(redisKey, hashKey);
        if (ObjectUtil.isEmpty(o)){
            redisTemplate.opsForHash().put(redisKey,hashKey,addrStr);
        }

        //更新地理位置
        Boolean result = userLocationApi.updateUserLocation(user.getId(), longitude, latitude, addrStr);
        if (!result) {
            throw new BusinessException(ErrorResult.updateUserLocationError());
        }
    }
}
