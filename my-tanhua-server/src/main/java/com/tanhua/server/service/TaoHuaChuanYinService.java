package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.mapper.UserMapper;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.common.vo.ErrorResult;
import com.tanhua.dubbo.server.api.SoundApi;
import com.tanhua.dubbo.server.pojo.Sound;
import com.tanhua.oss.OssTemplate;
import com.tanhua.server.vo.SoundVo;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.Duration;
import java.util.List;

@Service
public class TaoHuaChuanYinService {
    @Autowired
    private OssTemplate ossTemplate;
    @Reference
    private SoundApi soundApi;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private MyCenterService myCenterService;

    @Autowired
    private UserMapper userMapper;


    /**
     * 保存语音
     *
     * @param soundFile
     * @return
     */
    public Boolean saveSound(MultipartFile soundFile) {
        User user = UserThreadLocal.get();

        //创建redis语音键
        //在redis里设置一个唯一的缓存键，根据当天的时间设置，并且根据下面的清除缓存设置清除


        UserInfo userInfo = userInfoService.queryById(user.getId());
        Sound sound = new Sound();
        sound.setId(new ObjectId());
        sound.setUserId(user.getId());
        sound.setGender(userInfo.getSex().name());

        // 上传 阿里云
        String filePath = ossTemplate.upload(soundFile);
        sound.setSoundUrl(filePath);

        soundApi.saveSound(sound);
        return true;
    }

    /**
     * @return
     */
    public SoundVo querySound() {
        User user = UserThreadLocal.get();
        //判断他的接收次数
        //创建redis键(接收传音每天只能10次)

        //记当天的接收次数
        String key = user.getId() + "_SOUND";

        //判断
        String keyValue = redisTemplate.opsForValue().get(key);

        if (StrUtil.equals(keyValue, "0")) {
            throw new BusinessException(ErrorResult.surplusCountFail());
        }

        Long count;


        // 当前用户信息
        UserInfo userInfoNow = userInfoService.queryById(user.getId());

        //获取到可用的声音文件的集合
        List<Sound> sounds = soundApi.querySounds(user.getId(), userInfoNow.getSex().name());

        if (CollUtil.isEmpty(sounds)) {
            throw new BusinessException(ErrorResult.noSoundsFail());
        }

        if (StrUtil.isEmpty(keyValue)) {
            // 剩余时间
            long surplusTime = DateUtil.endOfDay(DateUtil.date()).getTime() - System.currentTimeMillis();
            redisTemplate.opsForValue().set(key, "9", Duration.ofMinutes(surplusTime));
            count = 9L;
        } else {
            count = redisTemplate.opsForValue().increment(key, -1);
        }
        //通过createSoundUserId方法，传入sounds和当前用户的对象，随机获取非当前用户的传音

        int randomInt = RandomUtil.randomInt(sounds.size());

        Sound sound = sounds.get(randomInt);

        // 将该条语音状态改为 不可用 0
        soundApi.removeSound(sound.getId());

        // 获取接收语音的用户信息
        UserInfo userInfo = userInfoService.queryById(sound.getUserId());

        //返回响应
        SoundVo soundVo = new SoundVo();
        soundVo.setUserId(Convert.toInt(sound.getUserId()));
        soundVo.setAvatar(userInfo.getLogo());
        soundVo.setNickname(userInfo.getNickName());
        soundVo.setGender(userInfo.getSex().name().toLowerCase());
        soundVo.setAge(userInfo.getAge());
        soundVo.setSoundUrl(sound.getSoundUrl());

        soundVo.setRemainingTimes(Convert.toInt(count));

        return soundVo;

    }


}
