package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.common.vo.ErrorResult;
import com.tanhua.dubbo.server.pojo.RecommendUser;
import com.tanhua.dubbo.server.vo.PageParams;
import com.tanhua.server.vo.PageResp;
import com.tanhua.server.vo.TodayBest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TodayBestService {
    @Autowired
    private UserService userService;
    @Autowired
    private RecommendUserService recommendUserService;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Value("${tanhua.sso.default.user}")
    private Long defaultUser;

    /**
     * 查询和当前用户score值最高的用户
     * 调用sso系统解析token(由过滤器执行)
     * 根据当前用户id在MongoDB中查找score值最高的用户id(dubbo)
     * 根据查询到的用户id在mysql中查找用户信息
     * 将结果封装成对象
     *
     * @return
     */
    public TodayBest todayBest() {
        //获取User对象
        User user = UserThreadLocal.get();
        //根据当前用户id查找score值最高的用户
        RecommendUser recommendUser = recommendUserService.queryTodayBest(user.getId());
        //设置默认
        if (recommendUser == null) {
            recommendUser = new RecommendUser();
            recommendUser.setUserId(defaultUser);
            recommendUser.setScore(88D);
        }
        //根据查询到的用户id在mysql中查找用户信息
        LambdaQueryWrapper<UserInfo> warpper = new LambdaQueryWrapper<>();
        warpper.eq(UserInfo::getUserId, recommendUser.getUserId());
        UserInfo userInfo = userInfoMapper.selectOne(warpper);
        //将结果进行封装
        TodayBest todayBest = new TodayBest();
        todayBest.setId(recommendUser.getUserId().intValue());
        todayBest.setFateValue(recommendUser.getScore().intValue());
        todayBest.setGender(userInfo.getSex().getValue() == 1 ? "man" : "woman");
        todayBest.setNickname(userInfo.getNickName());
        todayBest.setAge(userInfo.getAge());
        todayBest.setTags(userInfo.getTags().split(","));
        todayBest.setAvatar(userInfo.getLogo());
        return todayBest;
    }

    /**
     * 查询当前用户的推荐列表
     * 调用sso系统解析token(由过滤器执行)
     * 根据当前用户id在MongoDB中查找用户列表(dubbo)
     * 遍历用户列表,根据用户id和条件在mysql中查询用户信息
     * 将结果封装成对象
     *
     * @param pageParams 筛选条件
     * @return
     */
    public PageResp recommendation(PageParams pageParams) {
        //调用sso系统解析token
        User user = UserThreadLocal.get();
        //根据当前用户id在MongoDB中查找用户列表
        List<RecommendUser> recommendUserList = recommendUserService.queryRecommendation(user.getId());
        if (recommendUserList.size() == 0) {
            throw new BusinessException(ErrorResult.noRecommendationFail());
        }
        //遍历用户列表获取id集合
        Map<Long, RecommendUser> recommendUserMap = new HashMap<>();
        for (RecommendUser recommendUser : recommendUserList) {
            recommendUserMap.put(recommendUser.getUserId(), recommendUser);
        }
        //设置查询条件
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        //设置查询的id
        wrapper.in(UserInfo::getUserId, recommendUserMap.keySet());
        /*//设置性别条件
        if (pageParams.getGender() != null) {
            wrapper.eq(UserInfo::getSex, pageParams.getGender().equals("man") ? 1 : 2);
        }
        //设置年龄条件
        if (pageParams.getAge() != null) {
            wrapper.le(UserInfo::getAge, pageParams.getAge());
        }
        //设置城市条件
        if (pageParams.getCity() != null) {
            wrapper.like(UserInfo::getCity, pageParams.getCity());
        }
        //设置学历条件
        if (pageParams.getEducation() != null) {
            wrapper.eq(UserInfo::getEdu, pageParams.getEducation());
        }*/
        //在mysql中查询用户信息
        List<UserInfo> userInfoList = userInfoMapper.selectList(wrapper);
        if (userInfoList.size() == 0) {
            throw new BusinessException(ErrorResult.noRecommendationFail());
        }
        //封装结果对象
        PageResp pageResp = new PageResp();
        pageResp.setCounts(userInfoList.size());
        pageResp.setPagesize(pageParams.getPagesize());
        //根据总记录数和每页条数计算总页数
        pageResp.setPages(PageUtil.totalPage(pageResp.getCounts(), pageResp.getPagesize()));
        pageResp.setPage(pageParams.getPage());

        List<TodayBest> list = new ArrayList<>();
        //遍历mysql查到的信息
        for (UserInfo userInfo : userInfoList) {
            TodayBest todayBest = new TodayBest();

            todayBest.setId(userInfo.getUserId().intValue());
            //根据id从recommendUserMap中获取对应id的recommendUser对象,从对象中获取相应的score值
            todayBest.setFateValue(recommendUserMap.get(userInfo.getUserId()).getScore().intValue());
            todayBest.setAge(userInfo.getAge());
            todayBest.setAvatar(userInfo.getLogo());
            todayBest.setNickname(userInfo.getNickName());
            todayBest.setTags(userInfo.getTags().split(","));
            todayBest.setGender(userInfo.getSex().getValue() == 1 ? "man" : "woman");

            list.add(todayBest);
        }
        //根据score排序
        list.sort(new Comparator<TodayBest>() {
            @Override
            public int compare(TodayBest o1, TodayBest o2) {
                return o2.getFateValue() - o1.getFateValue();
            }
        });
        //根据当前页与每页条数获取相应的数据
        List<TodayBest> sub = CollUtil.sub(list,
                (pageParams.getPage() - 1) * pageParams.getPagesize(),
                pageParams.getPage() * pageParams.getPagesize());
        //将分页后的数据封装到结果对象
        pageResp.setItems(sub);
        return pageResp;
    }
}
