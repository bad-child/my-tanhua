package com.tanhua.server.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.dubbo.server.api.RecommendApi;
import com.tanhua.dubbo.server.pojo.RecommendUser;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecommendUserService {
    @Reference
    private RecommendApi recommendApi;

    /**
     * 查询近日佳人
     *
     * @param id 登录用户id
     * @return
     */
    public RecommendUser queryTodayBest(Long id) {
        return recommendApi.findTodayBest(id);
    }

    /**
     * 查询佳人列表
     *
     * @param id 登录用户id
     * @return
     */
    public List<RecommendUser> queryRecommendation(Long id) {
        return recommendApi.findRecommendation(id);
    }

    /**
     * 查询目标的缘分值
     *
     * @param userId   目标用户id
     * @param toUserid 当前登陆用户id
     * @return
     */
    public Double queryScore(Long userId, Long toUserid) {
        Double score = recommendApi.queryScore(userId, toUserid);
        return score;
    }

    /**
     * 探花-查询推荐列表
     *
     * @param id 登录者id
     * @return
     */
    public List<RecommendUser> queryCardsList(Long id) {
        return recommendApi.queryCardsList(id);
    }
}