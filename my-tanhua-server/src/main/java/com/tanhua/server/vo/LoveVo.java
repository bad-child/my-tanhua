package com.tanhua.server.vo;

import cn.hutool.core.annotation.Alias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoveVo {

    @Alias("userId")
    private Integer id; //用户id
    @Alias("logo")
    private String avatar; //头像
    @Alias("nickName")
    private String nickname; //昵称
    private String gender; //性别 man woman
    private Integer age; //年龄
    private String city; //城市
    @Alias("edu")
    private String education; //学历
    private Integer marriage; //婚姻状态（0未婚，1已婚）
    private Integer matchRate; //匹配度
    private boolean alreadyLove; //是否喜欢ta
}
