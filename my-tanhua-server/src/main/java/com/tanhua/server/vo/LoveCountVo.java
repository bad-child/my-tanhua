package com.tanhua.server.vo;

//互相喜欢，喜欢，粉丝 - 统计封装实体类

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoveCountVo {

    private Integer eachLoveCount;
    private Integer loveCount;
    private Integer fanCount;
}
