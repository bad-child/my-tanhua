package com.tanhua.server.vo;

import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
public class PageResp {
    private Integer counts = 0;//总记录数
    private Integer pagesize = 0;//页大小
    private Integer pages = 0;//总页数
    private Integer page = 0;//当前页码
    private List<?> items = Collections.emptyList(); //列表
}
