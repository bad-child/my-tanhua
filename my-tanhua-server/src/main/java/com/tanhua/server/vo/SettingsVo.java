package com.tanhua.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SettingsVo {
    private Integer id;
    private String strangerQuestion;
    private String phone;
    private boolean likeNotification;
    private boolean pinglunNotification;
    private boolean gonggaoNotification;
}
