package com.tanhua.server;

import com.tanhua.common.mapper.BlackListMapper;
import com.tanhua.dubbo.server.pojo.Questions;
import com.tanhua.server.service.MyCenterService;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class Tests {
    @Autowired
    private BlackListMapper blackListMapper;
    @Autowired
    private MyCenterService myCenterService;

    @Test
    public void testAddBlackList() {
        System.out.println(myCenterService.saveBlackList(23L));
    }

    @Test
    public void testRemoveBlackList() {
        System.out.println(myCenterService.removeBlackList(23L));
    }

}
