package com.tanhua.server;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.common.pojo.User;
import com.tanhua.common.utils.UserThreadLocal;
import com.tanhua.dubbo.server.api.HuanXinApi;
import com.tanhua.server.service.IMService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestHuanxin {
    @Reference
    private HuanXinApi huanXinApi;
    @Autowired
    private IMService imService;

    @Test
    public void testRegisterAllUser() {
        for (int i = 98; i < 100; i++) {
            this.huanXinApi.register(Long.valueOf(i));
        }
    }

    @Test
    public void testDUsers() {
        huanXinApi.addUserFriend( 1L,2L);
    }

    /**
     * 构造好友数据，为1~100用户构造10个好友
     */
    @Test
    public void testUsers() {
        for (int i = 99; i < 100; i++) {
            for (int j = 0; j < 10; j++) {
                User user = new User();
                user.setId(Convert.toLong(i));
                UserThreadLocal.set(user);
                this.imService.contactUser(this.getFriendId(user.getId()));
            }
        }
    }

    private Long getFriendId(Long userId) {
        Long friendId = RandomUtil.randomLong(1, 98);
        if (friendId.intValue() == userId.intValue()) {
            getFriendId(userId);
        }
        return friendId;
    }


}
