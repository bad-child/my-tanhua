package com.tanhua.common.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorResult {

    private String errCode = "999999";
    private String errMessage;

    public static ErrorResult error() {
        return ErrorResult.builder().errCode("999999").errMessage("系统异常稍后再试").build();
    }

    public static ErrorResult fail() {
        return ErrorResult.builder().errCode("000001").errMessage("发送验证码失败").build();
    }

    public static ErrorResult loginError() {
        return ErrorResult.builder().errCode("000002").errMessage("验证码失效").build();
    }

    public static ErrorResult faceError() {
        return ErrorResult.builder().errCode("000003").errMessage("图片非人像，请重新上传!").build();
    }

    public static ErrorResult mobileError() {
        return ErrorResult.builder().errCode("000004").errMessage("手机号码已注册").build();
    }

    public static ErrorResult contentError() {
        return ErrorResult.builder().errCode("000005").errMessage("动态内容为空").build();
    }

    public static ErrorResult likeError() {
        return ErrorResult.builder().errCode("000006").errMessage("用户已点赞").build();
    }

    public static ErrorResult disLikeError() {
        return ErrorResult.builder().errCode("000007").errMessage("用户未点赞").build();
    }

    public static ErrorResult loveError() {
        return ErrorResult.builder().errCode("000008").errMessage("用户已喜欢").build();
    }

    public static ErrorResult disloveError() {
        return ErrorResult.builder().errCode("000009").errMessage("用户未喜欢").build();
    }

    public static ErrorResult redisKeyError() {
        return ErrorResult.builder().errCode("000010").errMessage("验证码未失效").build();
    }

    public static ErrorResult checkCodeError() {
        return ErrorResult.builder().errCode("000011").errMessage("验证码错误").build();
    }

    public static ErrorResult tokenFail() {
        return ErrorResult.builder().errCode("000012").errMessage("token无效").build();
    }

    public static ErrorResult registerFail() {
        return ErrorResult.builder().errCode("000012").errMessage("存储用户信息失败").build();
    }

    public static ErrorResult updateLogoFail() {
        return ErrorResult.builder().errCode("000013").errMessage("更新用户头像失败").build();
    }

    public static ErrorResult picTypeFail() {
        return ErrorResult.builder().errCode("000014").errMessage("图片类型错误").build();
    }

    public static ErrorResult picUplodFail() {
        return ErrorResult.builder().errCode("000015").errMessage("图片上传失败").build();
    }

    public static ErrorResult picParseFail() {
        return ErrorResult.builder().errCode("000016").errMessage("图片解析失败").build();
    }

    public static ErrorResult noRecommendationFail() {
        return ErrorResult.builder().errCode("000016").errMessage("无推荐朋友").build();
    }

    public static ErrorResult errorHuanxinCreateFail() {
        return ErrorResult.builder().errCode("000017").errMessage("创建环信用户失败").build();
    }

    public static ErrorResult errorHuanxinQueryFail() {
        return ErrorResult.builder().errCode("000018").errMessage("查询环信用户信息失败").build();
    }

    public static ErrorResult addFriendError() {
        return ErrorResult.builder().errCode("000019").errMessage("添加好友失败").build();
    }

    public static ErrorResult addHuanXinFriendError() {
        return ErrorResult.builder().errCode("000020").errMessage("添加环信好友失败").build();
    }

    public static ErrorResult errorHuanxinReplyFail() {
        return ErrorResult.builder().errCode("000021").errMessage("回答陌生人问题失败").build();
    }

    public static ErrorResult addHuanxinBlackListError() {
        return ErrorResult.builder().errCode("000022").errMessage("添加环信黑名单失败").build();
    }

    public static ErrorResult removeHuanxinBlackListError() {
        return ErrorResult.builder().errCode("000023").errMessage("移除环信黑名单失败").build();
    }

    public static ErrorResult blackListError() {
        return ErrorResult.builder().errCode("000024").errMessage("在陌生人黑名单中").build();
    }

    public static ErrorResult updateUserLocationError() {
        return ErrorResult.builder().errCode("000025").errMessage("更新用户地理位置失败").build();
    }

    public static ErrorResult likeUserError() {
        return ErrorResult.builder().errCode("000026").errMessage("喜欢用户失败").build();
    }

    public static ErrorResult notLikeUserError() {
        return ErrorResult.builder().errCode("000027").errMessage("不喜欢用户失败").build();
    }

    /**
     * 撤销消息失败
     * @return
     */
    public static ErrorResult deletePublishFail() {
        return ErrorResult.builder().errCode("000028").errMessage("撤销消息失败").build();
    }

    public static ErrorResult loginFail() {
        return ErrorResult.builder().errCode("000028LC").errMessage("登陆失败,用户名或者密码错误").build();

    }

    public static ErrorResult getCodePicFail() {
        return ErrorResult.builder().errCode("000029LC").errMessage("获取验证码失败").build();

    }

    public static ErrorResult AdminInfoFail() {
        return ErrorResult.builder().errCode("000030LC").errMessage("用户名或者密码不得为空").build();

    }

    /**
     * 获取动态集合失败
     * @return
     */
    public static ErrorResult queryPublishListFail() {
        return ErrorResult.builder().errCode("000029").errMessage("该条件下没有查询到数据").build();
    }

    /**
     * 获取动态失败
     * @return
     */
    public static ErrorResult queryPublishFail() {
        return ErrorResult.builder().errCode("000031").errMessage("获取动态失败").build();
    }

    public static ErrorResult updateFile() {
        return ErrorResult.builder().errCode("000028").errMessage("修改用户信息失败").build();
    }

    public static ErrorResult updateMobileFile() {
        return ErrorResult.builder().errCode("000029").errMessage("修改用户手机号失败").build();
    }

    public static ErrorResult timeOver() {
        return ErrorResult.builder().errCode("000028").errMessage("次数用尽,明天再试").build();

    }

    public static ErrorResult upload() {
        return ErrorResult.builder().errCode("000029").errMessage("上传失败").build();
    }

    public static ErrorResult publishPassFail() {
        return ErrorResult.builder().errCode("000030").errMessage("动态审核通过失败").build();


    }

    public static ErrorResult publishRejectFail() {
        return ErrorResult.builder().errCode("000031").errMessage("动态审核拒绝失败").build();


    }

    public static ErrorResult userLoginFrozen() {
        return ErrorResult.builder().errCode("000032").errMessage("该用户已被永久冻结,请联系管理员").build();

    }public static ErrorResult userLoginFrozen(String reason) {
        return ErrorResult.builder().errCode("000032").errMessage("该用户已被永久冻结,冻结原因:"+reason+",如有疑问,请联系管理员").build();

    }


    public static ErrorResult userLoginFrozen(String reason,Long hour,Long minute,Long second) {
        return ErrorResult.builder().errCode("000042").errMessage("该用户已被冻结,冻结原因:"+reason+",剩余冻结时长:"+hour+"小时"+minute+"分钟"+second+"秒").build();

    }

    public static ErrorResult userTalkFrozen() {
        return ErrorResult.builder().errCode("000033").errMessage("您的发言已被永久冻结,请联系管理员").build();

    }

    public static ErrorResult userTalkFrozen(String reason,Long hour,Long minute,Long second) {
        return ErrorResult.builder().errCode("000043").errMessage("您的发言已被冻结,冻结原因:"+reason+",剩余冻结时长:"+hour+"小时"+minute+"分钟"+second+"秒").build();

    }

    public static ErrorResult userPublishFrozen() {
        return ErrorResult.builder().errCode("000034").errMessage("您的发布动态功能已被永久冻结,请联系管理员").build();

    }

    public static ErrorResult userPublishFrozen(String reason,Long hour,Long minute,Long second) {
        return ErrorResult.builder().errCode("000044").errMessage("您的发布动态功能已被冻结,冻结原因:"+reason+",剩余冻结时长:"+hour+"小时"+minute+"分钟"+second+"秒").build();

    }


    public static ErrorResult surplusCountFail() {
        return ErrorResult.builder().errCode("000035").errMessage("今日获取声音剩余次数不足,请明天再试").build();


    }

    public static ErrorResult noSoundsFail() {
        return ErrorResult.builder().errCode("000036").errMessage("未获取到声音,请等待用户添加声音...").build();

    }


    public static ErrorResult codeExpired() {
        return ErrorResult.builder().errCode("000035LC").errMessage("验证码失效,请刷新重试").build();

    }
}