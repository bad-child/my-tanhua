package com.tanhua.common.exception;


import com.tanhua.common.vo.ErrorResult;

import lombok.Data;

//自定义异常类
@Data
public class BusinessException extends RuntimeException {
    private ErrorResult errorResult;


    public BusinessException(ErrorResult errorResult) {
        //调用RuntimeException设置错误消息的构造方法
        super(errorResult.getErrMessage());
        this.errorResult = errorResult;
    }


}
