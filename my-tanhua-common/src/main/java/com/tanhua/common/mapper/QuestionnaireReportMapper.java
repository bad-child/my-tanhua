package com.tanhua.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.dubbo.server.pojo.QuestionnaireReport;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface QuestionnaireReportMapper extends BaseMapper<QuestionnaireReport> {
}
