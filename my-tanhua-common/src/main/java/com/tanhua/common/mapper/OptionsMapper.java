package com.tanhua.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.dubbo.server.pojo.Options;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OptionsMapper extends BaseMapper<Options> {
}
