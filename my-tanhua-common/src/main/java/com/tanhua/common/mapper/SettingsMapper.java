package com.tanhua.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.common.pojo.Settings;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SettingsMapper extends BaseMapper<Settings> {
}
