package com.tanhua.common.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor//提供无参构造方法
@AllArgsConstructor//提供全参构造方法
public class User extends BasePojo{
    private Long id;
    private String mobile;//手机号
    @JsonIgnore//json序列化时忽略
    private String password;//密码
}
