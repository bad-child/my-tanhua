package com.tanhua.common.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor//提供无参构造方法
@AllArgsConstructor//提供全参构造方法
public class Announcement extends BasePojo {
    private String id;
    private String title;
    private String description;
    @TableField("created")
    private String createDate;
}
