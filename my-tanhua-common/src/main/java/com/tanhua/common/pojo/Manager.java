package com.tanhua.common.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Manager extends BasePojo{

    private Long id;//用户id
    private String username;//用户名
    private String password;//用户密码
    private  String logo;//头像


}
