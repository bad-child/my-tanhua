package com.tanhua.common.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Settings extends BasePojo {
    private Long id;
    private Long userId;
    //private String strangerQuestion;
    //private String phone;
    private boolean likeNotification;
    private boolean pinglunNotification;
    private boolean gonggaoNotification;
}
