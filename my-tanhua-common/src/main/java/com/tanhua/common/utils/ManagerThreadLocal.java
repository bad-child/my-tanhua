package com.tanhua.common.utils;

import com.tanhua.common.pojo.Manager;

public class ManagerThreadLocal {

    private static final ThreadLocal<Manager> LOCAL=new ThreadLocal<>();

    private ManagerThreadLocal(){};

    /**
     * 将对象放入到threadLocal
     * @param manager
     */
    public static void set(Manager manager){

        LOCAL.set(manager);
    }

    /**
     * 获取到当前线程中的对象
     * @return
     */
    public static Manager get(){

        return LOCAL.get();
    }

    /**
     * 删除当前线程中的Manager对象
     */
    public static void remove(){
        LOCAL.remove();
    }

}
