package com.tanhua.oss;

import com.aliyun.oss.OSSClient;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

public class OssTemplate {

    private OssProperties ossProperties;

    private OSSClient ossClient;

    public OssTemplate(OssProperties ossProperties) {
        this.ossProperties = ossProperties;
        ossClient = new OSSClient(ossProperties.getEndpoint(), ossProperties.getAccessKeyId(), ossProperties.getAccessKeySecret());
    }

    public String upload(MultipartFile uploadFile) {
        try {
            //给文件设置新路径
            String fileName = uploadFile.getOriginalFilename();
            String filePath = getFilePath(fileName);
            //上传文件到阿里云
            ossClient.putObject(ossProperties.getBucketName(), filePath, new ByteArrayInputStream(uploadFile.getBytes()));
            //返回Url路径
            return this.ossProperties.getUrlPrefix() + filePath;
        } catch (Exception e) {
            return null;
        }
    }

    public List<String> uploads(MultipartFile[] uploadFiles) {
        List<String> list = new ArrayList<>();
        for (MultipartFile uploadFile : uploadFiles) {
            list.add(upload(uploadFile));
        }
        return list;
    }
    private String getFilePath(String sourceFileName) {
        DateTime dateTime = new DateTime();
        return "images/" + dateTime.toString("yyyy")
                + "/" + dateTime.toString("MM")
                + "/" + dateTime.toString("dd")
                + "/" + System.currentTimeMillis()
                + RandomUtils.nextInt(100, 9999) + "."
                + StringUtils.substringAfterLast(sourceFileName, ".");
    }
}
