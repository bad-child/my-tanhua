package com.tanhua.sso.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.mapper.UserMapper;
import com.tanhua.common.pojo.User;
import com.tanhua.common.vo.ErrorResult;
import com.tanhua.dubbo.server.api.DashBoardApi;
import com.tanhua.dubbo.server.api.FrozenUserApi;
import com.tanhua.dubbo.server.api.HuanXinApi;
import com.tanhua.dubbo.server.pojo.FrozenUser;
import com.tanhua.sms.SmsTemplate;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserService {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private UserMapper userMapper;
    @Value("${jwt.secret}")
    private String secret;
    @Autowired
    private SmsTemplate smsTemplate;
    @Reference
    private HuanXinApi huanXinApi;
    @Reference
    private DashBoardApi dashBoardApi;
    @Reference
    private FrozenUserApi frozenUserApi;


    /**
     * 为此手机号在redis中设置验证码并将验证码发送给该手机
     *
     * @param phone 用户手机号
     */
    public void setCodeToRedis(String phone) {

        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getMobile, phone);
        User user = userMapper.selectOne(wrapper);

        String status = redisTemplate.opsForValue().get("FROZEN_" + user.getId());

        Long expireTime = redisTemplate.opsForValue().getOperations().getExpire("FROZEN_" + user.getId());

        FrozenUser frozenUser = frozenUserApi.getFrozenUser(user.getId());


        if (ObjectUtil.isNotEmpty(frozenUser)){
            String reason = frozenUser.getReasonsForFreezing();

            if (expireTime == -1) {
                throw new BusinessException(ErrorResult.userLoginFrozen(reason));
            }

            // 剩余小时与分钟
            Long hour = expireTime / 60 / 60;
            Long minute = expireTime / 60 % 60;
            Long second = expireTime % 60;




            if (StrUtil.equals(status, "1")) {
                throw new BusinessException(ErrorResult.userLoginFrozen(reason, hour, minute,second));
            }
        }



        //redisKey
        String redisKey = "CHECK_CODE_" + phone;
        //判断redis中是否有当前key的存在
        if (Boolean.TRUE.equals(redisTemplate.hasKey(redisKey))) {
            //有则抛出异常
            //throw new RuntimeException("验证码已存在");
            throw new BusinessException(ErrorResult.redisKeyError());
        }
        String code = "123456";
        //随机生成6位数字验证码
        //String code = RandomUtils.nextInt(100000, 999999) + "";
        //生成验证码并发送给该手机
        //code = smsTemplate.sendSms(phone, code);
        //验证码为null则抛出异常
        if (code == null) {
            //throw new RuntimeException("发送验证码失败");
            throw new BusinessException(ErrorResult.fail());
        }
        //存入redis设置有效时间

        redisTemplate.opsForValue().set(redisKey, code, Duration.ofMinutes(1));
        String s = redisTemplate.opsForValue().get(redisKey);

    }

    /**
     * 通过手机号对应的redis中的key判断验证码是否正确
     * 验证码正确则判断该用户是否为新用户
     * 通过手机号和用户id生成token
     *
     * @param phone
     * @param code
     * @return 返回token和是否为新用户
     */
    public Map<String, Object> checkMobileAndCode(String phone, String code) {
        //redisKey
        String redisKey = "CHECK_CODE_" + phone;
        //设置是否为新用户的标记位
        boolean isNew = false;
        //从redis获取验证码
        String redisCode = redisTemplate.opsForValue().get(redisKey);
        //将redis中的验证码清除
        redisTemplate.delete(redisKey);
        //验证码为null则抛出异常
        if (redisCode == null) {
            throw new BusinessException(ErrorResult.loginError());
        }
        //验证码不正确则抛出异常
        if (!code.equals(redisCode)) {
            //throw new RuntimeException("验证码不正确");
            throw new BusinessException(ErrorResult.checkCodeError());
        }
        //判断数据库中是否已有该手机号信息
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getMobile, phone);

        User user = userMapper.selectOne(wrapper);

        //user为null说明该用户为新用户
        if (user == null) {
            //为新用户创建信息
            user = new User();
            user.setMobile(phone);
            user.setPassword("123456");
            //存入数据库
            userMapper.insert(user);
            //注册环信用户
            Boolean register = huanXinApi.register(user.getId());
            if (!register) {
                throw new BusinessException(ErrorResult.errorHuanxinCreateFail());
            }
            //记录用户登录操作
            dashBoardApi.saveUserActive(user.getId(), 1L);
            //改变标记位为true
            isNew = true;
        } else {
            // 若不是新用户 更新 登录时间 信息
            userMapper.updateById(user);
            //记录用户登录操作
            dashBoardApi.saveUserActive(user.getId(), 1L);
        }


        //封装token中要存放的数据
        Map<String, Object> claims = new HashMap<>();
        claims.put("mobile", user.getMobile());
        claims.put("id", user.getId());
        //生成token
        String token = Jwts.builder()
                //.setHeader(header)
                .setClaims(claims)      //存放数据的位置,不能存放敏感数据
                .signWith(SignatureAlgorithm.HS256, secret)  //设置加密方法和加密盐
                .setExpiration(new DateTime().plusHours(12).toDate())   //设置过期时间
                //.setExpiration(new DateTime().plusSeconds(30).toDate())   //设置过期时间
                .compact();
        //String token = "123456";
        //将结果封装为map对象返回
        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("isNew", isNew);
        return map;
    }

    /**
     * 验证并解析token,
     *
     * @param token
     * @return 返回user对象
     */
    public User queryToken(String token) {
        try {
            Map<String, Object> body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
            //将body转化为json字符串
            String jsonString = JSON.toJSONString(body);
            //将json字符串转化为user对象
            User user = JSON.parseObject(jsonString, User.class);
            return user;
        } catch (Exception e) {
            throw new BusinessException(ErrorResult.tokenFail());
        }
    }


}
