package com.tanhua.sso.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.common.enums.SexEnum;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.vo.ErrorResult;
import com.tanhua.oss.OssTemplate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Service
public class UserInfoService {
    private static final String[] IMAGE_TYPE = new String[]{
            ".bmp",
            ".jpg",
            ".jpeg",
            ".gif",
            ".png",
    };
    @Autowired
    private UserService userService;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private OssTemplate ossTemplate;
    @Autowired
    private FaceEngineService faceEngineService;

    /**
     * 验证并解析token
     * 将map中的信息和token中的信息封装成UserInfo对象
     * 存储到数据库中
     *
     * @param token
     * @param map
     */
    public void registerUserInfo(String token, Map<String, String> map) {
        //验证并解析token
        User user = userService.queryToken(token);
        //封装UserInfo对象
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(user.getId());
        userInfo.setSex(SexEnum.MAN.toString().equalsIgnoreCase(map.get("gender")) ? SexEnum.MAN : SexEnum.WOMAN);
        userInfo.setNickName(map.get("nickname"));
        userInfo.setBirthday(map.get("birthday"));
        userInfo.setCity(map.get("city"));
        //存储数据
        int insert = userInfoMapper.insert(userInfo);
        //修改记录数为0 则插入记录失败
        if (insert == 0) {
            throw new BusinessException(ErrorResult.registerFail());
        }
    }

    /**
     * 验证并解析
     * 验证图片是否为真人
     * 将图片上传至阿里云oss服务器
     * 将图片地址写入数据库
     *
     * @param token
     * @param headPhoto
     */
    public void head(String token, MultipartFile headPhoto) {
        //验证并解析token
        User user = userService.queryToken(token);
        boolean isLegal = false;
        //校验图片格式(比对后缀名
        for (String type : IMAGE_TYPE) {
            if (StringUtils.endsWithIgnoreCase(headPhoto.getOriginalFilename(), type)) {
                isLegal = true;
                break;
            }
        }
        //类型错误则通知用户
        if (!isLegal) {
            throw new BusinessException(ErrorResult.picTypeFail());
        }
        //人脸识别
        boolean isPerson = false;
        try {
            isPerson = faceEngineService.checkIsPortrait(headPhoto.getBytes());
        } catch (IOException e) {
            throw new BusinessException(ErrorResult.picParseFail());
        }
        if (!isPerson){
            throw new BusinessException(ErrorResult.faceError());
        }
        //验证图片并上传
        String picUrl = null;
        try {
            picUrl = ossTemplate.upload(headPhoto);
        } catch (Exception e) {
            throw new BusinessException(ErrorResult.picUplodFail());
        }
        /*if (picUrl == null) {
            throw new BusinessException(ErrorResult.faceError());
        }*/
        //String picUrl = "123";
        //将图片地址写入数据库
        UserInfo userInfo = new UserInfo();
        userInfo.setLogo(picUrl);
        LambdaQueryWrapper<UserInfo> warpper = new LambdaQueryWrapper<>();
        warpper.eq(UserInfo::getUserId, user.getId());
        int update = userInfoMapper.update(userInfo, warpper);
        //更新记录数为0 则更新失败
        if (update == 0) {
            throw new BusinessException(ErrorResult.updateLogoFail());
        }
    }

}
