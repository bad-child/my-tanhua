package com.tanhua.sso.controller;

import com.tanhua.common.pojo.Manager;
import com.tanhua.common.pojo.User;
import com.tanhua.common.utils.NoAuthorization;
import com.tanhua.sso.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired//自动注入UserService
    private UserService userService;

    /**
     * 登陆的第一步,接收手机号,为此手机号在redis中设置验证码,将验证码发送给该手机
     *
     * @param map 手机号
     * @return
     */
    @PostMapping("login")

    public ResponseEntity login(@RequestBody Map<String, String> map) {
        //获取手机号
        String phone = map.get("phone");
        //调用service
        userService.setCodeToRedis(phone);
        //返回成功
        return ResponseEntity.ok(null);
    }

    /**
     * 验证手机号和验证码登陆
     *
     * @param map 手机号和验证码
     * @return
     */
    @PostMapping("loginVerification")
    public ResponseEntity loginVerification(@RequestBody Map<String, String> map) {
        //获取手机号
        String phone = map.get("phone");
        //获取验证码
        String code = map.get("verificationCode");
        //调用service并将结果返回
        Map<String, Object> result = userService.checkMobileAndCode(phone, code);
        return ResponseEntity.ok(result);
    }

    /**
     * 对外提供token解析成User
     *
     * @param token
     * @return
     */

    @GetMapping("{token}")//    从url地址中获取token的值
    public User queryToken(@PathVariable("token") String token) {
        return userService.queryToken(token);
    }



}
