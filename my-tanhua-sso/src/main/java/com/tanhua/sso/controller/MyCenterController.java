package com.tanhua.sso.controller;

import com.tanhua.sso.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("users")
public class MyCenterController {
    @Autowired
    private UserInfoService userInfoService;

    @PostMapping("header")
    public ResponseEntity head(@RequestHeader("Authorization") String token, MultipartFile headPhoto) {
        userInfoService.head(token, headPhoto);
        return ResponseEntity.ok(null);
    }
}
