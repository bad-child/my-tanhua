package com.tanhua.sso.controller;

import com.tanhua.sso.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequestMapping("user")
public class UserInfoController {
    @Autowired
    private UserInfoService userInfoService;

    /**
     * 新用户填写资料
     * 先验证token
     * 存储用户信息数据
     *
     * @param token 用于验证用户身份,在请求头中
     * @param map   用户性别,昵称,生日,城市
     * @return 无返回数据
     */
    @PostMapping("loginReginfo")
    public ResponseEntity loginReginfo(@RequestHeader("Authorization") String token, @RequestBody Map<String, String> map) {
        userInfoService.registerUserInfo(token, map);
        return ResponseEntity.ok(null);
    }

    /**
     * 验证并上传头像
     * 先验证token
     * 校验图片是否为真人
     * 将图片上传至阿里云oss服务器
     * 将图片路径写入数据库
     *
     * @param token     用于验证用户身份,在请求头中
     * @param headPhoto 用户上传的图片
     * @return 无返回数据
     */
    @PostMapping("loginReginfo/head")
    public ResponseEntity head(@RequestHeader("Authorization") String token, MultipartFile headPhoto) {
        userInfoService.head(token, headPhoto);
        return ResponseEntity.ok(null);
    }
}
