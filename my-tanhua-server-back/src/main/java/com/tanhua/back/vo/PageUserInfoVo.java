package com.tanhua.back.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户管理:翻页返回对象
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageUserInfoVo {
    private Integer id;
    private String logo;
    private String logoStatus = "1";// 头像状态(1通过,2拒绝)
    private String nickname;// 昵称
    private String mobile;// 手机号
    private String sex;
    private Integer age;
    private String occupation;// 职业
    private String userStatus;// 用户状态(1正常,2冻结)
    private Long lastActiveTime;// 最近活跃时间
    private String city;// 注册城市名称

}
