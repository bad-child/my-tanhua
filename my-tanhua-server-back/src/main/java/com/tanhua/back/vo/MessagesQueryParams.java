package com.tanhua.back.vo;

import com.tanhua.dubbo.server.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//消息翻页功能参数实体类
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessagesQueryParams {

    private Integer page;
    private Integer pagesize;
    private String id;//消息id
    private String sd;//开始时间
    private String ed;//结束时间
    private String sortProp;//排序字段
    private String sortOrder;//ascending 升序 descending 降序
    private Long uid;//用户ID
    private Integer state;//审核状态
}
