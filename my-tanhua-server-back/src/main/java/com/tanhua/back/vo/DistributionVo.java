package com.tanhua.back.vo;

import com.tanhua.dubbo.server.vo.Distribution;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DistributionVo {
    private List<Distribution> industryDistribution;//行业分布
    private List<Distribution> ageDistribution;//年龄分布
    private List<Distribution> genderDistribution;//性别分布
    private List<Distribution> localDistribution;//地区分布
    private List<Distribution> localTotal;//地区合计
}
