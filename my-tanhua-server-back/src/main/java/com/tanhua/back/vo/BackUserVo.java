package com.tanhua.back.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BackUserVo {

    private Integer id;// 用户id
    private String nickname;// 昵称
    private String mobile;// 手机号
    private String sex;// 性别
    private String personalSignature;// 个性签名
    private Integer age;// 年龄
    private Integer countBeLiked;// 呗喜欢人数
    private Integer countLiked;// 喜欢人数
    private Integer countMatching;// 配对人数
    private Integer income;// 收入
    private String occupation;// 职业
    private String userStatus;// 用户状态,1为正常,2为冻结
    private Long created;// 注册时间
    private String city;// 注册地区,城市
    private Long lastActiveTime;// 最近活跃时间
    private String lastLoginLocation;// 最近登录地
    private String logo;// 头像
    private String tags;// 用户标签


}
