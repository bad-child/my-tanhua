package com.tanhua.back.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 评论
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentVo {

    private String id; //评论id
    private String nickname; //昵称
    private Integer userId; //昵称
    private String content; //评论
    private Long createDate; //评论时间: 08:27

}