package com.tanhua.back.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TotalsVo {

    private String title;   //状态标题
    private String code;//状态代码
    private Integer value;//数量
}
