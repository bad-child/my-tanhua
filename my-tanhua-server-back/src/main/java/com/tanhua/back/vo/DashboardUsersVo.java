package com.tanhua.back.vo;

import com.tanhua.dubbo.server.vo.Distribution;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DashboardUsersVo {
    private List<Distribution> thisYear;
    private List<Distribution> lastYear;
}
