package com.tanhua.back.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VideoVo {

    private Integer id;
    private String nickname; //昵称
    private Long userId;
    private Long createDate;
    private String videoUrl; //视频URL
    private String picUrl; //封面
    private Integer reportCount;  //举报数
    private Integer likeCount; //点赞数量
    private Integer commentCount; //评论数量
    private Integer forwardingCount; //转发数量
}
