package com.tanhua.back.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/*
登录日志
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginLogsVo {

    private Integer id;  // 编号
    private Long logTime;  // 开始登录时间
    private String place;  // 地点
    private String equipment;  // 设备
}
