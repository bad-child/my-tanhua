package com.tanhua.back.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessagesVo {

    private String id;//编号
    private String nickname;//作者昵称
    private Integer userId;//作者ID
    private String userLogo;//作者头像
    private Long createDate;//发布日期
    private String text;//正文
    private String state;   //审核状态
    private Integer reportCount;//举报数
    private Integer likeCount;//点赞数
    private Integer commentCount;//评论数
    private Integer forwardingCount;//转发数
    private List<String> medias;//图片列表
    private Integer topState;   //顶置状态 1/2 未顶置/顶置

}
