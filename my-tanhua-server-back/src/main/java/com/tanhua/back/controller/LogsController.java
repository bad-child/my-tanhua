package com.tanhua.back.controller;

import com.tanhua.back.service.LogsService;
import com.tanhua.back.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("manage")
public class LogsController {

    @Autowired
    private LogsService logsService;

    /**
     * 日志管理 : 日志翻页
     *
     * @param token
     * @param page      // 页数
     * @param pageSize  // 页尺寸
     * @param sortProp  // 排序字段
     * @param sortOrder // ascending 升序 descending 降序
     * @param type      // 类型 01 用户登录
     * @param userId    // 用户ID
     * @return
     */
    @GetMapping("logs")
    public ResponseEntity queryLogsList(@RequestHeader("Authorization") String token,
                                        @RequestParam(value = "page", defaultValue = "1", required = false) Integer page,
                                        @RequestParam(value = "pagesize", defaultValue = "10", required = false) Integer pageSize,
                                        @RequestParam(value = "sortProp") String sortProp,
                                        @RequestParam(value = "sortOrder") String sortOrder,
                                        @RequestParam(value = "type",defaultValue = "1",required = false) String type,
                                        @RequestParam(value = "uid") String userId) {
        PageResult pageResult = logsService.queryLogsList(page, pageSize, sortProp, sortOrder, type, userId);
        return ResponseEntity.ok(pageResult);
    }
}
