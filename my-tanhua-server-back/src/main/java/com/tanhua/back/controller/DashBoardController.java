package com.tanhua.back.controller;

import com.tanhua.back.service.DashBoardService;
import com.tanhua.back.vo.DashboardUsersVo;
import com.tanhua.back.vo.DistributionVo;
import com.tanhua.back.vo.SummaryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("dashboard")
public class DashBoardController {
    @Autowired
    private DashBoardService dashBoardService;

    /**
     * 注册用户分布，行业top、年龄、性别、地区
     *
     * @param sd 开始时间
     * @param ed 结束时间
     * @return
     */
    @GetMapping("distribution")
    public ResponseEntity QueryDistribution(Long sd, Long ed) {
        DistributionVo distributionVo = dashBoardService.queryDistribution(sd, ed);
        return ResponseEntity.ok(distributionVo);
    }

    /**
     * 新增、活跃用户、次日留存率
     *
     * @param sd   开始时间
     * @param ed   结束时间
     * @param type 101 新增 102 活跃用户 103 次日留存率
     * @return
     */
    @GetMapping("users")
    public ResponseEntity queryUsers(Long sd, Long ed, Integer type) {
        DashboardUsersVo dashboardUsersVo = dashBoardService.queryNewUsers(sd, ed, type);
        return ResponseEntity.ok(dashboardUsersVo);
    }

    /**
     * 查询概要统计信息
     *
     * @return
     */
    @GetMapping("summary")
    public ResponseEntity querySummary() {
        SummaryVo summaryVo = dashBoardService.querySummary();
        return ResponseEntity.ok(summaryVo);
    }
}
