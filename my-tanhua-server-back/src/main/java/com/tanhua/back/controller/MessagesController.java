package com.tanhua.back.controller;

import com.tanhua.back.service.MessagesService;
import com.tanhua.back.vo.MessagesQueryParams;
import com.tanhua.back.vo.MessagesResult;
import com.tanhua.back.vo.MessagesVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("manage/messages")
public class MessagesController {

    @Autowired
    private MessagesService messagesService;    //从本地ioc容器中注值

    /**
     * 撤销消息
     *
     * @param idList 前端传来的id集合
     * @return
     */
    @PostMapping("revocation")
    public ResponseEntity revocation(@RequestBody List<String> idList) {

        //调用service,实现业务
        messagesService.revocation(idList);
        //响应给前端一个成功信息
        return ResponseEntity.ok("消息撤销成功");
    }

    /**
     * 消息翻页
     *
     * @param params 前端传过来的条件
     * @return
     */
    @GetMapping
    public ResponseEntity messages(MessagesQueryParams params) {

        //调用service,实现业务
        MessagesResult result = messagesService.queryMessages(params);
        //响应给前端
        return ResponseEntity.ok(result);
    }

    /**
     * 消息详情
     *
     * @param publishId
     * @return
     */
    @GetMapping("{id}")
    public ResponseEntity queryMessagesDetail(@PathVariable("id") String publishId) {
        ////调用service,实现业务
        MessagesVo messagesVo = messagesService.queryMessagesDetail(publishId);
        return ResponseEntity.ok(messagesVo);
    }

    //消息(动态审核)通过
    @PostMapping("pass")
    public ResponseEntity<String> messagesPass(@RequestBody List<String> ids) {
        //调用messagesPass方法
        messagesService.messagesPass(ids);
        return ResponseEntity.ok("动态审核通过！");

    }

    //消息(动态审核)拒绝
    @PostMapping("reject")
    public ResponseEntity<String> messagesReject(@RequestBody List<String> ids) {
        //messagesReject
        messagesService.messagesReject(ids);

        return ResponseEntity.ok("动态审核拒绝通过！");
    }

    //消息(动态审核)置顶
    @PostMapping("{id}/top")
    public ResponseEntity<String> messagesTop(@PathVariable("id") String id) {
        //调用messagesTop方法
        boolean result = messagesService.messagesTop(id);
        //判断消息是否置顶成功
        if (result) {
            return ResponseEntity.ok("消息已置顶！");
        }
        return ResponseEntity.ok("消息未置顶！");
    }

    //消息(动态审核)取消置顶
    @PostMapping("{id}/untop")
    public ResponseEntity<String> messagesUntop(@PathVariable("id") String id) {
        //调用messagesUntop方法
        boolean result = messagesService.messagesUntop(id);
        //判断消息是否取消置顶成功
        if (result) {
            return ResponseEntity.ok("消息已取消置顶！");
        }
        return ResponseEntity.ok("消息未取消置顶！");
    }
}
