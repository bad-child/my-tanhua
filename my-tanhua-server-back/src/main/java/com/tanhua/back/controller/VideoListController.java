package com.tanhua.back.controller;


import com.tanhua.back.service.CommentService;
import com.tanhua.back.service.VideoListService;
import com.tanhua.back.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("manage")
public class VideoListController {

    @Autowired
    private VideoListService videoListService;

    /**
     * 视频记录翻页
     * @param token
     * @param page
     * @param pageSize
     * @param sortProp
     * @param sortOrder
     * @param userId   用户ID
     * @return
     */
    @GetMapping("videos")
    public ResponseEntity queryVideoList(@RequestHeader("Authorization") String token,
                                           @RequestParam(value = "page", defaultValue = "1", required = false) Integer page,
                                           @RequestParam(value = "pagesize", defaultValue = "10", required = false) Integer pageSize,
                                           @RequestParam(value = "sortProp") String sortProp,
                                           @RequestParam(value = "sortOrder") String sortOrder,
                                           @RequestParam(value = "uid") String userId) {
        PageResult pageResult = videoListService.queryVideoList(page, pageSize, sortProp, sortOrder, userId);
        return ResponseEntity.ok(pageResult);
    }
}
