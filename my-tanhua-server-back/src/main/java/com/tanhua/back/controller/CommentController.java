package com.tanhua.back.controller;

import cn.hutool.core.util.ObjectUtil;
import com.tanhua.back.service.CommentService;
import com.tanhua.back.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("manage/messages")
public class CommentController {

    @Autowired
    private CommentService commentService;

    /**
     * 评论列表翻页
     *
     * @param token
     * @param page
     * @param pageSize
     * @param sortProp  排序字段
     * @param sortOrder ascending 升序 descending 降序
     * @param publishId 消息ID
     * @return
     */

    @GetMapping("comments")
    public ResponseEntity queryCommentList(@RequestHeader("Authorization") String token,
                                           @RequestParam(value = "page", defaultValue = "1", required = false) Integer page,
                                           @RequestParam(value = "pagesize", defaultValue = "10", required = false) Integer pageSize,
                                           @RequestParam(value = "sortProp") String sortProp,
                                           @RequestParam(value = "sortOrder") String sortOrder,
                                           @RequestParam(value = "messageID") String publishId) {

        PageResult pageResult = commentService.queryCommentListByPage(page, pageSize, sortProp, sortOrder, publishId);

        return ResponseEntity.ok(pageResult);
    }
}
