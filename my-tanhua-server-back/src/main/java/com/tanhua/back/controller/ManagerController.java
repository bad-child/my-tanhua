package com.tanhua.back.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.core.util.ObjectUtil;
import com.tanhua.back.service.ManagerService;
import com.tanhua.back.vo.ManagerVo;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.utils.NoAuthorization;
import com.tanhua.common.vo.ErrorResult;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.engine.Engine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.Duration;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("system/users")
public class ManagerController {
    @Autowired//注入manager对象
    private ManagerService managerService;
    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 管理者用户登录
     * @param param
     * @return
     */
    @PostMapping("login")
    @NoAuthorization
    public ResponseEntity<Object> login(@RequestBody Map<String,String> param,HttpServletResponse response) throws IOException {
        //调用业务层方法创建token
        Map map = managerService.login(param);
        //登录完成并且返回前端token
        return ResponseEntity.ok(map);
    }

    /**
     * 获取验证码图片
     * @return
     */
    @GetMapping("verification")
    @NoAuthorization
    public ResponseEntity getCodePic(@RequestParam String uuid, HttpServletResponse response) throws IOException {
        //调用业务层获取验证码图片
        BufferedImage image = managerService.getCodePic(uuid);
        ImageIO.write(image,"JPEG",response.getOutputStream());
        return ResponseEntity.ok("1");
    }


    /**
     * 获取管理员用户信息
     * @return
     */
    @PostMapping("profile")
    public ResponseEntity getAdminInfo(){
        //调用业务层方法获取管理员用户信息
        ManagerVo result=  managerService.getAdminInfo();
        return ResponseEntity.ok(result);

    }

    /**
     * 用户登出
     * @param token
     * @return
     */
    @NoAuthorization
    @PostMapping("logout")
    public ResponseEntity logout(@RequestHeader("Authorization") String token){
        //调用业务层方法获取管理员用户信息
        Boolean result =  managerService.logout(token);
        return ResponseEntity.ok(result);

    }






}
