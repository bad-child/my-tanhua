package com.tanhua.back.controller;


import cn.hutool.core.convert.Convert;
import com.tanhua.back.service.UserManageService;
import com.tanhua.back.vo.BackUserVo;
import com.tanhua.back.vo.PageResult;
import com.tanhua.dubbo.server.pojo.FrozenUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("manage")
public class UserManageController {

    @Autowired
    private UserManageService userManageService;

    /**
     * 用户基本资料
     *
     * @param userId
     * @return
     */
    @GetMapping("users/{userID}")
    public BackUserVo queryUserInfo(@PathVariable("userID") Long userId) {

        return userManageService.queryUserInfo(userId);

    }

    @PostMapping("users/freeze")
    public ResponseEntity<String> frozenOneUser(@RequestBody FrozenUser frozenUser) {

        String message = userManageService.frozenOneUser(frozenUser);
        return ResponseEntity.ok(message);

    }

    @PostMapping("users/unfreeze")
    public ResponseEntity<String> unFrozenOneUser(@RequestBody Map<String, Object> map) {

        Integer userId = Convert.toInt(map.get("userId"));
        //String reason = String.valueOf(map.get("reasonsForThawing"));
        String reason = String.valueOf(map.get("frozenRemarks"));


        String message = userManageService.unFrozenOneUser(userId, reason);
        return ResponseEntity.ok(message);

    }


    @GetMapping("users")
    public ResponseEntity<PageResult> queryUserByPage(@RequestParam("page") Integer page,
                                                      @RequestParam("pagesize") Integer pageSize,
                                                      @RequestParam("id") Long userId,
                                                      @RequestParam("nickname") String nickName,
                                                      @RequestParam("city") String city) {

        PageResult pageResult = userManageService.queryUserByPage(page, pageSize, userId, nickName, city);
        return ResponseEntity.ok(pageResult);
    }


}
