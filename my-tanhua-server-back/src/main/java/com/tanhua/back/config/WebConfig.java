package com.tanhua.back.config;

import com.tanhua.back.interceptor.ManagerTokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    private ManagerTokenInterceptor managerTokenInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //统一token验证过滤器
        registry.addInterceptor(managerTokenInterceptor).addPathPatterns("/**").excludePathPatterns("/error");

    }
}
