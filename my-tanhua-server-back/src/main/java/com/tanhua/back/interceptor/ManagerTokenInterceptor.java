package com.tanhua.back.interceptor;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.tanhua.back.service.ManagerService;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.pojo.Manager;
import com.tanhua.common.utils.ManagerThreadLocal;
import com.tanhua.common.utils.NoAuthorization;
import com.tanhua.common.vo.ErrorResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.annotations.CompletionField;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Configuration
public class ManagerTokenInterceptor implements HandlerInterceptor {
    @Autowired
    private ManagerService managerService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //校验是否为HandlerMethod对象
        if(!(handler instanceof HandlerMethod)){
            return true;
        }
        //直接放行包含NoAuthorization注解的方法
        if(((HandlerMethod) handler).hasMethodAnnotation(NoAuthorization.class)){
            return  true;
        }
        //从请求头中获取token
        String token = request.getHeader("Authorization");
        //校验token是否为空
        if(StrUtil.isNotEmpty(token)){
            //解析token
            Manager manager = managerService.queryAdminToken(token);
            //token有效则存入本地线程
            if(ObjectUtil.isNotEmpty(manager)){
                ManagerThreadLocal.set(manager);
                return true;
            }
        }
        //token无效则响应状态401
        //throw new BusinessException(ErrorResult.checkCodeError());
        response.setStatus(401);
        return false;

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //从本地线程中移除user对象
        ManagerThreadLocal.remove();

    }
}
