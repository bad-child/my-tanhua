package com.tanhua.back.service;


import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tanhua.back.vo.BackUserVo;
import com.tanhua.back.vo.PageResult;
import com.tanhua.back.vo.PageUserInfoVo;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.mapper.UserMapper;
import com.tanhua.common.pojo.User;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.dubbo.server.api.DashBoardApi;
import com.tanhua.dubbo.server.api.FrozenUserApi;
import com.tanhua.dubbo.server.api.UserLikeApi;
import com.tanhua.dubbo.server.pojo.FrozenUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserManageService {

    @Autowired
    private UserInfoService userInfoService;


    @Reference
    private UserLikeApi userLikeApi;

    @Autowired
    private UserMapper userMapper;

    @Reference
    private FrozenUserApi frozenUserApi;

    @Reference
    private DashBoardApi dashBoardApi;

    /**
     * 用户基本资料
     *
     * @param userId
     * @return
     */
    public BackUserVo queryUserInfo(Long userId) {
        BackUserVo backUserVo = new BackUserVo();

        // 获取 user Info 信息
        UserInfo userInfo = userInfoService.queryByUserId(userId);
        fillBackUserVo(backUserVo, userInfo);

        // 获取 user信息
        User user = userMapper.selectById(userId);

        // 封装
        backUserVo.setMobile(user.getMobile());


        Long lastActiveTime = dashBoardApi.getLastActiveTime(userId);

        if (ObjectUtil.isEmpty(lastActiveTime)) {
            backUserVo.setLastActiveTime(user.getUpdated().getTime());
        } else {
            backUserVo.setLastActiveTime(lastActiveTime);
        }


        return backUserVo;

    }

    /**
     * 封装对象
     *
     * @param backUserVo
     * @param userInfo
     */
    public void fillBackUserVo(BackUserVo backUserVo, UserInfo userInfo) {
        Long userId = userInfo.getUserId();
        backUserVo.setId(Convert.toInt(userId));
        backUserVo.setNickname(userInfo.getNickName());
        // 手机号获取(user封装)
        backUserVo.setSex(userInfo.getSex().toString());
        //TODO 个性签名
        backUserVo.setPersonalSignature("我就是我,good boy");
        backUserVo.setAge(userInfo.getAge());
        backUserVo.setCountBeLiked(userLikeApi.queryBeLikedCount(userId));// 被喜欢人数
        backUserVo.setCountLiked(userLikeApi.queryLikedCount(userId));// 喜欢人数
        backUserVo.setCountMatching(userLikeApi.queryMatchingCount(userId));// 配对人数(互相喜欢)
        backUserVo.setIncome(Convert.toInt(userInfo.getIncome()));
        backUserVo.setOccupation(userInfo.getIndustry());

        String status = frozenUserApi.getStatus(userId);
        backUserVo.setUserStatus(StrUtil.isEmpty(status) ? "1" : "2");// 用户状态
        backUserVo.setCreated(userInfo.getCreated().getTime());
        backUserVo.setCity(userInfo.getCity());
        // 最近活跃时间(user封装)
        backUserVo.setLastLoginLocation("安徽合肥");//TODO 最近登录地
        backUserVo.setLogo(userInfo.getLogo());
        backUserVo.setTags(userInfo.getTags());

    }

    /**
     * 冻结一个用户
     *
     * @param frozenUser 被冻结的用户
     * @return
     */
    public String frozenOneUser(FrozenUser frozenUser) {
        Boolean result = frozenUserApi.frozenOneUser(frozenUser);

        if (result) {
            return "操作成功";
        } else {
            return "操作失败";
        }

    }


    /**
     * 解冻一个用户
     *
     * @param userId 解冻的用户 id
     * @param reason 解冻的原因
     * @return
     */
    public String unFrozenOneUser(Integer userId, String reason) {
        Boolean result = frozenUserApi.unFrozenOneUser(userId, reason);

        if (result) {
            return "操作成功";
        } else {
            return "操作失败";
        }
    }


    /**
     * 分页分条件查询用户信息
     *
     * @param page     当前页
     * @param pageSize 每页显示条数
     * @param userId   用户 id
     * @param nickName 昵称
     * @param city     城市
     * @return
     */
    public PageResult queryUserByPage(Integer page, Integer pageSize, Long userId, String nickName, String city) {
        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPagesize(pageSize);


        // 获取 userInfo 集合
        IPage<UserInfo> iPage = userInfoService.multiQueryUser(page, pageSize, userId, nickName, city);

        // 封装总数与页数
        pageResult.setCounts(Convert.toInt(iPage.getTotal()));
        pageResult.setPages(Convert.toInt(iPage.getPages()));

        List<UserInfo> userInfoList = iPage.getRecords();


        List<PageUserInfoVo> list = new ArrayList<>();

        // 封装
        for (UserInfo userInfo : userInfoList) {
            PageUserInfoVo pageUserInfoVo = new PageUserInfoVo();
            pageUserInfoVo.setId(Convert.toInt(userInfo.getUserId()));
            pageUserInfoVo.setLogo(userInfo.getLogo());
            pageUserInfoVo.setNickname(userInfo.getNickName());

            // 获取 手机号
            LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(User::getId, userInfo.getUserId());
            User user = userMapper.selectOne(wrapper);

            pageUserInfoVo.setMobile(user.getMobile());//手机号

            pageUserInfoVo.setSex(userInfo.getSex().toString());
            pageUserInfoVo.setAge(userInfo.getAge());
            pageUserInfoVo.setOccupation(userInfo.getIndustry());

            // 获取用户状态
            String status = frozenUserApi.getStatus(userInfo.getUserId());
            pageUserInfoVo.setUserStatus(StrUtil.isEmpty(status) ? "1" : "2");// 用户状态(1正常,2冻结)

            //TODO 最近活跃时间
            //pageUserInfoVo.setLastActiveTime(user.getUpdated().getTime());
            Long lastActiveTime = dashBoardApi.getLastActiveTime(userInfo.getUserId());
            if (ObjectUtil.isEmpty(lastActiveTime)) {
                pageUserInfoVo.setLastActiveTime(user.getUpdated().getTime());
            } else {
                pageUserInfoVo.setLastActiveTime(lastActiveTime);
            }

            pageUserInfoVo.setCity(userInfo.getCity());

            list.add(pageUserInfoVo);
        }


        pageResult.setItems(list);

        return pageResult;
    }
}
