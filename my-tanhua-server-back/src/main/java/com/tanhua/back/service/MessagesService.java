package com.tanhua.back.service;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.back.vo.MessagesQueryParams;
import com.tanhua.back.vo.MessagesResult;
import com.tanhua.back.vo.MessagesVo;
import com.tanhua.back.vo.TotalsVo;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.common.vo.ErrorResult;
import com.tanhua.dubbo.server.api.MessagesApi;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.pojo.Publish;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tanhua.dubbo.server.enums.StatusEnum;

import java.util.*;

@Service
public class MessagesService {

    @Reference
    private MessagesApi messagesApi;

    @Autowired
    private UserInfoService userInfoService;

    @Reference
    private QuanZiApi quanZiApi;

    /**
     * 消息撤销
     *
     * @param idList 前端传来的id集合
     */
    public void revocation(List<String> idList) {

        //解析token

        //调用api服务,执行mongodb删除动态
        Boolean flag = messagesApi.updatePublish(idList);
        //判断删除操作是否成功
        if (!flag) {
            throw new BusinessException(ErrorResult.deletePublishFail());
        }
    }

    /**
     * 消息翻页
     *
     * @param params
     * @return
     */
    public MessagesResult queryMessages(MessagesQueryParams params) {
        //解析token

        //获取动态数据
        List<Publish> publishList = messagesApi.queryPublishList(params.getPage(), params.getPagesize(),
                params.getEd(), params.getId(), params.getSd(),
                params.getSortOrder(), params.getState(), params.getUid(), params.getSortProp());

        //判断集合是否为空
       /* if (CollUtil.isEmpty(publishList)) {
            throw new BusinessException(ErrorResult.queryPublishListFail());
        }*/

        //List<Object> userIds = CollUtil.getFieldValues(publishList, "userId");
        //封装数据
        List<MessagesVo> messagesVoList = fillMessagesVo(publishList, params.getSortProp(), params.getSortOrder());
        List<TotalsVo> totalsVoList = fillTotalsVo(params.getUid(), params.getEd(), params.getId(), params.getSd());

        //封装MessagesResult对象
        MessagesResult result = new MessagesResult();

        //当前端传来的状态不为空时,查询全部
        if (ObjectUtil.isEmpty(params.getState())) {
            result.setCounts(messagesApi.queryStatusCount(StatusEnum.getStatus(0), params.getUid(), params.getEd(), params.getId(), params.getSd()));
        } else {
            //不为0时,根据状态查询数量
            result.setCounts(messagesApi.queryStatusCount(StatusEnum.getStatus(params.getState()), params.getUid(), params.getEd(), params.getId(), params.getSd()));
        }
        //每页显示条数
        result.setPagesize(params.getPagesize());
        //列表
        result.setItems(messagesVoList);
        //状态合计
        result.setTotals(totalsVoList);

        return result;

    }

    /**
     * 消息详情
     *
     * @param publishId
     * @return
     */
    public MessagesVo queryMessagesDetail(String publishId) {
        //解析token

        //获取动态数据
        Publish publish = messagesApi.queryPublish(publishId);

        //当查询到的对象不为空时
        if (ObjectUtil.isEmpty(publish)) {
            throw new BusinessException(ErrorResult.queryPublishFail());
        }

        //分装数据
        MessagesVo messagesVo = fillMessagesVo(Arrays.asList(publish), null, null).get(0);

        return messagesVo;
    }

    /**
     * 封装MessagesVo集合
     *
     * @param publishList
     * @return
     */
    private List<MessagesVo> fillMessagesVo(List<Publish> publishList, String sortProp, String sortOrder) {

        List<MessagesVo> messagesVoList = new ArrayList<>();
        //遍历集合分装数据
        for (Publish publish : publishList) {
            MessagesVo messagesVo = new MessagesVo();
            messagesVo.setId(publish.getId().toHexString());    //动态id
            messagesVo.setUserId(Convert.toInt(publish.getUserId()));   //用户id
            messagesVo.setMedias(publish.getMedias());  //图片内容
            messagesVo.setState(String.valueOf(publish.getStatus()));    //状态
            messagesVo.setText(publish.getText());  //文字内容
            messagesVo.setCreateDate(publish.getCreated());  //发布时间
            messagesVo.setTopState(publish.getTop());   //置顶

            messagesVo.setCommentCount(Convert.toInt(quanZiApi.queryCommentCount(String.valueOf(publish.getId()), CommentType.COMMENT)));  //  评论数
            messagesVo.setForwardingCount(Convert.toInt(quanZiApi.queryCommentCount(String.valueOf(publish.getId()), CommentType.FORWARD)));    //转发数
            messagesVo.setLikeCount(Convert.toInt(quanZiApi.queryCommentCount(String.valueOf(publish.getId()), CommentType.LIKE)));         //  喜欢数
            messagesVo.setReportCount(Convert.toInt(quanZiApi.queryCommentCount(String.valueOf(publish.getId()), CommentType.REPORT)));// 举报数

            //获取发布动态这的用户信息
            UserInfo userInfo = userInfoService.queryByUserId(publish.getUserId());
            if (ObjectUtil.isNotEmpty(userInfo)) {
                messagesVo.setNickname(userInfo.getNickName()); //用户昵称
                messagesVo.setUserLogo(userInfo.getLogo()); //用户头像
                messagesVoList.add(messagesVo); //往集合中填充对象
            }
        }

        if (StrUtil.isNotEmpty(sortProp) && StrUtil.isNotEmpty(sortOrder)) {
            //按时间排序
            sortByCreateDate(sortProp, sortOrder, messagesVoList);
            //按举报数排序
            sortByReportCount(sortProp, sortOrder, messagesVoList);
            //按转发数排序
            sortByForwardingCount(sortProp, sortOrder, messagesVoList);
            //按评论数排序
            sortByCommentCount(sortProp, sortOrder, messagesVoList);
            //按喜欢数排序
            sortByLikeCount(sortProp, sortOrder, messagesVoList);
        }
        return messagesVoList;
    }

    /**
     * 按时间排序
     *
     * @param sortProp
     * @param sortOrder
     * @param messagesVoList
     */
    private void sortByCreateDate(String sortProp, String sortOrder, List<MessagesVo> messagesVoList) {
        if (StrUtil.equals("createDate", sortProp)) {
            if (StrUtil.equals("ascending", sortOrder)) {
                //升序
                Collections.sort(messagesVoList, new Comparator<MessagesVo>() {
                    @Override
                    public int compare(MessagesVo o1, MessagesVo o2) {
                        int result = Convert.toInt(o1.getCreateDate() - o2.getCreateDate());
                        return result;
                    }
                });
            } else if (StrUtil.equals("descending", sortOrder)) {
                //降序
                Collections.sort(messagesVoList, new Comparator<MessagesVo>() {
                    @Override
                    public int compare(MessagesVo o1, MessagesVo o2) {
                        int result = Convert.toInt(o2.getCreateDate() - o1.getCreateDate());
                        return result;
                    }
                });
            }
        }
    }

    /**
     * 按点赞数排序
     *
     * @param sortProp
     * @param sortOrder
     * @param messagesVoList
     */
    private void sortByLikeCount(String sortProp, String sortOrder, List<MessagesVo> messagesVoList) {
        if (StrUtil.equals("likeCount", sortProp)) {
            if (StrUtil.equals("ascending", sortOrder)) {
                //升序
                Collections.sort(messagesVoList, new Comparator<MessagesVo>() {
                    @Override
                    public int compare(MessagesVo o1, MessagesVo o2) {
                        int result = o1.getLikeCount() - o2.getLikeCount();
                        return result;
                    }
                });
            } else if (StrUtil.equals("descending", sortOrder)) {
                //降序
                Collections.sort(messagesVoList, new Comparator<MessagesVo>() {
                    @Override
                    public int compare(MessagesVo o1, MessagesVo o2) {
                        int result = o2.getLikeCount() - o1.getLikeCount();
                        return result;
                    }
                });
            }
        }
    }

    /**
     * 按评论数排序
     *
     * @param sortProp
     * @param sortOrder
     * @param messagesVoList
     */
    private void sortByCommentCount(String sortProp, String sortOrder, List<MessagesVo> messagesVoList) {
        if (StrUtil.equals("commentCount", sortProp)) {
            if (StrUtil.equals("ascending", sortOrder)) {
                //升序
                Collections.sort(messagesVoList, new Comparator<MessagesVo>() {
                    @Override
                    public int compare(MessagesVo o1, MessagesVo o2) {
                        int result = o1.getCommentCount() - o2.getCommentCount();
                        return result;
                    }
                });
            } else if (StrUtil.equals("descending", sortOrder)) {
                //降序
                Collections.sort(messagesVoList, new Comparator<MessagesVo>() {
                    @Override
                    public int compare(MessagesVo o1, MessagesVo o2) {
                        int result = o2.getCommentCount() - o1.getCommentCount();
                        return result;
                    }
                });
            }
        }
    }

    /**
     * 按转发数排序
     *
     * @param sortProp
     * @param sortOrder
     * @param messagesVoList
     */
    private void sortByForwardingCount(String sortProp, String sortOrder, List<MessagesVo> messagesVoList) {
        if (StrUtil.equals("forwardingCount", sortProp)) {
            if (StrUtil.equals("ascending", sortOrder)) {
                //升序
                Collections.sort(messagesVoList, new Comparator<MessagesVo>() {
                    @Override
                    public int compare(MessagesVo o1, MessagesVo o2) {
                        int result = o1.getForwardingCount() - o2.getForwardingCount();
                        return result;
                    }
                });
            } else if (StrUtil.equals("descending", sortOrder)) {
                //降序
                Collections.sort(messagesVoList, new Comparator<MessagesVo>() {
                    @Override
                    public int compare(MessagesVo o1, MessagesVo o2) {
                        int result = o2.getForwardingCount() - o1.getForwardingCount();
                        return result;
                    }
                });
            }
        }
    }

    /**
     * 按举报数排序
     *
     * @param sortProp
     * @param sortOrder
     * @param messagesVoList
     */
    private void sortByReportCount(String sortProp, String sortOrder, List<MessagesVo> messagesVoList) {
        if (StrUtil.equals("reportCount", sortProp)) {
            if (StrUtil.equals("ascending", sortOrder)) {
                //升序
                Collections.sort(messagesVoList, new Comparator<MessagesVo>() {
                    @Override
                    public int compare(MessagesVo o1, MessagesVo o2) {
                        int result = o1.getReportCount() - o2.getReportCount();
                        return result;
                    }
                });
            } else if (StrUtil.equals("descending", sortOrder)) {
                //降序
                Collections.sort(messagesVoList, new Comparator<MessagesVo>() {
                    @Override
                    public int compare(MessagesVo o1, MessagesVo o2) {
                        int result = o2.getReportCount() - o1.getReportCount();
                        return result;
                    }
                });
            }
        }
    }

    /**
     * 封装TotalsVo集合
     *
     * @return
     */
    private List<TotalsVo> fillTotalsVo(Long uid, String ed, String id, String sd) {
        List<TotalsVo> totalsVoList = new ArrayList<>();

        TotalsVo totalsVo0 = new TotalsVo();
        totalsVo0.setTitle(StatusEnum.getDescByValue(0));
        totalsVo0.setCode(String.valueOf(StatusEnum.ALL.getValue()));
        totalsVo0.setValue(messagesApi.queryStatusCount(StatusEnum.ALL, uid, ed, id, sd));
        totalsVoList.add(totalsVo0);

        TotalsVo totalsVo1 = new TotalsVo();
        totalsVo1.setTitle(StatusEnum.getDescByValue(3));
        totalsVo1.setCode(String.valueOf(StatusEnum.DSH.getValue()));
        totalsVo1.setValue(messagesApi.queryStatusCount(StatusEnum.DSH, uid, ed, id, sd));
        totalsVoList.add(totalsVo1);

        TotalsVo totalsVo2 = new TotalsVo();
        totalsVo2.setTitle(StatusEnum.getDescByValue(5));
        totalsVo2.setCode(String.valueOf(StatusEnum.YTG.getValue()));
        totalsVo2.setValue(messagesApi.queryStatusCount(StatusEnum.YTG, uid, ed, id, sd));
        totalsVoList.add(totalsVo2);


        TotalsVo totalsV3 = new TotalsVo();
        totalsV3.setTitle(StatusEnum.getDescByValue(4));
        totalsV3.setCode(String.valueOf(StatusEnum.YJJ.getValue()));
        totalsV3.setValue(messagesApi.queryStatusCount(StatusEnum.YJJ, uid, ed, id, sd));
        totalsVoList.add(totalsV3);

        return totalsVoList;
    }

    //消息通知(动态审核通过)
    public void messagesPass(List<String> ids) {
        //调用 messagespass 方法
        Boolean result = messagesApi.messagesPass(ids);


        if (!result) {
            throw new BusinessException(ErrorResult.publishPassFail());
        }

    }

    //消息拒绝(动态审核拒绝)
    public void messagesReject(List<String> ids) {
        //调用messagesReject方法
        Boolean result = messagesApi.messagesReject(ids);

        if (!result) {
            throw new BusinessException(ErrorResult.publishRejectFail());
        }

    }

    //消息置顶
    public boolean messagesTop(String id) {
        //调用messagesTop方法
        Boolean result = messagesApi.messagesTop(id);
        //判断消息是否置顶成功
        if (result) {
            return true;
        }
        return false;
    }

    //消息取消置顶
    public boolean messagesUntop(String id) {
        //调用messagesUntop方法
        Boolean result = messagesApi.messagesUntop(id);
        //判断消息是否取消置顶成功
        if (result) {
            return true;
        }
        return false;
    }

}
