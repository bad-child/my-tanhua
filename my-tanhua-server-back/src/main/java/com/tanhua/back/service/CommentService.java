package com.tanhua.back.service;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.back.vo.CommentVo;
import com.tanhua.back.vo.PageResult;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.dubbo.server.api.CommentApi;
import com.tanhua.dubbo.server.pojo.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CommentService {

    @Reference
    private CommentApi commentApi;

    @Autowired
    private UserInfoMapper userInfoMapper;

    /**
     * 评论列表翻页
     *
     * @param page
     * @param pageSize
     * @param sortProp
     * @param sortOrder
     * @param publishId
     * @return
     */
    public PageResult queryCommentListByPage(Integer page, Integer pageSize, String sortProp, String sortOrder, String publishId) {

        PageResult pageResult = new PageResult();
        pageResult.setPages(page);
        pageResult.setPagesize(pageSize);
        pageResult.setCounts(commentApi.queryAllCommentList(publishId).size());


        // 根据动态id,获取该动态的评论集合
        List<Comment> commentList = commentApi.queryCommentListByPage(page,pageSize,sortProp,sortOrder, publishId);

        if (CollUtil.isEmpty(commentList)) {
            pageResult.setPages(0);
            pageResult.setItems(Collections.EMPTY_LIST);
            return pageResult;
        }

        // 创建Vo对象的集合,对Vo对象进行封装
        List<CommentVo> commentVoList = new ArrayList<>();

        for (Comment comment : commentList) {

            //  根据用户id获取他的userInfo信息
            LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(UserInfo::getUserId, comment.getUserId());
            UserInfo userInfo = userInfoMapper.selectOne(wrapper);

            if (ObjectUtil.isNotEmpty(userInfo)) {
                CommentVo commentVo = new CommentVo();

                commentVo.setId(String.valueOf(comment.getId()));
                commentVo.setNickname(userInfo.getNickName());
                commentVo.setUserId(Convert.toInt(comment.getUserId()));
                commentVo.setContent(comment.getContent());
                commentVo.setCreateDate(comment.getCreated());

                commentVoList.add(commentVo);
            }
        }

        // 结果对象属性赋值
        //pageResult.setPagesize(pageSize);
        //pageResult.setPages(1);

        pageResult.setItems(commentVoList);
        return pageResult;
    }
}
