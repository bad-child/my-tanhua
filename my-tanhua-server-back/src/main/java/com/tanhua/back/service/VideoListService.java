package com.tanhua.back.service;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.back.vo.MessagesVo;
import com.tanhua.back.vo.PageResult;
import com.tanhua.back.vo.VideoVo;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.api.VideoListApi;
import com.tanhua.dubbo.server.enums.CommentType;
import com.tanhua.dubbo.server.pojo.Video;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class VideoListService {

    @Reference
    private QuanZiApi quanZiApi;

    @Reference
    private VideoListApi videoListApi;

    @Autowired
    private UserInfoMapper userInfoMapper;


    /**
     * 视频记录翻页
     *
     * @param page
     * @param pageSize
     * @param sortProp  排序字段
     * @param sortOrder ascending 升序 descending 降序
     * @param userId
     * @return
     */
    public PageResult queryVideoList(Integer page, Integer pageSize, String sortProp, String sortOrder, String userId) {


        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPagesize(pageSize);

        // 获取数据库指定用户id发布的视频对象集合
        List<Video> videoList = videoListApi.queryVideoList(page, pageSize, userId);

        // 创建Vo对象的集合,进行封装
        List<VideoVo> videoVoList = new ArrayList<>();

        for (Video video : videoList) {
            LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(UserInfo::getUserId, video.getUserId());
            UserInfo userInfo = userInfoMapper.selectOne(wrapper);

            if (ObjectUtil.isNotEmpty(userInfo)) {
                VideoVo videoVo = new VideoVo();

                videoVo.setId(Convert.toInt(video.getId()));  //编号
                videoVo.setNickname(userInfo.getNickName());  // 发布人
                videoVo.setUserId(video.getUserId());  // 视频发布者id
                videoVo.setCreateDate(video.getCreated());
                videoVo.setVideoUrl(video.getVideoUrl());  // 视频路径
                videoVo.setPicUrl(video.getPicUrl());  // 封面路径
                videoVo.setReportCount(Convert.toInt(videoListApi.queryCommentCount(video.getId().toHexString(), CommentType.REPORT)));  // 举报数
                videoVo.setLikeCount(Convert.toInt(videoListApi.queryCommentCount(video.getId().toHexString(), CommentType.LIKE)));  // 点赞数
                videoVo.setCommentCount(Convert.toInt(videoListApi.queryCommentCount(video.getId().toHexString(), CommentType.COMMENT)));  // 评论数
                videoVo.setForwardingCount(Convert.toInt(videoListApi.queryCommentCount(video.getId().toHexString(), CommentType.FORWARD)));  // 转发数

                videoVoList.add(videoVo);
            }
        }

        // 排序字段不为空,
        if (StrUtil.isNotEmpty(sortProp) && StrUtil.isNotEmpty(sortOrder)) {
            //按时间排序
            sortByCreateDate(sortProp, sortOrder, videoVoList);
            //按举报数排序
            sortByReportCount(sortProp, sortOrder, videoVoList);
            //按转发数排序
            sortByForwardingCount(sortProp, sortOrder, videoVoList);
            //按评论数排序
            sortByCommentCount(sortProp, sortOrder, videoVoList);
            //按喜欢数排序
            sortByLikeCount(sortProp, sortOrder, videoVoList);
        }


        // 结果对象赋值,返回
        pageResult.setItems(videoVoList);
        pageResult.setCounts(videoListApi.queryAllVideo(userId).size());
        pageResult.setPages((videoListApi.queryAllVideo(userId).size() / pageSize) + 1);

        return pageResult;
    }

    /**
     * 按时间排序
     *
     * @param sortProp
     * @param sortOrder
     * @param videoVoList
     */
    private void sortByCreateDate(String sortProp, String sortOrder, List<VideoVo> videoVoList) {
        if (StrUtil.equals("createDate", sortProp)) {
            if (StrUtil.equals("ascending", sortOrder)) {
                //升序
                Collections.sort(videoVoList, new Comparator<VideoVo>() {
                    @Override
                    public int compare(VideoVo o1, VideoVo o2) {
                        int result = Convert.toInt(o1.getCreateDate() - o2.getCreateDate());
                        return result;
                    }
                });
            } else if (StrUtil.equals("descending", sortOrder)) {
                //降序
                Collections.sort(videoVoList, new Comparator<VideoVo>() {
                    @Override
                    public int compare(VideoVo o1, VideoVo o2) {
                        int result = Convert.toInt(o2.getCreateDate() - o1.getCreateDate());
                        return result;
                    }
                });
            }
        }
    }

    /**
     * 按点赞数排序
     *
     * @param sortProp
     * @param sortOrder
     * @param videoVoList
     */
    private void sortByLikeCount(String sortProp, String sortOrder, List<VideoVo> videoVoList) {
        if (StrUtil.equals("likeCount", sortProp)) {
            if (StrUtil.equals("ascending", sortOrder)) {
                //升序
                Collections.sort(videoVoList, new Comparator<VideoVo>() {
                    @Override
                    public int compare(VideoVo o1, VideoVo o2) {
                        int result = o1.getLikeCount() - o2.getLikeCount();
                        return result;
                    }
                });
            } else if (StrUtil.equals("descending", sortOrder)) {
                //降序
                Collections.sort(videoVoList, new Comparator<VideoVo>() {
                    @Override
                    public int compare(VideoVo o1, VideoVo o2) {
                        int result = o2.getLikeCount() - o1.getLikeCount();
                        return result;
                    }
                });
            }
        }
    }

    /**
     * 按评论数排序
     *
     * @param sortProp
     * @param sortOrder
     * @param videoVoList
     */
    private void sortByCommentCount(String sortProp, String sortOrder, List<VideoVo> videoVoList) {
        if (StrUtil.equals("commentCount", sortProp)) {
            if (StrUtil.equals("ascending", sortOrder)) {
                //升序
                Collections.sort(videoVoList, new Comparator<VideoVo>() {
                    @Override
                    public int compare(VideoVo o1, VideoVo o2) {
                        int result = o1.getCommentCount() - o2.getCommentCount();
                        return result;
                    }
                });
            } else if (StrUtil.equals("descending", sortOrder)) {
                //降序
                Collections.sort(videoVoList, new Comparator<VideoVo>() {
                    @Override
                    public int compare(VideoVo o1, VideoVo o2) {
                        int result = o2.getCommentCount() - o1.getCommentCount();
                        return result;
                    }
                });
            }
        }
    }

    /**
     * 按转发数排序
     *
     * @param sortProp
     * @param sortOrder
     * @param videoVoList
     */
    private void sortByForwardingCount(String sortProp, String sortOrder, List<VideoVo> videoVoList) {
        if (StrUtil.equals("forwardingCount", sortProp)) {
            if (StrUtil.equals("ascending", sortOrder)) {
                //升序
                Collections.sort(videoVoList, new Comparator<VideoVo>() {
                    @Override
                    public int compare(VideoVo o1, VideoVo o2) {
                        int result = o1.getForwardingCount() - o2.getForwardingCount();
                        return result;
                    }
                });
            } else if (StrUtil.equals("descending", sortOrder)) {
                //降序
                Collections.sort(videoVoList, new Comparator<VideoVo>() {
                    @Override
                    public int compare(VideoVo o1, VideoVo o2) {
                        int result = o2.getForwardingCount() - o1.getForwardingCount();
                        return result;
                    }
                });
            }
        }
    }

    /**
     * 按举报数排序
     *
     * @param sortProp
     * @param sortOrder
     * @param videoVoList
     */
    private void sortByReportCount(String sortProp, String sortOrder, List<VideoVo> videoVoList) {
        if (StrUtil.equals("reportCount", sortProp)) {
            if (StrUtil.equals("ascending", sortOrder)) {
                //升序
                Collections.sort(videoVoList, new Comparator<VideoVo>() {
                    @Override
                    public int compare(VideoVo o1, VideoVo o2) {
                        int result = o1.getReportCount() - o2.getReportCount();
                        return result;
                    }
                });
            } else if (StrUtil.equals("descending", sortOrder)) {
                //降序
                Collections.sort(videoVoList, new Comparator<VideoVo>() {
                    @Override
                    public int compare(VideoVo o1, VideoVo o2) {
                        int result = o2.getReportCount() - o1.getReportCount();
                        return result;
                    }
                });
            }
        }
    }

}
