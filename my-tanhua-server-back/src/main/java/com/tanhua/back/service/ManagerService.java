package com.tanhua.back.service;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.back.vo.ManagerVo;
import com.tanhua.common.exception.BusinessException;
import com.tanhua.common.mapper.ManagerMapper;
import com.tanhua.common.pojo.Manager;
import com.tanhua.common.utils.ManagerThreadLocal;
import com.tanhua.common.vo.ErrorResult;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ManagerService {
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Autowired
    private ManagerMapper managerMapper;

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${tanhua.sso.url}")
    private String ssoUrl;



    /**
     * 管理用户登录,并且返回token
     * @param param
     * @return
     */
    public Map login(Map<String, String> param) {
        //1.将map中的数据提取出来
        String username = param.get("username");
        String password = param.get("password");
        String verificationCode = param.get("verificationCode");
        String uuid = param.get("uuid");

        //2.获取redis中的验证码
        String redisCode = redisTemplate.opsForValue().get(uuid);
        if(StrUtil.isEmpty(redisCode)){
            throw  new BusinessException(ErrorResult.codeExpired());
        }


        //3.判断redis中的验证码和传进来的验证码进行比较
        if(!verificationCode.equals(redisCode)){
            //3.1如果比对失败,抛出异常
            throw  new BusinessException(ErrorResult.checkCodeError());
        }
        //清空redis中的验证码
        redisTemplate.delete(uuid);
        //判断用户名和密码是否为空
        if(StrUtil.isAllEmpty(username,password)){
            throw  new BusinessException(ErrorResult.AdminInfoFail());
        }
        //成功
        //4.查询数据库,验证当前用户和密码是否正确
        LambdaQueryWrapper<Manager> wrapper =new LambdaQueryWrapper<>();
        wrapper.eq(Manager::getUsername,username).eq(Manager::getPassword,password);
        Manager manager = managerMapper.selectOne(wrapper);
        //判断用户名称密码是否正确
        if(ObjectUtil.isEmpty(manager)){
            //抛出用户名密码错误异常
            throw  new BusinessException(ErrorResult.loginFail());
        }
        //生成token加密
        Map<String, Object> claims = new HashMap<String, Object>();
        claims.put("username", manager.getUsername());
        claims.put("id", manager.getId());

        // 生成token
        String token = Jwts.builder()
                //.setHeader(header) header，可省略
                .setClaims(claims) //payload，存放数据的位置，不能放置敏感数据，如：密码等
                .signWith(SignatureAlgorithm.HS256, secret) //设置加密方法和加密盐
                .setExpiration(new DateTime().plusHours(12).toDate()) //设置过期时间，12小时后过期
                .compact();
        //创建返回对象map并且填充数据返回
        Map<String,String> map = new HashMap<>();
        redisTemplate.opsForValue().set(token,"555");
        map.put("token",token);
        return map;
    }


    /**
     * 获取验证码图片
     * @param uuid
     */
    public BufferedImage getCodePic(String uuid) {
        //hutool工具类获取验证码图片,并且将其加载到内存中
        CircleCaptcha circleCaptcha = CaptchaUtil.createCircleCaptcha(200, 100, 4, 1);
        BufferedImage image = circleCaptcha.getImage();
        //将验证码存入redis中,key为uuid,有效时间为5分钟
        redisTemplate.opsForValue().set(uuid,circleCaptcha.getCode(), Duration.ofMinutes(5));
        //判断获取得到的图片是否为空
        if(ObjectUtil.isEmpty(image)){
            throw new  BusinessException(ErrorResult.getCodePicFail());
        }
        return image;
    }

    /**
     * 获取管理员用户信息
     * @return
     */
    public ManagerVo getAdminInfo() {
        //解析token获取
        Manager manager = ManagerThreadLocal.get();
        //设置查询条件.根据用户id查询用户信息
        LambdaQueryWrapper<Manager> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Manager::getId,manager.getId());
        Manager manager1 = managerMapper.selectOne(wrapper);
        //创建返回对象并且填充属性
        ManagerVo managerVo = new ManagerVo();
        managerVo.setUid(String.valueOf(manager1.getId()));
        managerVo.setUsername(manager1.getUsername());
        managerVo.setAvatar(manager1.getLogo());
        return managerVo;
    }

    /**
     * 用户登出
     * @param token
     * @return
     */
    public  Boolean logout(String token) {
        //用户登出即为删除redis中的token
        Boolean delete = redisTemplate.delete(token);
        return delete;

    }

    /**
     * 解析token
     * @param token
     * @return
     */
    public Manager queryAdminToken(String token) {
        try {
            List<String> split = StrUtil.split(token, " ");
            Map<String, Object> body = Jwts.parser().setSigningKey(secret).parseClaimsJws(split.get(1)).getBody();
            //将body转化为json字符串
            String jsonString = JSON.toJSONString(body);
            //将json字符串转化为user对象
            Manager manager = JSON.parseObject(jsonString, Manager.class);
                return manager;
        } catch (Exception e) {
            throw new BusinessException(ErrorResult.tokenFail());
        }
    }
}
