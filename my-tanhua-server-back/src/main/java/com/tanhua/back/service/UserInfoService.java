package com.tanhua.back.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserInfoService {
    @Autowired
    private UserInfoMapper userInfoMapper;

    /**
     * 根据 userId 查询单个用户
     *
     * @param userId
     * @return
     */
    public UserInfo queryByUserId(Long userId) {
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserInfo::getUserId, userId);
        UserInfo userInfo = userInfoMapper.selectOne(wrapper);
        return userInfo;
    }

    /**
     * 根据 userIds 查询多个用户
     *
     * @param userIds
     * @return
     */
    public List<UserInfo> queryByUserIds(List<Object> userIds) {
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(UserInfo::getUserId, userIds);
        List<UserInfo> userInfoList = userInfoMapper.selectList(wrapper);
        return userInfoList;
    }

    /**
     * 多条件查询
     *
     * @param userId
     * @param nickName
     * @param city
     * @return
     */
    public IPage<UserInfo> multiQueryUser(Integer page, Integer pageSize, Long userId, String nickName, String city) {



        IPage<UserInfo> iPage = new Page<>(page,pageSize);

        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();


        if (ObjectUtil.isNotEmpty(userId)) {
            wrapper.like(UserInfo::getUserId, userId);
        }

        if (StrUtil.isNotEmpty(nickName)) {
            wrapper.like(UserInfo::getNickName, nickName);
        }

        if (StrUtil.isNotEmpty(city)) {
            wrapper.like(UserInfo::getCity, city);
        }


       iPage= userInfoMapper.selectPage(iPage, wrapper);


        return iPage;
    }
}
