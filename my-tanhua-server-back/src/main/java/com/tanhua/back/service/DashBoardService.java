package com.tanhua.back.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.EnumUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.back.vo.DashboardUsersVo;
import com.tanhua.back.vo.DistributionVo;
import com.tanhua.back.vo.SummaryVo;
import com.tanhua.common.enums.SexEnum;
import com.tanhua.common.mapper.UserInfoMapper;
import com.tanhua.common.pojo.UserInfo;
import com.tanhua.dubbo.server.api.DashBoardApi;
import com.tanhua.dubbo.server.enums.AgeRangeEnum;
import com.tanhua.dubbo.server.enums.AreaEnum;
import com.tanhua.dubbo.server.enums.IndustryEnum;
import com.tanhua.dubbo.server.pojo.UserActiveRecord;
import com.tanhua.dubbo.server.vo.Distribution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DashBoardService {
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Reference
    private DashBoardApi dashBoardApi;

    /**
     * 注册用户分布，行业top、年龄、性别、地区
     *
     * @param sd 开始时间
     * @param ed 结束时间
     * @return 行业top10 年龄分别 性别分布 地区分布 地区合计
     */
    public DistributionVo queryDistribution(Long sd, Long ed) {
        //结果对象
        DistributionVo distributionVo = new DistributionVo();
        //查询行业top10
        List<Object> industryList = EnumUtil.getFieldValues(IndustryEnum.class, "name");
        //备选集合
        List<Distribution> industryTop = new ArrayList<>();
        for (Object industry : industryList) {
            LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
            wrapper.like(UserInfo::getIndustry, industry);
            //设置时间范围
            wrapper.between(UserInfo::getCreated, new Date(sd), new Date(ed));
            //查询行业人数
            Integer count = userInfoMapper.selectCount(wrapper);
            //封装成对象
            Distribution industryDistribution = new Distribution();
            industryDistribution.setTitle(String.valueOf(industry));
            industryDistribution.setAmount(count);
            //添加到备选集合
            industryTop.add(industryDistribution);
        }
        //将备选集合进行排序
        CollUtil.sort(industryTop, new Comparator<Distribution>() {
            @Override
            public int compare(Distribution o1, Distribution o2) {
                //以人数降序排列
                return o2.getAmount() - o1.getAmount();
            }
        });
        //取前十个组成最终结果
        List<Distribution> industryTop10 = CollUtil.sub(industryTop, 0, 10);
        //正确数据
        //distributionVo.setIndustryDistribution(industryTop10);


        //解决前端显示错误
        List<Distribution> industryF = new ArrayList<>();
        for (int i = 0; i < industryTop10.size(); i++) {
            Distribution distribution = new Distribution();
            distribution.setTitle(industryTop10.get(industryTop10.size() - 1 - i).getTitle());
            distribution.setAmount(industryTop10.get(i).getAmount());
            industryF.add(distribution);
        }
        //提供错误数据以解决前端显示错误
        distributionVo.setIndustryDistribution(industryF);


        //查询年龄分布
        List<Distribution> ageRangeDistributionList = new ArrayList<>();
        //遍历年龄分别枚举
        for (AgeRangeEnum ageRangeEnum : AgeRangeEnum.values()) {
            LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
            //设置年龄范围(不包括最大值)
            wrapper.between(UserInfo::getAge, ageRangeEnum.getMin(), ageRangeEnum.getMax() - 1);
            //设置时间范围
            wrapper.between(UserInfo::getCreated, new Date(sd), new Date(ed));
            //查询年龄范围的人数
            Integer count = userInfoMapper.selectCount(wrapper);
            //封装成对象
            Distribution ageRangeDistribution = new Distribution();
            ageRangeDistribution.setTitle(ageRangeEnum.getDesc());
            ageRangeDistribution.setAmount(count);
            ageRangeDistributionList.add(ageRangeDistribution);
        }
        distributionVo.setAgeDistribution(ageRangeDistributionList);
        //查询性别分布
        List<Distribution> genderDistributionList = new ArrayList<>();
        //查询男性人数
        {
            LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
            //设置条件为男性
            wrapper.eq(UserInfo::getSex, SexEnum.MAN.getValue());
            //设置时间范围
            wrapper.between(UserInfo::getCreated, new Date(sd), new Date(ed));
            //查询男性人数
            Integer count = userInfoMapper.selectCount(wrapper);
            //封装成对象
            Distribution manDistribution = new Distribution();
            manDistribution.setTitle("男性用户");
            manDistribution.setAmount(count);
            genderDistributionList.add(manDistribution);
        }
        //查询女性人数
        {
            LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
            //设置条件为女性
            wrapper.eq(UserInfo::getSex, SexEnum.WOMAN.getValue());
            //设置时间范围
            wrapper.between(UserInfo::getCreated, new Date(sd), new Date(ed));
            //查询女性人数
            Integer count = userInfoMapper.selectCount(wrapper);
            //封装成对象
            Distribution womanDistribution = new Distribution();
            womanDistribution.setTitle("女性用户");
            womanDistribution.setAmount(count);
            genderDistributionList.add(womanDistribution);
        }
        distributionVo.setGenderDistribution(genderDistributionList);
        //查询地区分布
        List<Distribution> areaDistributionList = new ArrayList<>();
        //查询地区合计
        List<Distribution> areaTotalDistributionList = new ArrayList<>();
        //获取大地区列表
        List<Object> areaList = EnumUtil.getFieldValues(AreaEnum.class, "area");
        //去除重复大地区
        Set<String> areaSet = new HashSet<>();
        for (Object area : areaList) {
            areaSet.add(area.toString());
        }
        for (String area : areaSet) {
            //封装地区合计对象
            Distribution areaTotalDistribution = new Distribution();
            areaTotalDistribution.setTitle(area.toString());
            //设置初始值为0人
            areaTotalDistribution.setAmount(0);
            //遍历地区分别枚举
            for (AreaEnum areaEnum : AreaEnum.values()) {
                if (StrUtil.equals(area.toString(), areaEnum.getArea())) {
                    LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
                    //设置地区条件
                    wrapper.like(UserInfo::getCity, areaEnum.getProvince());
                    //设置时间范围
                    wrapper.between(UserInfo::getCreated, new Date(sd), new Date(ed));
                    //查询地区人数
                    Integer count = userInfoMapper.selectCount(wrapper);
                    //封装成对象
                    Distribution areaDistribution = new Distribution();
                    areaDistribution.setTitle(areaEnum.getProvince());
                    areaDistribution.setAmount(count);
                    areaDistributionList.add(areaDistribution);
                    //统计到地区合计人数
                    areaTotalDistribution.setAmount(areaTotalDistribution.getAmount() + count);
                }
            }
            areaTotalDistributionList.add(areaTotalDistribution);
        }
        distributionVo.setLocalDistribution(areaDistributionList);
        distributionVo.setLocalTotal(areaTotalDistributionList);
        return distributionVo;
    }

    /**
     * 查询时间段内每日新增、活跃用户、次日留存率
     *
     * @param sd   开始时间
     * @param ed   结束时间
     * @param type 101 新增  102 活跃用户  103 次日留存率
     * @return
     */
    public DashboardUsersVo queryNewUsers(Long sd, Long ed, Integer type) {
        //创建结果对象
        DashboardUsersVo dashboardUsersVo = new DashboardUsersVo();
        //查询今年时间段内每日注册人数
        List<Distribution> thisYearDistributionList = new ArrayList<>();
        //查询去年时间段内每日注册人数
        List<Distribution> lastYearDistributionList = new ArrayList<>();
        //开始时间
        Date startDate = new Date(sd);
        //结束时间
        Date endDate = new Date(ed);
        //计算天数
        long betweenDays = DateUtil.between(startDate, endDate, DateUnit.DAY);
        for (long i = -1; i < betweenDays; i++) {
            //今年
            Distribution thisYearDistribution = new Distribution();
            //去年
            Distribution lastYearDistribution = new Distribution();
            //要查询的具体日期(今年)
            Date dayTime = DateUtil.offsetDay(startDate, Convert.toInt(i + 1));
            //要查询的具体日期(去年)
            DateTime lastDayTime = DateUtil.offset(dayTime, DateField.YEAR, -1);
            Integer thisYearCount = 0;
            Integer lastYearCount = 0;
            switch (type) {
                case 101: {
                    //查询今年新增用户
                    thisYearCount = queryNewUsers(dayTime);
                    //查询去年新增用户
                    lastYearCount = queryNewUsers(lastDayTime);
                    break;
                }
                case 102: {
                    //查询今年活跃用户
                    thisYearCount = dashBoardApi.countTimeActiveUsers(DateUtil.beginOfDay(dayTime).getTime()
                            , DateUtil.endOfDay(dayTime).getTime(), 0L);
                    //查询去年活跃用户
                    lastYearCount = dashBoardApi.countTimeActiveUsers(DateUtil.beginOfDay(lastDayTime).getTime()
                            , DateUtil.endOfDay(lastDayTime).getTime(), 0L);
                    break;
                }
                case 103: {
                    //查询今年次日留存率
                    thisYearCount = queryNextDayUsers(dayTime);
                    //查询去年次日留存率
                    lastYearCount = queryNextDayUsers(lastDayTime);
                    break;
                }
            }
            //封装今年对象
            thisYearDistribution.setTitle((DateUtil.month(dayTime) + 1) + "月" + (DateUtil.dayOfMonth(dayTime)) + "日");
            thisYearDistribution.setAmount(thisYearCount);
            thisYearDistributionList.add(thisYearDistribution);
            //封装去年对象
            lastYearDistribution.setTitle((DateUtil.month(lastDayTime) + 1) + "月" + (DateUtil.dayOfMonth(lastDayTime)) + "日");
            lastYearDistribution.setAmount(lastYearCount);
            lastYearDistributionList.add(lastYearDistribution);
        }
        dashboardUsersVo.setThisYear(thisYearDistributionList);
        dashboardUsersVo.setLastYear(lastYearDistributionList);
        return dashboardUsersVo;
    }

    /**
     * 查询日期当日新增用户
     *
     * @param dayTime 日期
     * @return 新增用户数
     */
    private Integer queryNewUsers(Date dayTime) {
        //查询新增用户
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        //条件为创建时间在 当日开始时间 与 当日结束时间 之间
        wrapper.between(UserInfo::getCreated, DateUtil.beginOfDay(dayTime), DateUtil.endOfDay(dayTime));
        return userInfoMapper.selectCount(wrapper);
    }

    /**
     * 查询每月注册用户的次日留存率
     *
     * @param dayTime 日期
     * @return 次日留存人数
     */
    public Integer queryNextDayUsers(Date dayTime) {
        //查询日期前一天注册用户
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        //条件为创建时间在 日期前一天开始 到 日期前一天结束 之间
        wrapper.between(UserInfo::getCreated,
                DateUtil.beginOfDay(DateUtil.offsetDay(dayTime, -1)),
                DateUtil.endOfDay(DateUtil.offsetDay(dayTime, -1)));
        List<UserInfo> userInfoList = userInfoMapper.selectList(wrapper);
        //将UserInfo集合封装为Distribution集合
        List<UserActiveRecord> userActiveRecords = new ArrayList<>();
        for (UserInfo userInfo : userInfoList) {
            //需要userId与activeTime属性
            UserActiveRecord userActiveRecord = new UserActiveRecord();
            userActiveRecord.setUserId(userInfo.getUserId());
            userActiveRecord.setActiveTime(userInfo.getCreated().getTime());
            userActiveRecords.add(userActiveRecord);
        }
        //查询次日留存人数
        return dashBoardApi.countNextDayUsers(userActiveRecords);
    }

    /**
     * 查询查询概要统计信息
     *
     * @return
     */
    public SummaryVo querySummary() {
        //创建结果对象
        SummaryVo summaryVo = new SummaryVo();
        //查询累积用户
        {
            Integer count = userInfoMapper.selectCount(null);
            summaryVo.setCumulativeUsers(count);
        }
        //查询过去三十天活跃用户(不包括今日)
        {
            Integer count = dashBoardApi.countActivePass(30, 0L);
            summaryVo.setActivePassMonth(count);
        }
        //查询过去七天活跃用户(不包括今日)
        {
            Integer count = dashBoardApi.countActivePass(7, 0L);
            summaryVo.setActivePassWeek(count);
        }
        //查询今日新增用户
        {
            LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
            wrapper.between(UserInfo::getCreated, DateUtil.beginOfDay(DateUtil.date()), DateUtil.endOfDay(DateUtil.date()));
            Integer count = userInfoMapper.selectCount(wrapper);
            summaryVo.setNewUsersToday(count);
        }
        //查询今日新增用户涨跌率
        {
            //计算昨日开始时间
            Date startTime = DateUtil.beginOfDay(DateUtil.offsetDay(DateUtil.date(), -1));
            //计算昨日结束时间
            Date endTime = DateUtil.endOfDay(DateUtil.offsetDay(DateUtil.date(), -1));
            LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
            wrapper.between(UserInfo::getCreated, startTime, endTime);
            //查询昨日新增用户数
            Double countYesterday = Convert.toDouble(userInfoMapper.selectCount(wrapper));
            //查询今日新增用户数
            Double countNewUsersToday = Convert.toDouble(summaryVo.getNewUsersToday());
            //防止by Zero错误
            if (countYesterday == 0) {
                //今日和昨日都为0则涨跌率为0
                if (countNewUsersToday == 0) {
                    summaryVo.setNewUsersTodayRate(0);
                } else {
                    summaryVo.setNewUsersTodayRate(100);
                }
            } else {
                //昨日不为0,今日为0则下降100%
                if (countNewUsersToday == 0) {
                    summaryVo.setNewUsersTodayRate(-100);
                } else {
                    //计算涨跌率
                    Integer count = Convert.toInt((countNewUsersToday - countYesterday) / countYesterday * 100);
                    summaryVo.setNewUsersTodayRate(count);
                }
            }
        }
        //查询今日登录次数
        {
            Integer count = dashBoardApi.countTimeLoginUsers(DateUtil.beginOfDay(DateUtil.date()).getTime()
                    , DateUtil.endOfDay(DateUtil.date()).getTime());
            summaryVo.setLoginTimesToday(count);
        }
        //查询今日登录次数涨跌率
        {
            //查询昨日登陆操作数
            Double countLoginPass = Convert.toDouble(dashBoardApi.countActivePass(1, 1L));
            //查询今日登陆操作数
            Double countLoginTimesToday = Convert.toDouble(summaryVo.getLoginTimesToday());
            //防止by Zero错误
            if (countLoginPass == 0) {
                //今日和昨日都为0则涨跌率为0
                if (countLoginTimesToday == 0) {
                    summaryVo.setLoginTimesTodayRate(0);
                } else {
                    summaryVo.setLoginTimesTodayRate(100);
                }
            } else {
                //昨日不为0,今日为0则下降100%
                if (countLoginTimesToday == 0) {
                    summaryVo.setLoginTimesTodayRate(-100);
                } else {
                    //计算涨跌率
                    Integer count = Convert.toInt((countLoginTimesToday - countLoginPass) / countLoginPass * 100);
                    summaryVo.setLoginTimesTodayRate(count);
                }
            }
        }
        //查询今日活跃用户
        {
            Integer count = dashBoardApi.countActiveToday(0L);
            summaryVo.setActiveUsersToday(count);
        }
        //查询昨日活跃用户
        {
            //计算昨日开始时间
            DateTime beginOfYesterday = DateUtil.beginOfDay(DateUtil.offsetDay(DateUtil.date(), -1));
            //结束昨日结束时间
            DateTime endOfYesterday = DateUtil.endOfDay(DateUtil.offsetDay(DateUtil.date(), -1));
            //查询活跃用户
            Integer count = dashBoardApi.countTimeActiveUsers(beginOfYesterday.getTime(), endOfYesterday.getTime(), 0L);
            summaryVo.setActiveUsersYesterday(count);
        }
        //查询今日日活跃用户涨跌率
        {
            //查询昨日日活跃用户涨跌率
            Double activeUsersYesterday = Convert.toDouble(summaryVo.getActiveUsersYesterday());
            //查询今日日活跃用户涨跌率
            Double activeUsersToday = Convert.toDouble(summaryVo.getActiveUsersToday());
            //防止by Zero错误
            if (activeUsersYesterday == 0) {
                //今日和昨日都为0则涨跌率为0
                if (activeUsersToday == 0) {
                    summaryVo.setActiveUsersTodayRate(0);
                } else {
                    summaryVo.setActiveUsersTodayRate(100);
                }
            } else {
                //昨日不为0,今日为0则下降100%
                if (activeUsersToday == 0) {
                    summaryVo.setActiveUsersTodayRate(-100);
                } else {
                    //计算涨跌率
                    Integer count = Convert.toInt((activeUsersToday - activeUsersYesterday) / activeUsersYesterday * 100);
                    summaryVo.setActiveUsersTodayRate(count);
                }
            }
        }
        //查询过去七天平均使用时长(秒)
        {
            /*//获取活跃用户数
            Integer activePassWeek = summaryVo.getActivePassWeek();
            //防止by Zero错误
            if (activePassWeek == 0) {
                activePassWeek = 1;
            }*/
            //查询过去七天总使用时长
            Long useTime = dashBoardApi.queryUseTimePass(7);
            //计算平均使用时长(s)    使用时长/总用户数/1000
            Long count = useTime / 7 - (8 * 60 * 60 * 1000);
            summaryVo.setUseTimePassWeek(count);
        }
        //查询昨日活跃用户涨跌率
        {
            //计算前天开始时间
            long startTime = DateUtil.beginOfDay(DateUtil.offsetDay(DateUtil.date(), -2)).getTime();
            //计算前天结束时间
            long endTime = DateUtil.endOfDay(DateUtil.offsetDay(DateUtil.date(), -2)).getTime();
            //查询前天活跃人数
            Double activeUsers = Convert.toDouble(dashBoardApi.countTimeActiveUsers(startTime, endTime, 0L));
            //查询昨天活跃人数
            Double activeUsersYesterday = Convert.toDouble(summaryVo.getActiveUsersYesterday());
            //防止by Zero错误
            if (activeUsers == 0) {
                //昨日和前日都为0则涨跌率为0
                if (activeUsersYesterday == 0) {
                    summaryVo.setActiveUsersYesterdayRate(0);
                } else {
                    summaryVo.setActiveUsersYesterdayRate(100);
                }
            } else {
                //前日不为0,昨日为0则下降100%
                if (activeUsersYesterday == 0) {
                    summaryVo.setActiveUsersYesterdayRate(-100);
                } else {
                    //计算涨跌率
                    Integer count = Convert.toInt((activeUsersYesterday - activeUsers) / activeUsers * 100);
                    summaryVo.setActiveUsersYesterdayRate(count);
                }
            }
        }
        return summaryVo;
    }
}
