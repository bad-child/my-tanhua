package com.tanhua.back.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.back.vo.LoginLogsVo;
import com.tanhua.back.vo.PageResult;
import com.tanhua.dubbo.server.api.LogsApi;
import com.tanhua.dubbo.server.api.UserLocationApi;
import com.tanhua.dubbo.server.pojo.UserActiveRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class LogsService {

    @Reference
    private LogsApi logsApi;

    @Reference
    private UserLocationApi uerLocationApi;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    /**
     * 日志管理 : 日志翻页
     *
     * @param page
     * @param pageSize
     * @param sortProp
     * @param sortOrder
     * @param type
     * @param userId
     * @return
     */
    public PageResult queryLogsList(Integer page, Integer pageSize, String sortProp, String sortOrder, String type, String userId) {

        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPagesize(pageSize);

        // 获取用户登录记录对象的集合
        List<UserActiveRecord> backUserVoList = logsApi.queryLogsList(page, pageSize, sortProp, sortOrder, type, userId);
        if (CollUtil.isEmpty(backUserVoList)) {
            return pageResult;
        }

        //  创建登录日志vo对象的集合,对每条登录日志对象进行封装
        List<LoginLogsVo> loginLogsVoList = new ArrayList<>();

        for (UserActiveRecord userActiveRecord : backUserVoList) {
            LoginLogsVo loginLogsVo = new LoginLogsVo();

            loginLogsVo.setId(Convert.toInt(userActiveRecord.getId()));

            loginLogsVo.setLogTime(userActiveRecord.getActiveTime());  // 登录的日志时间
            System.out.println(loginLogsVo.getLogTime());

            String redisKey = "login" + userId;
            String hashKey = userActiveRecord.getActiveTime().toString();
            Object o = redisTemplate.opsForHash().get(redisKey, hashKey);
            String loginAddress = Convert.toStr(o);
            if (StrUtil.isEmpty(loginAddress)){
                loginLogsVo.setPlace("中国北京市东城区锡拉胡同6号");  // 登录地址
            }else {
                loginLogsVo.setPlace(loginAddress);
            }



            loginLogsVo.setEquipment("Android");  // 登录设备

            loginLogsVoList.add(loginLogsVo);

        }

        pageResult.setPages((logsApi.queryAllLogs(userId).size() / pageSize) + 1);
        pageResult.setCounts(logsApi.queryAllLogs(userId).size());
        pageResult.setItems(loginLogsVoList);
        return pageResult;
    }
}
